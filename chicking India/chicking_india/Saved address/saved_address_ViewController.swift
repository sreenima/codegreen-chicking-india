//
//  saved_address_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 26/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlacePicker
import Alamofire
import SWRevealViewController

class saved_address_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var new_btn: UIButton!
    @IBOutlet weak var table_view: UITableView!
    var all_address = NSArray()
    var checker = String()
    var addr = NSString()
    var addr_pos = NSString()
    var no_ad = NSString()
    var address_keep = NSString()
    var idd = String()
    var from_edit = String()
    var check = String()
    var no_back = String()
    var the_way = String()
    var full_text = String()
    
    var on_my_way_from_account = String()
    
    @IBOutlet weak var drawer_btn: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if userdata_services.instance.side_btn_needed == "yes"{
            
            self.from_edit = "no"
            self.no_back = "yes"
            userdata_services.instance.side_btn_needed = "no"
            back_btn.isHidden = true
            drawer_btn.isHidden = false
        }
        else{
            back_btn.isHidden = false
            drawer_btn.isHidden = true
        }
        
        if userdata_services.instance.the_way == "no_ad"{
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "order") as! order_customizeViewController
            
            
            
            self.present(newViewController, animated: false, completion: nil)
            
            
        }
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        
        new_btn.layer.cornerRadius = 8
        
        
        if  authentication_services.instance.isloggedin == false
        {
            
            
            print("Not logged in!")
            
        }
        else{
            var header = [
                "Authorization":"Bearer \(authentication_services.instance.token)",
                "Content-Type": "application/json; charset=utf-8"
            ]
            
            self.spinner.startAnimating()
            Alamofire.request("\("\(BASE_URL)listAllAddress")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                
                print(response.result.error)

                
                if response.result.error == nil {
                    self.spinner.stopAnimating()
                    guard let response_data = response.data else {return}
                    
                    let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                    print(json as Any)
                    let doom = json as! [String : Any]
                    if doom["error"] == nil{
                        
                        if doom["status"] as! String == "ok"{
                            print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                            print(doom)
                            let data1 = doom["data"] as! NSArray
                            
                            self.all_address = data1
                            self.table_view.reloadData()
                            
                        }
                        else{
                            
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(doom["message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.all_address = NSArray()
                            
                            self.table_view.reloadData()
                            self.present(alert2, animated: true, completion: nil)
                            
                            
                        }
                    }
                    else{
                        self.spinner.stopAnimating()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                       
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    // Do any additional setup after loading the view.
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                    
                    
                }
            }
            
        }
        
        if self.revealViewController() != nil {
            drawer_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
           
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
           
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
        
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return self.all_address.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! saved_address_TableViewCell
        
        
        
        
        cell.Vview.layer.cornerRadius = 8
        cell.Vview.layer.borderWidth = 1
        cell.Vview.layer.borderColor = UIColor.clear.cgColor
        cell.Vview.layer.masksToBounds = true
        
        cell.Vview.layer.shadowColor = UIColor.black.cgColor
        cell.Vview.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.Vview.layer.shadowRadius = 3.0
        cell.Vview.layer.shadowOpacity = 0.2
        cell.Vview.layer.masksToBounds = false
        cell.Vview.layer.backgroundColor = UIColor.white.cgColor
        
        if self.all_address.count != 0{
            
            
            var dat = self.all_address[indexPath.section] as! Dictionary<String,Any>
            print(all_address)
            print(dat)
            
            var type = dat["type"] as! String
            var add = dat["street"] as! String
            var flat = dat["flat_name"] as! String
            var land = dat["landmark"] as! String
            var area1 = dat["area_details"] as! Dictionary<String,Any>
            var area = area1["name"] as! String
            var building = dat["building"] as! String
            
            if type == "home"{
                
                cell.img_vw.image = #imageLiteral(resourceName: "web-page-home (1)")
                
            }
            else if type == "work"{
                
                cell.img_vw.image = #imageLiteral(resourceName: "portfolio (1)")
            }
            else if type == "other"{
                
                cell.img_vw.image = #imageLiteral(resourceName: "pin (2)")
            }
            else{
                
                cell.img_vw.image = #imageLiteral(resourceName: "pin (2)")
                
            }
            
            
            
            if type == "home"{
                
                
                cell.address_lbl.text = "HOME"
                
            }
            else if type == "work"{
                cell.address_lbl.text = "WORK"
                
            }
            else{
                
                cell.address_lbl.text = "OTHER"
            }
            
            
            if building != ""{
                
                self.full_text = "\(building),"
                cell.main_address_label.text = full_text
                print(full_text)
            }
            
            if add != ""{
                
                self.full_text = "\(self.full_text)\(add),"
                cell.main_address_label.text = full_text
                print(full_text)
            }
            if flat != ""{
                
                self.full_text =  "\(self.full_text)\(flat),"
                cell.main_address_label.text = full_text
                print(full_text)
            }
            if land != ""{
                
                self.full_text = "\(self.full_text)\(land)"
                cell.main_address_label.text = full_text
                print(full_text)
            }
            if area != ""{

                self.full_text =  "\(self.full_text)\(area),"
                cell.main_address_label.text = full_text
                print(full_text)

            }
                
            else{
                var add2 = dat["name"] as! String
                if add2 != ""{
                    cell.main_address_label.text = add2
                    print(add2)
                }
            }
            
            
        }
        
        
        
        return cell
        
    }
    
    @IBAction func delete_address(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "", message: "Do you want to delete this address !", preferredStyle: UIAlertControllerStyle.alert)
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.table_view)
                let indexPath1 = self.table_view.indexPathForRow(at: buttonPosition)
                print(indexPath1!)
                
                let  pg = self.all_address[indexPath1!.row] as! Dictionary<String,Any>
                print("njaaaaaaaaaaaaaaaaaaaaaaaaan\(pg)")
                let id = pg["id"]!
                var header = [
                    "Authorization":"Bearer \(authentication_services.instance.token)",
                    "Content-Type": "application/json; charset=utf-8"
                ]
                self.spinner.startAnimating()
                Alamofire.request("\("\(BASE_URL)deleteAddress/\(id)")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                    print(response)
                    print(response.result)
                    print(response.result.error)
                    if response.result.error == nil {
                        self.spinner.stopAnimating()
                        guard let response_data = response.data else {return}
                        
                        
                        let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                        print(json as Any)
                        let doom = json as! [String : Any]
                        if doom["error"] == nil{
                            
                            if doom["status"] as! String == "ok"{
                                self.viewDidLoad()
                                self.table_view.reloadData()
                                
                                
                            }
                            else{
                                self.spinner.stopAnimating()
                                let alert2 = UIAlertController(title: "", message: "\(doom["message"])", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                                
                                
                            }
                        }
                        else{
                            self.spinner.stopAnimating()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                            
                            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        // Do any additional setup after loading the view.
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                    
                }
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dat = self.all_address[indexPath.section] as! Dictionary<String,Any>
        print(dat)
        idd = "\(dat["id"]!)"
        if userdata_services.instance.make_address == "yes"{
            authentication_services.instance.Edit_addresS(id: idd){ (success) in
                //self.spinner.stopAnimating()
                if success {
                    //self.spinner2.stopAnimating()
                    if userdata_services.instance.add_address_data1["error"]  == nil{
                        
                        
                        if userdata_services.instance.add_address_data1["status"]  as! String == "ok"{
                            
                            
                            
                            print("adipoli")
                            if userdata_services.instance.make_address == "yes"{
                                let alert = UIAlertController(title: "", message: "Do You Want To Make This The Default Address", preferredStyle: UIAlertControllerStyle.alert)
                                
                                
                                alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { action in
                                    switch action.style{
                                    case .default:
                                        print("default")
                                        
                                    case .cancel:
                                        print("cancel")
                                        
                                    case .destructive:
                                        print("destructive")
                                        
                                        
                                    }}))
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    switch action.style{
                                    case .default:
                                        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order")
                                        
                                        self.present(newViewController, animated: false, completion: nil)
                                        
                                        
                                    case .cancel:
                                        print("cancel")
                                        
                                    case .destructive:
                                        print("destructive")
                                        
                                        
                                    }}))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            // self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data1["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        //userdata_services.instance.logout()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    
                }
                    
                else{
                    
                    //self.spinner2.stopAnimating()
                    
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
        }
        else{
           
        }
    }
    
    
    @IBAction func back_click(_ sender: Any) {
        if userdata_services.instance.make_address == "yes"{
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order")
            
            self.present(newViewController, animated: false, completion: nil)
        }else{
            if no_back == "yes"{
                let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal1")
                self.present(nnewViewController, animated: false, completion: nil)
                
            }
            else{
                
                if  on_my_way_from_account == "yes"{
                    let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "acc")
                    self.present(nnewViewController, animated: false, completion: nil)
                    
                }else{
                    dismiss(animated: false, completion: nil)
                }
            }
        }
    }
    
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func homee_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    @IBAction func add_address_click(_ sender: Any) {
        
       
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "search") as! search_area_ViewController
        
        newViewController.addr_pos = self.addr_pos
        newViewController.addr = self.addr
        newViewController.no_ad = self.no_ad
        newViewController.from_edit = self.from_edit
        // userdata_services.instance.no_address = "yes"
        
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension saved_address_ViewController : GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        viewController.dismiss(animated: true, completion: nil)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            addr = place.formattedAddress! as NSString
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        
        if from_edit == "yes" {
            newViewController.check = "no"
            newViewController.checker = "NO"
        }
        else{
            newViewController.checker = "yes"
            newViewController.check = "yes"
            newViewController.id_check = self.check
        }
        
        
        if no_ad == "yes" {
            newViewController.no_add = no_ad
            newViewController.test = addr_pos
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            //            add_text.text = addr as String
        }
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        //dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
}
