//
//  main_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 13/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire
import AlamofireImage
import MapKit
import CoreLocation
import Firebase

class main_ViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, CLLocationManagerDelegate{
    
    @IBOutlet weak var account_icon: UIButton!
    @IBOutlet weak var cart_pop: UIButton!
    @IBOutlet weak var collection1: UICollectionView!
    @IBOutlet weak var collection2: UICollectionView!
    @IBOutlet weak var collection3: UICollectionView!
    @IBOutlet weak var collection4: UICollectionView!
    @IBOutlet weak var home_icon: UIButton!
    @IBOutlet weak var account_btn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var scrolling_index = 0
    var longi = "55.17128"
    var lati = "25.0657"
    var data3 = Dictionary<String,Any>()
    var ok = String()
    var doo = "no"
    let locationManager = CLLocationManager()
    @IBOutlet weak var menu_btn: UIButton!
    var image_array = NSArray()
    var popular_dishes = NSArray()
    
    var Testimonial = NSArray()
    var check_coll2 = String()
    var img_name = String()
    
    var image1 = UIImage()
    var image2 = UIImage()
    var image3 = UIImage()
    var image4 = UIImage()
    var image5 = UIImage()
    var last_load = String()
    var device_token = String()
    var instanceIDTokenMessage = String()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
        
    }
    
    
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        
        if image_array.count > 0{
            
            if scrolling_index < (image_array.count - 1){
                
                scrolling_index = scrolling_index + 1
                
                collection1.scrollToItem(at: [0,scrolling_index], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                
            }
            else{
                scrolling_index = 0
                
                collection1.scrollToItem(at: [0,scrolling_index], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            }
            
            
        }
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func starTimer() {
        
        let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector((scrollToNextCell)), userInfo: nil, repeats: true);
        doo = "yes"
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        longi = "\(locValue.longitude)"
        lati = "\(locValue.latitude)"
        
        print(longi)
        print(lati)
        
        self.outlet()
    }
    
    
    func token_updated(){
        
        if  authentication_services.instance.isloggedin != false {
            
            print(self.device_token)
            
            self.spinner.startAnimating()
            authentication_services.instance.fcm_toke_n(device: "ios", value: self.device_token){ (success) in
                if success {
                    
                    self.spinner.stopAnimating()
                    if userdata_services.instance.fcm_tk["error"]  == nil{
                        
                        
                        if userdata_services.instance.fcm_tk["status"]  as! String == "ok"{
                            
                            
                            print(userdata_services.instance.fcm_tk["data"])
                            
                          
                            self.collection4.reloadData()
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.fcm_tk["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        //userdata_services.instance.logout()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    
                }
                    
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
            
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userdata_services.instance.no_address = "no"
        userdata_services.instance.make_address = "no"
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {

            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
           
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
           
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        self.outlet()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
        }
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.instanceIDTokenMessage  = "Remote InstanceID token: \(result.token)"
                self.device_token = "\(result.token)"
                print(self.device_token)
                self.token_updated()
            }
        }
        
        UserDefaults.standard.set(1, forKey: "keepvalue")
        
        home_icon.setBackgroundImage(#imageLiteral(resourceName: "icon-3"), for: [])
        
        
        self.spinner.startAnimating()
        authentication_services.instance.testimonial(){ (success) in
            if success {
                
                self.spinner.stopAnimating()
                if userdata_services.instance.testimonials_data["error"]  == nil{
                    
                    
                    if userdata_services.instance.testimonials_data["status"]  as! String == "ok"{
                        
                        
                        
                        self.Testimonial = userdata_services.instance.testimonials_data["data"] as! NSArray
                        
                        print(self.Testimonial)
                        self.collection4.reloadData()
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.testimonials_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
   
        
        self.spinner.startAnimating()
        authentication_services.instance.banner_images(FoodORcake: "cake"){ (success) in
            if success {
                
                self.spinner.stopAnimating()
                if userdata_services.instance.banner_images["error"]  == nil{
                    
                    
                    if userdata_services.instance.banner_images["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.banner_images["data"] as! NSArray
                        
                        self.image_array = data1
                        print(self.image_array)
                        self.collection1.reloadData()
                        if self.doo != "yes"{
                            
                            self.starTimer()
                        }
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.banner_images["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        authentication_services.instance.popular_dishes(){ (success) in
            if success {
                //self.spinner2.stopAnimating()
                if userdata_services.instance.popular_dishes["error"]  == nil{
                    
                    
                    if userdata_services.instance.popular_dishes["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.popular_dishes["data"] as! NSArray
                        
                        self.popular_dishes = data1
                        self.check_coll2 = "ok"
                        self.collection2.reloadData()
                        
                        
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.popular_dishes["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
            
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
        
        
    }
    
    
    
    func generateRoundCornerImage(image : UIImage , radius : CGFloat) -> UIImage {
        
        let imageLayer = CALayer()
        imageLayer.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        imageLayer.contents = image.cgImage
        imageLayer.masksToBounds = true
        imageLayer.cornerRadius = radius
        
        UIGraphicsBeginImageContext(image.size)
        imageLayer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
    
    
    func outlet() {
        let body = [
            
            
            "longitude" : "\(longi)",
            "latitude" : "\(lati)"
        ]
        
        
        
        print("body is:\(body)")
        
        
        
        Alamofire.request("\(BASE_URL)getNearbyOutlets", method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                self.data3 = json as! [String : Any]
                print("iaaaaaaavaaaaaaanaaaaaaaaaaanuuuuuuuau")
                print(self.data3)
                self.ok = "ok"
                self.collection3.reloadData()
                
                
            }
        }
    }
    
    
    
    @IBAction func menu_click(_ sender: Any) {
        
        UserDefaults.standard.set(1, forKey: "keepvalue")
    }
    
    @IBAction func cart_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        //        page?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        //        page?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collection1 {
            
            return self.image_array.count
            
            
        }
        else if collectionView == collection2 {
            if popular_dishes.count > 5{
                return 6
                
            }
            else{
                return popular_dishes.count+1
            }
            
            
            
        }
        else if collectionView == collection3 {
            
            return 3
        }
        else {
            return Testimonial.count
        }
        
    }
    var imageCache1 = NSCache<NSString,UIImage>()
    var imageCache2 = NSCache<NSString,UIImage>()
    var imageCache3 = NSCache<NSString,UIImage>()
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView == collection2 {
            
            return CGSize(width: collection2.frame.width/3.3, height: 120)
        }
            
            
        else if collectionView == collection3{
            
            return CGSize(width: collection2.frame.width/3.3, height: 105)
            
            
        }
        else if collectionView == collection4{
            
            return CGSize(width: collection4.frame.width-50, height: collection4.frame.height-10)
            
            
        }
            
        else{
            
            return collectionView.frame.size
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        
        if collectionView == collection2{
            
            return UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
        }
            
        else if collectionView == collection1{
            
            return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        }
        else if collectionView == collection4{
            
            return UIEdgeInsets(top: 20, left: 2, bottom: 25, right: 2)
        }
            
        else{
            
            return UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        if collectionView == collection2 || collectionView == collection3{
            
            return 15
        }
        else if collectionView == collection4 {
            return 18
        }
        else{
            
            return 0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        if collectionView == collection2 || collectionView == collection3{
            
            return 5
        }
        else if collectionView == collection1
        {
            
            return 0
            
        }
        else{
            
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == collection1 {
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll1", for: indexPath) as! maincell_CollectionViewCell
            
            
            cell.bannerview.layer.cornerRadius = 16.0
            cell.banner_img.layer.cornerRadius = 16.0
            
            cell.banner_img.roundCornersForAspectFit(radius: 12)
            
            cell.bannerview.layer.masksToBounds = true
            cell.banner_img.layer.masksToBounds = true
            cell.banner_img.clipsToBounds = true
            
            cell.layer.cornerRadius = 12
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false
            
            
            cell.banner_img.roundCornersForAspectFit(radius: 12)
            
            cell.banner_img.layer.cornerRadius=8
            
            let img_name = (((image_array.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            
            
            if let cacheImage = imageCache1.object(forKey: img_name as NSString ){
                print("image cache")
                cell.banner_img.image = cacheImage
                
                let corneredImage = generateRoundCornerImage(image: cacheImage, radius: 12)
                
              
                cell.banner_img.image = corneredImage
                
                cell.banner_img.clipsToBounds = true
              
            }
            else{
                print("image not cache")
                //self.spinner.startAnimating()
                print("\(BANNER_IMAGE_URL)\(String(describing: img_name))")
                
                Alamofire.request("\(BANNER_IMAGE_URL)\(String(describing: img_name))").responseImage { response in
                    
                    if let image = response.result.value {
                        
                        let imageData = UIImagePNGRepresentation(image)
                        let bytes = imageData!.count
                        let kB = Double(bytes) / 1000.0 // Note the difference
                        let KB = Double(bytes) / 1024.0 // Note the difference
                        
                        print(kB)
                        print(KB)
                       
                        print("image downloaded: \(image)")
                        cell.banner_img.image = image
                        
                        let corneredImage = self.generateRoundCornerImage(image: image, radius: 12)
                        
                       
                        cell.banner_img.image = corneredImage
                        
                        
                        cell.banner_img.clipsToBounds = true
                        self.imageCache1.setObject(image, forKey: img_name as NSString)
                       
                    }
                    
                }
                
            }
            
            
            return cell
        }
            
        else if collectionView == collection2{
            
            
            
            
            var ind = 0
            
            if popular_dishes.count > 5{
                ind = 5
                
            }
            else{
                ind = popular_dishes.count
            }
            
            if indexPath.row == ind
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2m", for: indexPath) as! maincell_CollectionViewCell
                
                
                cell.coll3view.layer.cornerRadius = 8
                cell.layer.cornerRadius = 8
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 3.0
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                
                cell.layer.backgroundColor = UIColor.white.cgColor
                
                return cell
                
            }
            else{
                if indexPath.row == 0 {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! maincell_CollectionViewCell
                    cell.coll2_img.image = UIImage()
                    cell.layer.cornerRadius = 8
                    cell.layer.borderWidth = 1
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 3.0
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.masksToBounds = false
                   
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    
                    
                    
                    cell.coll2_lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
                    
                    self.img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    
                    
                    print("image not cache")
                    //self.spinner.startAnimating()
                    print("\(IMAGE_URL)\(String(describing: img_name))")
                    
                    Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                        
                        
                        if let image = response.result.value {
                       
                            let imageData = UIImagePNGRepresentation(image)
                            let bytes = imageData!.count
                            let kB = Double(bytes) / 1000.0 // Note the difference
                            let KB = Double(bytes) / 1024.0 // Note the difference
                            
                            print(kB)
                            print(KB)
                          
                            print("image downloaded: \(image)")
                            
                            cell.coll2_img.isHidden = false
                            
                            
                            
                            
                            cell.coll2_img.image = image
                            
                            
                            
                            cell.coll2_img.clipsToBounds = true
                            self.imageCache2.setObject(image, forKey: self.img_name as NSString)
                            //      }
                        }
                        
                    }
                   
                    
                    return cell
                }
                
                if indexPath.row == 1 {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! maincell_CollectionViewCell
                    cell.coll2_img.image = UIImage()
                    cell.layer.cornerRadius = 8
                    cell.layer.borderWidth = 1
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 3.0
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.masksToBounds = false
                   
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    
                    
                    
                    cell.coll2_lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
                    
                    self.img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    
                    
                    print("image not cache")
                    //self.spinner.startAnimating()
                    print("\(IMAGE_URL)\(String(describing: img_name))")
                    
                    Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                        
                        
                        
                        if let image = response.result.value {
                            
                            print("image downloaded: \(image)")
                            
                            cell.coll2_img.isHidden = false
                            
                            cell.coll2_img.image = image
                            
                            
                            
                            cell.coll2_img.clipsToBounds = true
                           
                        }
                        
                    }
                    
                    
                    
                    
                    
                    return cell
                }
                if indexPath.row == 2 {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! maincell_CollectionViewCell
                    cell.coll2_img.image = UIImage()
                    cell.layer.cornerRadius = 8
                    cell.layer.borderWidth = 1
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 3.0
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.masksToBounds = false
                    
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    
                    
                    
                    cell.coll2_lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
                    
                    self.img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    
                    
                    print("image not cache")
                    //self.spinner.startAnimating()
                    print("\(IMAGE_URL)\(String(describing: img_name))")
                    
                    Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                        
                        
                        
                        if let image = response.result.value {
                            
                           
                            print("image downloaded: \(image)")
                            
                            cell.coll2_img.isHidden = false
                            
                            
                            
                            cell.coll2_img.image = image
                            
                            
                            
                            
                            cell.coll2_img.clipsToBounds = true
                           
                        }
                        
                    }
                
                    
                    return cell
                }
                if indexPath.row == 3 {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! maincell_CollectionViewCell
                    cell.coll2_img.image = UIImage()
                    cell.layer.cornerRadius = 8
                    cell.layer.borderWidth = 1
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 3.0
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.masksToBounds = false
                   
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    
                    
                    
                    cell.coll2_lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
                    
                    self.img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    
                    
                    print("image not cache")
                    //self.spinner.startAnimating()
                    print("\(IMAGE_URL)\(String(describing: img_name))")
                    
                    Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                        
                        
                        
                        if let image = response.result.value {
                            
                            print("image downloaded: \(image)")
                            
                            cell.coll2_img.isHidden = false
                            
                            cell.coll2_img.image = image
                            
                            cell.coll2_img.clipsToBounds = true
                            
                        }
                        
                    }
                    
               
                    
                    return cell
                }
                else {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! maincell_CollectionViewCell
                    cell.coll2_img.image = UIImage()
                    cell.layer.cornerRadius = 8
                    cell.layer.borderWidth = 1
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.black.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 3.0
                    cell.layer.shadowOpacity = 0.2
                    cell.layer.masksToBounds = false
                    
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    
                    
                    
                    cell.coll2_lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
                    
                    self.img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    
                    
                    print("image not cache")
                    
                    print("\(IMAGE_URL)\(String(describing: img_name))")
                    
                    Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                        
                        
                        if let image = response.result.value {
                            
                            print("image downloaded: \(image)")
                            
                            cell.coll2_img.isHidden = false
                            
                            cell.coll2_img.image = image
                            
                            
                            
                            cell.coll2_img.clipsToBounds = true
                          
                        }
                        
                    }
                  
                    return cell
                }
                
            }
            
        }
            
        else if collectionView == collection3{
            
            
            
            if indexPath.row == 0{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll3", for: indexPath) as! maincell_CollectionViewCell
                
                
                cell.layer.cornerRadius = 8
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 3.0
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                
                cell.layer.backgroundColor = UIColor.white.cgColor
                if ok == "ok"{
                    let datt = self.data3["data"] as! NSArray
                    let dat1 = datt[indexPath.row] as! Dictionary<String,Any>
                    let img = dat1["image"]
                    let name = dat1["title"] as! String
                    
                    
                    Alamofire.request("\(IMAGE_URL)branch/\(img!)").responseImage { response in
                        
                        
                        if let image = response.result.value {
                           
                            if response.error == nil{
                                
                                print("image downloaded: \(image)")
                                
                                var immm : UIImage = image
                                print("immm = \(immm)")
                                cell.outlet_img.image = image
                                cell.outlet_name.text = name
                                cell.outlet_img.clipsToBounds = true
                                
                                
                            }
                            
                        }
                    }
                }
                
                
                return cell
                
            }
            if indexPath.row == 1{
                
                
                
                
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll3", for: indexPath) as! maincell_CollectionViewCell
                
                
                cell.layer.cornerRadius = 8
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 3.0
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
               
                cell.layer.backgroundColor = UIColor.white.cgColor
                
                if ok == "ok"{
                    let datt = self.data3["data"] as! NSArray
                    let dat1 = datt[indexPath.row] as! Dictionary<String,Any>
                    let img = dat1["image"]
                    let name = dat1["title"] as! String
                    
                    
                    
                    
                    Alamofire.request("\(IMAGE_URL)branch/\(img!)").responseImage { response in
                        
                        
                        if let image = response.result.value {
                            //  cell.banner_img.frame.size.height = 220
                            
                            if response.error == nil{
                                
                                print("image downloaded: \(image)")
                                
                                let immm : UIImage = image
                                cell.outlet_img.image = image
                                cell.outlet_name.text = name
                                cell.outlet_img.clipsToBounds = true
                                
                                
                            }
                            
                        }
                    }
                }
                
                return cell
            }
            else if indexPath.row == 2
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll3m", for: indexPath) as! maincell_CollectionViewCell
                cell.coll4view.layer.cornerRadius = 8
                cell.layer.cornerRadius = 8
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 3.0
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                
                cell.layer.backgroundColor = UIColor.white.cgColor
                
                
                
                return cell
                
            }
                
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll3", for: indexPath) as! maincell_CollectionViewCell
                
                cell.layer.cornerRadius = 8
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 3.0
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                
                cell.layer.backgroundColor = UIColor.white.cgColor
                return cell
            }
            
            
        }
            
        else if collectionView == collection4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll4", for: indexPath) as! maincell_CollectionViewCell
            cell.col4img.layer.cornerRadius = cell.col4img.frame.height/2
            cell.col4img.clipsToBounds = true
            
            cell.layer.cornerRadius = 8
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 6.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false
            //  cell.layer.shadowPath = UIBezierPath(roundedRect: cell.layer.bounds, cornerRadius:  cell.layer.cornerRadius).cgPath
            cell.layer.backgroundColor = UIColor.white.cgColor
            
            
            var data1 = Testimonial[indexPath.row] as! Dictionary<String,Any>
            
            
            
            var comment = data1["comment"] as! String
            
            var name = data1["name"] as! String
            
            var type = data1["type"] as! String
            
            var text1 = comment.htmlToString
            
            cell.col4_lbl.text = text1
            
            
            cell.col4_name.text = name
            
            cell.col4_user.text = type
            
            
            
            let img_name = ("\(((data1["image"]!)) )").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            
            
            if let cacheImage = imageCache1.object(forKey: img_name as NSString ){
                print("image cache")
                cell.col4img.image = cacheImage
                //return
            }
            else{
                print("image not cache")
                
                print("\(IMAGE_URL)testimonials/\(String(describing: img_name))")
                // self.spinner.startAnimating()
                Alamofire.request("\(IMAGE_URL)testimonials/\(String(describing: img_name))").responseImage { response in
                    
                    
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.col4img.image = image
                        self.imageCache1.setObject(image, forKey: img_name as NSString)
                        //      }
                    }
                    
                }
                
            }
            
            
            
            
            return cell
            
            
            
            
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll4", for: indexPath) as! maincell_CollectionViewCell
            return cell
        }
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collection1{
            
            
            let nam = self.image_array[indexPath.row] as! Dictionary<String,Any>
            let naa = nam["destination_type"] as! String
            let noo = nam["destination_id"] as! String
            if naa == "common"{
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
                
                self.present(newViewController, animated: false, completion: nil)
                
                
            }
            if naa == "category"{
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food") as! foodmenu_ViewController
                newViewController.destination_id = "\(noo)" as String
            self.present(newViewController, animated: false, completion: nil)

                           
                       }
            
            if naa == "specific"{
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
                newViewController.cake_id = "\(noo)"
                self.present(newViewController, animated: false, completion: nil)
                
            }
            
        }
        
        if collectionView == collection2{
            
            
            var ind = 0
            
            if popular_dishes.count > 5{
                ind = 5
                
            }
            else{
                ind = popular_dishes.count
            }
            
            
            
            
            if indexPath.row == ind
            {
              
                                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
                
                                self.present(newViewController, animated: false, completion: nil)
                
                
                

            }
            else{
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
                newViewController.cake_id = "\((((popular_dishes.value(forKey: "id") as! NSArray).object(at: indexPath.row)) ))"
                self.present(newViewController, animated: false, completion: nil)
                
            }
            
        }
        else if collectionView == collection3
        {
            if indexPath.row == 2
            {
                
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "showonmap")
                //as! more_outlets_ViewController
                
                self.present(newViewController, animated: false, completion: nil)
            }
            else{
                
                if let datt = self.data3["data"] as? NSArray{
                    print(datt)
                    print(datt[indexPath.row])
                    let posi = datt[indexPath.row] as! Dictionary<String,Any>
                    print(posi)
                    let lati = posi["latitude"] as! String
                    let longi = posi["longitude"] as! String
                    
                    if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                        UIApplication.shared.openURL(NSURL(string:
                            "comgooglemaps://?saddr=&daddr=\(lati),\(longi)&directionsmode=driving")! as URL)
                        
                    } else {
                        NSLog("Can't use comgooglemaps://");
                    }
                    
                }
               
            }
            
            
        }
        else if collectionView == collection1
        {
            
            
        }
        
    }
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func account_click(_ sender: Any) {
        
      /*  let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal4") //as! book_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    
    
    
    
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        newViewController.check_name = "favorite"
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    
    
}


extension UIImageView
{
    func roundCornersForAspectFit(radius: CGFloat)
    {
        if let image = self.image {
            
            //calculate drawingRect
            let boundsScale = self.bounds.size.width / self.bounds.size.height
            let imageScale = image.size.width / image.size.height
            
            var drawingRect: CGRect = self.bounds
            
            if boundsScale > imageScale {
                drawingRect.size.width =  drawingRect.size.height * imageScale
                drawingRect.origin.x = (self.bounds.size.width - drawingRect.size.width) / 2
            } else {
                drawingRect.size.height = drawingRect.size.width / imageScale
                drawingRect.origin.y = (self.bounds.size.height - drawingRect.size.height) / 2
            }
            let path = UIBezierPath(roundedRect: drawingRect, cornerRadius: radius)
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}

