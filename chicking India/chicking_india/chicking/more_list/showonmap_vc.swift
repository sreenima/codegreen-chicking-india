//
//  showonmap_vc.swift
//  LaBrioche
//
//  Created by CGT on 28/09/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import UIKit
import MapKit

class showonmap_vc: UIViewController,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    @IBOutlet weak var cart_pop: UIButton!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! showonmap_cell
        
        let dat = cities[indexPath.row] as! Dictionary<String,Any>
        cell.lbl.text = "\(dat["title"]!)"
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    class ArtworkView: MKAnnotationView {
        override var annotation: MKAnnotation? {
            willSet {
                guard let artwork = newValue as? Artwork else {return}
                canShowCallout = true
                calloutOffset = CGPoint(x: -5, y: 5)
                rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                
                if let imageName = artwork.imageName {
                    image = #imageLiteral(resourceName: "Chicking Map ICON-01 (1)")
                    
                } else {
                    image = nil
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        
        
        
        dropdown_status = false
        //let cell = tableView.cellForRow(at: indexPath) as! showonmap_cell
        self.city_lbl.textColor = .white
        let dat = cities[indexPath.row] as! Dictionary<String,Any>
        self.city_lbl.text = "\(dat["title"]!)"
        self.city_lbl.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        
        let lat = "\(dat["latitude"]!)"
        let lon = "\(dat["longitude"]!)"
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.tab_height.constant = 0
            self.view.layoutIfNeeded()
        })
        
        
        let initialLocation = CLLocation(latitude:Double(lat)!, longitude: Double(lon)!)
        
        self.centerMapOnLocation(location: initialLocation)
        
        self.mapView.reloadInputViews()
        
        
        
    }
    
    
    
    @IBAction func home(_ sender: Any) {
    }
    
    @IBAction func back(_ sender: Any) {
        
        
        
        self.dismiss(animated: false, completion: nil)
        
        
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    
    
    @IBOutlet var tab_height: NSLayoutConstraint!
    var dropdown_status = false
    @IBOutlet var dropdown_btn: UIButton!
    @IBOutlet var city_lbl: UILabel!
    @IBOutlet var mapView: MKMapView!
    
    
    @IBAction func back_clicked(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cart_clicked(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    @IBAction func dropdown(_ sender: UIButton) {
       print("dropdown")
        
        if dropdown_status == false {
            
            
            dropdown_status = true
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 189
                self.view.layoutIfNeeded()
            })
            
        }
        else{
            dropdown_status = false
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 0
                self.view.layoutIfNeeded()
            })
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    @IBOutlet var tab: UITableView!
    
    
    
    
    
    var artworks: [Artwork] = []
    func loadInitialData() {
        // 1
//        guard let fileName = Bundle.main.path(forResource: "PublicArt", ofType: "json")
//            else { return }
//        let optionalData = try? Data(contentsOf: URL(fileURLWithPath: fileName))
//
//        guard
//            let data = optionalData,
//            // 2
//            let json = try? JSONSerialization.jsonObject(with: data),
//            // 3
//            let dictionary = json as? [String: Any],
            // 4
            let works = userdata_services.instance.data1["data"] as? [[Any]]
           // let works = dictionary["data"] as? [[Any]]
        
        
        
        
//            else { return }
        
        
        
        
        
        
        // 5
        if userdata_services.instance.data1["data"] != nil{
        
        let validWorks = works!.compactMap { Artwork(json: $0) }
            
            
            print("valid works:\(validWorks)")
            
            artworks.append(contentsOf: validWorks)}
    }
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        if userLocation != nil {
//
//
//
//            locationManager.stopUpdatingLocation()
//
//
//            mapView.camera = GMSCameraPosition(target: selectedCoordinate!.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
//        } else if let location = locations.first {
//
//            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
//
//            locationManager.stopUpdatingLocation()
//        }
//
//    }
    
   // var locationManager = CLLocationManager()
    @IBAction func drop_touch(_ sender: UITapGestureRecognizer) {
        
        
        if dropdown_status == false {
            
            
            dropdown_status = true
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 189
                self.view.layoutIfNeeded()
            })
            
        }
        else{
            dropdown_status = false
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 0
                self.view.layoutIfNeeded()
            })
            
            
            
            
        }
        
        
    }
    @IBOutlet var city_view: UIView!
    var cities = NSArray()
    var branches = NSArray()
    @IBOutlet var spinner: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        city_lbl.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        city_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        city_view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        city_view.layer.borderWidth = 0.5
        city_view.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        city_view.layer.cornerRadius = 8
        city_view.clipsToBounds = true
        spinner.startAnimating()
        
        
        
        authentication_services.instance.cities_List(){ (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.show_ciities_data["error"] == nil{
                    
                    if userdata_services.instance.show_ciities_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.show_ciities_data["data"] as! NSArray
                        self.cities = dat
                        
                        self.tab.reloadData()
                        
                
                    }
                    else{
                        self.spinner.stopAnimating()
                        
                        
                       
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_ciities_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        authentication_services.instance.branches_list(cityid: "0"){ (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.show_branch_data["error"] == nil{
                    
                    if userdata_services.instance.show_branch_data["status"]  as! String == "ok"{
                        userdata_services.instance.data1 = userdata_services.instance.show_branch_data
                       
                        let dat  = userdata_services.instance.show_branch_data["data"] as! NSArray
                        print(dat)
                        self.branches = dat
                        
                        var branch_dat = NSMutableArray()
                        for i in 0...self.branches.count-1{
                            
                            var d = self.branches[i] as! Dictionary<String,Any>
                            var d2 = NSMutableArray()
                            d2[0] = d["latitude"]
                            d2[1] = d["longitude"]
                            d2[2] = d["title"]
                            d2[3] = d["address"]
                            d2[4] = d["mbile"]
                            branch_dat[i] = d2 as NSMutableArray
                            
                            
                        }
                        userdata_services.instance.data1 = ["data":branch_dat]
                        print("the data 1 is***********************\(userdata_services.instance.data1)")
                        
                        let initialLocation = self.locationManager.location
                        // let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
                        
                        //        // show artwork on map
                        //        let artwork = Artwork(title: "King David Kalakaua",
                        //                              locationName: "Waikiki Gateway Park",
                        //                              discipline: "Sculpture",
                        //                              coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
                        //        mapView.addAnnotation(artwork)
                        self.mapView.delegate = self
                        
                        self.mapView.register(ArtworkView.self,
                                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
                        
                        //        mapView.register(ArtworkMarkerView.self,
                        //                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
                        if initialLocation != nil{
                            self.centerMapOnLocation(location: initialLocation!)}
                        self.loadInitialData()
                        self.mapView.addAnnotations(self.artworks)
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                       // self.tab.reloadData()
                        
                        
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        
                        
                        
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_branch_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        
        
        
        
//        // Do any additional setup after loading the view.
//
//        // set initial location in Honolulu
//        let initialLocation = locationManager.location
//      // let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
//
////        // show artwork on map
////        let artwork = Artwork(title: "King David Kalakaua",
////                              locationName: "Waikiki Gateway Park",
////                              discipline: "Sculpture",
////                              coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
////        mapView.addAnnotation(artwork)
//        mapView.delegate = self
//
//        mapView.register(ArtworkView.self,
//                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//
////        mapView.register(ArtworkMarkerView.self,
////                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//        if initialLocation != nil{
//            centerMapOnLocation(location: initialLocation!)}
//        loadInitialData()
//        mapView.addAnnotations(artworks)
        
    }
    let regionRadius: CLLocationDistance = 18000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    
    
    
    
    
    
    
    
    let locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension showonmap_vc: MKMapViewDelegate {
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
    
       
      
        print("\(latitude)")
        print("\(longitude)")
//
//        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
//        location.mapItem().openInMaps(launchOptions: launchOptions)
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")! as URL)

        } else {
            //                    UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/@10.0261,76.3071,6z")!)
            NSLog("Can't use comgooglemaps://");
        }
    }
    
    // 1
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        // 2
//        guard let annotation = annotation as? Artwork else { return nil }
//        // 3
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        // 4
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            // 5
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = true
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
}


class showonmap_cell : UITableViewCell{
    
    
    
    @IBOutlet var lbl: UILabel!
    
    
}

