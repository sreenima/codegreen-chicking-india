//
//  more_outlets_new_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 16/10/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import MapKit
class more_outlets_new_ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var a = NSInteger()
    @IBOutlet weak var mapView: MKMapView!
   
    @IBOutlet weak var cart_pop: UIButton!
    var locationManager = CLLocationManager()
   
    @IBOutlet weak var height_table: NSLayoutConstraint!
    @IBOutlet weak var B_B: UIButton!
    @IBOutlet weak var city_lbl: UILabel!
    
    @IBOutlet weak var down_arrow: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        B_B.layer.borderWidth = 1
        B_B.layer.borderColor = #colorLiteral(red: 0.8235294118, green: 0, blue: 0, alpha: 1)
        B_B.layer.cornerRadius = 5
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
       locationManager.delegate = self
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        // Do any additional setup after loading the view.
    }
    }
    @IBAction func back_clicked(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! more_outlets_new_TableViewCell
        
        return cell
    }
    
    
    
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
            let location = locations.last as! CLLocation
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            region.center = self.mapView.userLocation.coordinate
            self.mapView.setRegion(region, animated: true)
        }
    @IBAction func big_b_Clicked(_ sender: Any) {
        if a == 0{
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.height_table.constant = 120
                self.view.layoutIfNeeded()
         
        })
                 a = 1
        }
    
        else{
            
            
            a = 0
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.height_table.constant = 0
                self.view.layoutIfNeeded()
        })
    }
    }
    
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
        /*let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        self.present(newViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
        
        
        
        
        
        
        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

