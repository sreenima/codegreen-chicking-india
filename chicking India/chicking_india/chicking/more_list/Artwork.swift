//
//  Artwork.swift
//  LaBrioche
//
//  Created by CGT on 28/09/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        
        
      //  if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
        let url = URL(string:"https://www.google.com/maps/@\(location.coordinate.latitude),\(location.coordinate.longitude)")


//            UIApplication.shared.open(NSURL(string:
//                "comgooglemaps://?saddr=&daddr=\(location.coordinate.latitude),\(location.coordinate.longitude)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
            
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)

//            UIApplication.shared.openURL(NSURL(string:
//                "comgooglemaps://?saddr=&daddr=\(location.coordinate.latitude),\(location.coordinate.longitude)&directionsmode=driving")! as URL)
//
//        } else {
//            NSLog("Can't use comgooglemaps://");
//        }
    
        
        
//        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
//        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    init?(json: [Any]) {
        
        
//        self.title = json[16] as? String ?? "No Title"
//        //        self.locationName = json[12] as! String
//        self.locationName = json[11] as! String
//        self.discipline = json[15] as! String
//        // 2
//        if let latitude = Double(json[18] as! String),
//            let longitude = Double(json[19] as! String)
        
        
        
        
        
        
        // 1
        self.title = json[2] as? String
//        self.locationName = json[12] as! String
        self.locationName = json[3] as! String
        self.discipline = "diciplin"
        // 2
        if let latitude = Double(json[0] as! String),
            let longitude = Double(json[1] as! String)
        
        
        
        
        {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
    
    
    var imageName: String? {
        if discipline == "Sculpture" { return "Statue" }
        return "Flag"
    }
    
    
   
    
    
    // markerTintColor for disciplines: Sculpture, Plaque, Mural, Monument, other
    var markerTintColor: UIColor  {
        switch discipline {
        case "Monument":
            return .red
        case "Mural":
            return .cyan
        case "Plaque":
            return .blue
        case "Sculpture":
            return .purple
        default:
            return .green
        }
    }
    
    
}
