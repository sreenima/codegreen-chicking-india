//
//  more_popular_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class more_popular_ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
   
    
    
    
    
    
    var popular_dishes = NSArray()
    
    var imageCache2 = NSCache<NSString,UIImage>()
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collection1.frame.size.width/2.2), height: 160)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        
        
        
        
        
        
        
        
        
        authentication_services.instance.popular_dishes(){ (success) in
            if success {
                //self.spinner2.stopAnimating()
                if userdata_services.instance.popular_dishes["error"]  == nil{
                    
                    
                    if userdata_services.instance.popular_dishes["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.popular_dishes["data"] as! NSArray
                        
                        self.popular_dishes = data1
                        
                        self.collection1.reloadData()
                        
                        
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.popular_dishes["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                //self.spinner2.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //cell arrangement design #######################################################
        
//
//        let layout2: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//
//        layout2.sectionInset = UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
//        layout2.itemSize = CGSize(width: collection1.frame.width/2.1, height: 160)
//        layout2.minimumInteritemSpacing = 5
//        layout2.minimumLineSpacing = 30
//
//        collection1.collectionViewLayout = layout2
//
        //cell arrangement design #######################################################
        
        
        
    }

   
    
    


    @IBAction func back_clicked(_ sender: Any) {
        
        
       
  dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func cart_clicked(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popular_dishes.count
        
    }
    
    @IBOutlet weak var collection1: UICollectionView!
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
//        let layout2: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//
//        layout2.sectionInset = UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
//        layout2.itemSize = CGSize(width: collection1.frame.width/2.1, height: 160)
//        layout2.minimumInteritemSpacing = 5
//        layout2.minimumLineSpacing = 20
//        
//        collection1.collectionViewLayout = layout2
        
        
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll1", for: indexPath) as! more_popular_CollectionViewCell
        
       
        cell.layer.cornerRadius = 12
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false
        //   cell.layer.shadowPath = UIBezierPath(roundedRect: cell.layer.bounds, cornerRadius:  cell.layer.cornerRadius).cgPath
        cell.layer.backgroundColor = UIColor.white.cgColor
        
        
        
        
        
        
        
        cell.lbl.text = (((popular_dishes.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
        
        let img_name = (((popular_dishes.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        
        if let cacheImage = imageCache2.object(forKey: img_name as NSString ){
            print("image cache")
            cell.img.image = cacheImage
            //                    cell.collection1_img.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1571061644)
            //                    cell.collection1_img.layer.borderWidth = 5
            //                    cell.collection1_img.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1865903253)
            cell.img.clipsToBounds = true
            //return
        }
        else{
            print("image not cache")
            //self.spinner.startAnimating()
            print("\(IMAGE_URL)\(String(describing: img_name))")
            
            Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                //debugPrint(response)
                //self.spinner.stopAnimating()
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.img.image = image
                    //                            cell.collection1_img.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1571061644)
                    //                            cell.collection1_img.layer.borderWidth = 5
                    //                            cell.collection1_img.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1865903253)
                    cell.img.clipsToBounds = true
                    self.imageCache2.setObject(image, forKey: img_name as NSString)
                    //      }
                }
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
    return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data1 : NSArray = userdata_services.instance.popular_dishes["data"] as! NSArray
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
        
        newViewController.cake_id = "\(((data1.value(forKey: "id") as! NSArray).object(at: indexPath.row)) )"
        
        self.present(newViewController, animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        self.present(newViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
         newViewController.check_name = "favorite"
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
