//
//  Forgot_passViewController.swift
//  chicking
//
//  Created by Ios CGT on 16/10/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class Forgot_passViewController: UIViewController {
    @IBOutlet weak var BG_view2: UIView!
    @IBOutlet weak var pass1_txt: UITextField!
    @IBOutlet weak var pass2_txt: UITextField!
    @IBOutlet weak var continue_btn: UIButton!
    @IBOutlet var otp_txt: UITextField!
    var userid = String()
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    @IBAction func continue_action( _ sender: Any) {
        
        
        
        
        
        
        
        if otp_txt.text != "" && pass1_txt.text != "" && pass2_txt.text  != "" {
            
            
            if pass1_txt.text == pass2_txt.text{
                
                let md5Data = MD5(string: self.pass1_txt.text!)
                
                let  md5_pass =  md5Data.map { String(format: "%02hhx", $0) }.joined()
                
         
                
                spinner.startAnimating()
                
                        
                authentication_services.instance.forgot_reset_pwd(otp: otp_txt.text!, password: md5_pass, userid: userid) { (success) in
                    if success {
                        
                        if userdata_services.instance.forgot_reset_pwd_data["status"] as! String == "ok"{
                            
                            self.spinner.stopAnimating()
                            ////////////////
                            
                            authentication_services.instance.token = userdata_services.instance.forgot_reset_pwd_data["token"]  as! String
                            
                            BEARER_HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            BEARER_2HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            //var data1 : Dictionary<String,Any> = userdata_services.instance.login_data["data"] as! Dictionary<String, Any>
                            authentication_services.instance.isloggedin = true

                            
                            /////////////
                            authentication_services.instance.isloggedin = true
                            authentication_services.instance.token = userdata_services.instance.forgot_reset_pwd_data["token"] as! String
                            
                            print(userdata_services.instance.forgot_reset_pwd_data)
                            
                            
                            BEARER_HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            
                            authentication_services.instance.userid = self.userid
                            
                            let alert2 = UIAlertController(title: "", message: "Password updated successfully.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                                    
                                   //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                                    
                                    self.present(newViewController, animated: false, completion: nil)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                }
                            }))
                            self.present(alert2, animated: true, completion: nil)
                            
                            
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.forgot_reset_pwd_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                        
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                
                
                
                
                
            }
            else{
                let alert2 = UIAlertController(title: "", message: "Passwords are not matching", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        else{
            
            
            let alert2 = UIAlertController(title: "", message: "Enter all fields", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            
        }
        
        
        
        
    
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        BG_view2.layer.cornerRadius = 15.0
        BG_view2.layer.shadowColor = UIColor.black.cgColor
        BG_view2.layer.shadowOpacity = 0.2
        BG_view2.layer.shadowOffset = CGSize(width: -1, height: 1)
        BG_view2.layer.shadowRadius = 3
       // BG_view2.layer.shouldRasterize = true
        
        continue_btn.layer.cornerRadius = 10.0
 let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        // Do any additional setup after loading the view.
        view.addGestureRecognizer(tap)
        
       
        pass1_txt.layer.borderWidth = 0.5
         pass1_txt.layer.cornerRadius = 8


         pass1_txt.layer.borderColor = UIColor.red.cgColor
         pass1_txt.layer.cornerRadius = 8
        
        otp_txt.layer.borderWidth = 0.5
        otp_txt.layer.cornerRadius = 8
        
        
        otp_txt.layer.borderColor = UIColor.red.cgColor
        otp_txt.layer.cornerRadius = 8
        
        
       
        pass2_txt.layer.borderWidth = 0.5
        pass2_txt.layer.cornerRadius = 8
        
       
        pass2_txt.layer.borderColor = UIColor.red.cgColor
        pass2_txt.layer.cornerRadius = 8
        
        addPaddingAndBorder(to: otp_txt)
        addPaddingAndBorder(to: pass2_txt)
        addPaddingAndBorder(to: pass1_txt)
        
    }
    
    
    func addPaddingAndBorder(to textfield: UITextField) {
    
        
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 15.0, height: 2.0))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
    
    
    
    
    
    @IBAction func back_click(_ sender: Any) {
        
         dismiss(animated: false, completion: nil)
    }
    
    @IBAction func continue_click(_ sender: Any) {
//        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//        //      present(dashboardWorkout, animated: false, completion: nil)
//        self.present(newViewController, animated: false, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
