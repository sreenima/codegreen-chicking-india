//
//  verify_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 13/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class verify_ViewController: UIViewController, UITextFieldDelegate {
    var userid = String()
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var OTP1_txtfld: UITextField!
    @IBOutlet weak var OTP2_txtfld: UITextField!
    @IBOutlet weak var OTP3_txtfld: UITextField!
    @IBOutlet weak var OTP4_txtfld: UITextField!
    @IBOutlet weak var Done_btn: UIButton!
    @IBOutlet weak var resend_btn: UIButton!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        OTP1_txtfld.delegate = self
        OTP2_txtfld.delegate = self
        OTP3_txtfld.delegate = self
        OTP4_txtfld.delegate = self
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.OTP1_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP2_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP3_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP4_txtfld.keyboardType = UIKeyboardType.decimalPad
        
        
        OTP1_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP2_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP3_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP4_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case OTP1_txtfld:
                OTP2_txtfld.becomeFirstResponder()
            case OTP2_txtfld:
                OTP3_txtfld.becomeFirstResponder()
            case OTP3_txtfld:
                OTP4_txtfld.becomeFirstResponder()
            case OTP4_txtfld:
                OTP4_txtfld.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case OTP1_txtfld:
                OTP1_txtfld.becomeFirstResponder()
            case OTP2_txtfld:
                OTP1_txtfld.becomeFirstResponder()
            case OTP3_txtfld:
                OTP2_txtfld.becomeFirstResponder()
            case OTP4_txtfld:
                OTP3_txtfld.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    @IBAction func back_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "register") as! register_ViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        //      present(dashboardWorkout, animated: false, completion: nil)
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func done_click(_ sender: Any) {
        
        
        if OTP1_txtfld.text?.count == 1 && OTP2_txtfld.text?.count == 1 && OTP3_txtfld.text?.count == 1 && OTP4_txtfld.text?.count == 1{
            
            
            var otp = "\(OTP1_txtfld.text!)\(OTP2_txtfld.text!)\(OTP3_txtfld.text!)\(OTP4_txtfld.text!)"
            
            
            
            if otp == "" ||  otp.count != 4{
                let alert2 = UIAlertController(title: "", message: "Enter OTP correctly.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            else{
                self.spinner.startAnimating()
                authentication_services.instance.verify_otp_register(otp: otp, userid: self.userid){ (success) in
                    if success {
                        self.spinner.stopAnimating()
                        if userdata_services.instance.verify_otp_register_data["status"]  as! String == "ok"{
                            authentication_services.instance.token = userdata_services.instance.verify_otp_register_data["token"]  as! String
                            BEARER_HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            
                            BEARER_2HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            var data1 : Dictionary<String,Any> = userdata_services.instance.verify_otp_register_data["data"] as! Dictionary<String, Any>
                            authentication_services.instance.isloggedin = true
                            authentication_services.instance.mobile = data1["mobileno"] as! String
                            authentication_services.instance.userid = "\(data1["id"]!)"
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.verify_otp_register_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                                    
                                    
                                    
                                    self.present(newViewController, animated: false, completion: nil)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                }
                            }))
                            self.present(alert2, animated: true, completion: nil)
                            
                            
                            
                        }
                        else{
                            //self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.verify_otp_register_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
            }
            
        }
        
        
    }
    
    
    @IBAction func resend_click(_ sender: Any) {
        
        
        // self.spinner.startAnimating()
        authentication_services.instance.alreadyreg_user_otp(userid:userid ){ (success) in
            if success {
                // self.spinner.stopAnimating()
                if userdata_services.instance.registered_user_otp_data["status"]  as! String == "otpsent"{
                    
                    
                    let alert2 = UIAlertController(title: "", message: "OTP send to your mobile number", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("default")
                            
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                        }
                    }))
                    self.present(alert2, animated: true, completion: nil)
                    
                    
                    
                }
                else{
                    // self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.registered_user_otp_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                }
                
            }
            else{
                // self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
         
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
