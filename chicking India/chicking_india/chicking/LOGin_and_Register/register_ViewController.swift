//
//  register_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 13/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit


class register_ViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var cc = "2"
    
    @IBOutlet weak var email_txt_fld: UITextField!
    var counry_id = ""
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet weak var view_txt: UIView!
    @IBOutlet weak var c_btn: UIButton!
    @IBOutlet weak var name_txtfld: UITextField!
    @IBOutlet weak var phone_txtfld: UITextField!
    @IBOutlet weak var email_txtfld: UITextField!
    @IBOutlet weak var pwdt_xtfld: UITextField!
    @IBOutlet weak var register_btn: UIButton!
    @IBOutlet weak var BG_view: UIView!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var login_btn: UIButton!
    @IBOutlet weak var tabl_view: UITableView!
    var countrycodes = NSArray()
    var tap = UITapGestureRecognizer()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email_txt_fld.delegate = self
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        tabl_view.layer.borderWidth = 1
        tabl_view.layer.borderColor = #colorLiteral(red: 0.7684356182, green: 0, blue: 0.007193906408, alpha: 1)
        
        
        
        spinner.startAnimating()
        
        
        authentication_services.instance.countrycodes() { (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.countrycode_data["error"] == nil{
                    if userdata_services.instance.countrycode_data["status"] as! String == "ok"{
                        self.spinner.stopAnimating()
                        self.countrycodes = userdata_services.instance.countrycode_data["data"] as! NSArray
                        
                        
                        if self.countrycodes.count>0{
                            
                            self.c_btn.titleLabel?.text = "\(((self.countrycodes.value(forKey: "country_code") as! NSArray).object(at: 0)))"
                            self.counry_id = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: 0)))"
                            self.cc = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: 0)))"
                            
                        }
                        
                        self.tabl_view.reloadData()
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.countrycode_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                    
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            
        }
        
        
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        
        view.addGestureRecognizer(tap)
        
        
        // Design for the view
        
        BG_view.layer.cornerRadius = 15.0
        BG_view.layer.shadowColor = UIColor.black.cgColor
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        BG_view.layer.shadowRadius = 3
        
        
        register_btn.layer.cornerRadius = 10.0
        
        
        name_txtfld.layer.borderWidth = 0.5
        view_txt.layer.borderWidth = 0.5
        
        pwdt_xtfld.layer.borderWidth = 0.5
        
        name_txtfld.layer.cornerRadius = 8
        view_txt.layer.cornerRadius = 8
        
        pwdt_xtfld.layer.cornerRadius = 8
        
        name_txtfld.layer.borderColor = UIColor.red.cgColor
        view_txt.layer.borderColor = UIColor.red.cgColor
        
        pwdt_xtfld.layer.borderColor = UIColor.red.cgColor
        
        email_txt_fld.layer.borderWidth = 0.5
        email_txt_fld.layer.borderColor = UIColor.red.cgColor
        email_txt_fld.layer.cornerRadius = 8
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == email_txt_fld{
            
            if  email_txt_fld.text!.isValidEmail(){
                
                print("valid email")
                
            }else{
                
                let alert2 = UIAlertController(title: "", message: "Invalid Email.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countrycodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! register_TableViewCell
        
        
        cell.lbl.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        self.counry_id = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: indexPath.row)) )"
        //lb.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.addGestureRecognizer(tap)
        
        if height_view.constant == 0 {
            height_view.constant = 70
        }
        else
        {
            height_view.constant = 0
            
            
        }
        
        cc =  "\(((countrycodes.value(forKey: "id") as! NSArray).object(at: indexPath.row)))"
        
        
        c_btn.titleLabel!.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)))"
    }
    
    
    @IBOutlet weak var height_view: NSLayoutConstraint!
    
    @IBAction func C_C_clicked(_ sender: Any) {
        
        if height_view.constant == 0 {
            view.removeGestureRecognizer(tap)
            height_view.constant = 70
        }
        else
        {
            view.addGestureRecognizer(tap)
            height_view.constant = 0
            
            
        }
        
        
    }
    
    
    
    @IBOutlet weak var scroll: UIScrollView!
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        scroll.isScrollEnabled = true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        scroll.isScrollEnabled = false
    }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    
    
    @IBAction func register_now_click(_ sender: Any) {
        
        
        if  email_txt_fld.text!.isValidEmail(){
            
            print("valid email")
            
            if name_txtfld.text != "" &&  pwdt_xtfld.text != "" && phone_txtfld.text != ""   && phone_txtfld.text!.count > 8 &&  phone_txtfld.text!.count < 11
                
                // && country_lbl.text != ""
            {
                
                
                if self.pwdt_xtfld.text == self.pwdt_xtfld.text {
                    self.spinner.startAnimating()
                    
                    let md5Data = MD5(string: self.pwdt_xtfld.text!)
                    
                    let  md5_pass =  md5Data.map { String(format: "%02hhx", $0) }.joined()
                    
                    authentication_services.instance.register_user(name: name_txtfld.text!, password: md5_pass, mobile: phone_txtfld.text!,country:self.cc, email:self.email_txt_fld.text!) { (success) in
                        if success {
                            
                            if userdata_services.instance.register_data["status"] as! String == "ok"{
                                self.spinner.stopAnimating()
                                
                                
                                if userdata_services.instance.register_data["message"] as! String == "Mobile number already exists but not verified"{
                                    
                                    
                                    let user_id1 = "\(userdata_services.instance.register_data["data"]!)"
                                    
                                    
                                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.register_data["message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("default")
                                            
                                            
                                            
                                            
                                            self.spinner.startAnimating()
                                            authentication_services.instance.alreadyreg_user_otp(userid:"\(userdata_services.instance.register_data["data"]!)" ){ (success) in
                                                if success {
                                                    self.spinner.stopAnimating()
                                                    if userdata_services.instance.registered_user_otp_data["status"]  as! String == "otpsent"{
                                                        
                                                        
                                                        let alert2 = UIAlertController(title: "", message: "OTP send to your mobile number", preferredStyle: UIAlertControllerStyle.alert)
                                                        alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                            switch action.style{
                                                            case .default:
                                                                print("default")
                                                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "otp") as! verify_ViewController
                                                                
                                                                newViewController.userid = "\(user_id1)"
                                                                
                                                                self.present(newViewController, animated: false, completion: nil)
                                                                
                                                            case .cancel:
                                                                print("cancel")
                                                                
                                                            case .destructive:
                                                                print("destructive")
                                                                
                                                            }
                                                        }))
                                                        self.present(alert2, animated: true, completion: nil)
                                                        
                                                        
                                                        
                                                    }
                                                    else{
                                                        self.spinner.stopAnimating()
                                                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.registered_user_otp_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                        self.present(alert2, animated: true, completion: nil)
                                                    }
                                                    
                                                }
                                                else{
                                                    self.spinner.stopAnimating()
                                                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                    self.present(alert2, animated: true, completion: nil)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        case .cancel:
                                            print("cancel")
                                            
                                        case .destructive:
                                            print("destructive")
                                            
                                        }
                                    }))
                                    self.present(alert2, animated: true, completion: nil)
                                  
                                }
                                    
                                    
                                else{
                                    
                                    
                                    let alert2 = UIAlertController(title: "", message: "OTP send to your mobile number.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style{
                                        case .default:
                                            print("default")
                                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "otp") as! verify_ViewController
                                            
                                            newViewController.userid = "\(userdata_services.instance.register_data["user_id"]!)"
                                            
                                            self.present(newViewController, animated: false, completion: nil)
                                            
                                        case .cancel:
                                            print("cancel")
                                            
                                        case .destructive:
                                            print("destructive")
                                            
                                        }
                                    }))
                                    self.present(alert2, animated: true, completion: nil)}
                                
                            }
                            else{
                                self.spinner.stopAnimating()
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.register_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                                
                            }
                            
                            
                            
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                }
                else {
                    
                    let alert1 = UIAlertController(title: "", message: "Passwords are not matching.", preferredStyle: UIAlertControllerStyle.alert)
                    alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert1, animated: true, completion: nil)
                    
                    
                }
            }
                
                
            else{
                
                let alert2 = UIAlertController(title: "", message: "Enter all fields correctly.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
                
            }
            
        }else{
            
            let alert2 = UIAlertController(title: "", message: "Enter A Valid Email.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            
            
            
        }
        
        
    }
    
    
    @IBAction func continue_without_clicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
        
        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    
    @IBAction func login_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        //      present(dashboardWorkout, animated: false, completion: nil)
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func back_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        //      present(dashboardWorkout, animated: false, completion: nil)
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
