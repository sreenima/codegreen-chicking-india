//
//  log_in_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 13/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class log_in_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate  {
    
    var password_changed = String()
    @IBOutlet weak var tabl_view: UITableView!
    @IBOutlet weak var C_code_btn: UIButton!
    @IBOutlet weak var tbl_height: NSLayoutConstraint!
    @IBOutlet weak var text_BG: UIView!
    @IBOutlet weak var phone_number_txtfld: UITextField!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var password_txtfld: UITextField!
    
    @IBOutlet weak var eye_password_btn: UIButton!
    
    @IBOutlet weak var forgot_pwd_btn: UIButton!
    
    @IBOutlet weak var signin_btn: UIButton!
    
    @IBOutlet weak var BG_view: UIView!
    
    var tap = UITapGestureRecognizer()
    var instanceIDTokenMessage = String()
    var countrycodes = NSArray()
    var device_token = String()
    @IBOutlet weak var register_btn: UIButton!
    let locationManager = CLLocationManager()
    var a = NSInteger()
    
    let device = UIDevice.current
    
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            print("yes")
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.instanceIDTokenMessage  = "Remote InstanceID token: \(result.token)"
                self.device_token = "\(result.token)"
            }
        }
        
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        spinner.startAnimating()
        
        
        authentication_services.instance.countrycodes() { (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.countrycode_data["error"] == nil{
                    if userdata_services.instance.countrycode_data["status"] as! String == "ok"{
                        self.spinner.stopAnimating()
                        self.countrycodes = userdata_services.instance.countrycode_data["data"] as! NSArray
                        
                        
                        if self.countrycodes.count>0{
                            
                            self.C_code_btn.titleLabel?.text = "\(((self.countrycodes.value(forKey: "country_code") as! NSArray).object(at: 0)) )"
                            //self.counry_id = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: 0)) )"
                            
                        }
                        
                        self.tabl_view.reloadData()
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.countrycode_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            
        }
        
        
        
        
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        
        view.addGestureRecognizer(tap)
        
        tabl_view.layer.borderWidth = 1
        tabl_view.layer.borderColor = #colorLiteral(red: 0.7684356182, green: 0, blue: 0.007193906408, alpha: 1)
        
        // Design for the view
        
        BG_view.layer.cornerRadius = 15.0
        BG_view.layer.shadowColor = UIColor.black.cgColor
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        BG_view.layer.shadowRadius = 3
        //            BG_view.layer.shouldRasterize = true
        
        signin_btn.layer.cornerRadius = 10.0
        //*********************************************************************************
        
        // design for the text field
        
        text_BG.layer.borderWidth = 0.5
        password_txtfld.layer.borderWidth = 0.5
        text_BG.layer.cornerRadius = 8
        
        text_BG.layer.borderColor = UIColor.red.cgColor
        password_txtfld.layer.borderColor = UIColor.red.cgColor
        password_txtfld.layer.cornerRadius = 8
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countrycodes.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! login_table_TableViewCell
        
        
        
        
        cell.lbl.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        C_code_btn.titleLabel!.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        
        if a == 0{
            view.removeGestureRecognizer(tap)
            tbl_height.constant = 70
            a = 1
        }
        else{
            view.addGestureRecognizer(tap)
            tbl_height.constant = 0
            a = 0
            
        }
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func C_btn_clicked(_ sender: Any) {
        
        if a == 0{
            view.removeGestureRecognizer(tap)
            tbl_height.constant = 70
            a = 1
        }
        else{
            view.addGestureRecognizer(tap)
            tbl_height.constant = 0
            a = 0
            
        }
    }
    
    
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    @IBAction func holdrelese(_ sender: Any)
    {
        password_txtfld.isSecureTextEntry = true
    }
    
    @IBAction func holddown(_ sender: Any)
    {
        password_txtfld.isSecureTextEntry = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    
    
    @IBAction func Continue_without_login(_ sender: Any) {
        
        
        authentication_services.instance.isloggedin = false
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
        
        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    //device_information
    
    
    @IBAction func signin_click(_ sender: Any) {
        
        
        if self.phone_number_txtfld.text == "" ||   self.password_txtfld.text == "" || phone_number_txtfld.text!.count < 9 ||  (phone_number_txtfld.text?.count)! > 10 {
            let alert2 = UIAlertController(title: "", message: "Enter all fields correctly.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        }
        else{
            self.spinner.startAnimating()
            
            
            
            let md5Data = MD5(string: self.password_txtfld.text!)
            
            let  md5_pass =  md5Data.map { String(format: "%02hhx", $0) }.joined()
            authentication_services.instance.login(mobile: self.phone_number_txtfld.text!, password: md5_pass, value: device_token){ (success) in
                if success {
                    
                    print(self.device_token)
                    self.spinner.stopAnimating()
                    if userdata_services.instance.login_data["status"]  as! String == "ok"{
                        var data1 : Dictionary<String,Any> = userdata_services.instance.login_data["data"] as! Dictionary<String, Any>
                        if data1["status"] as! String == "active"{
                            
                            authentication_services.instance.token = userdata_services.instance.login_data["token"]  as! String
                            
                            BEARER_HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            BEARER_2HEADER = [
                                "Authorization":"Bearer \(authentication_services.instance.token)",
                                "Content-Type": "application/json; charset=utf-8"
                            ]
                            //var data1 : Dictionary<String,Any> = userdata_services.instance.login_data["data"] as! Dictionary<String, Any>
                            authentication_services.instance.isloggedin = true
                            authentication_services.instance.mobile = data1["mobileno"] as! String
                            authentication_services.instance.userid = "\(String(describing: data1["id"]!))"
                            
                            
                            
                            //DEVICE INFO  DEVICE INFO  DEVICE INFO
                            
                            print(UIDevice.modelName)
                            print(self.device.userInterfaceIdiom)
                            print(self.device.systemVersion)
                            print(self.appVersion)
                            
                            self.spinner.startAnimating()
                            authentication_services.instance.device_information(model: UIDevice.modelName, model_number: self.device.localizedModel, os_version: self.device.systemVersion, app_version: self.appVersion!){ (success) in
                                if success {
                                    print("Device info updated")
                                    self.spinner.stopAnimating()
                                    if userdata_services.instance.device_info_data["status"]  as! String == "ok"{
                                        
                                        
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                                        
                                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                                        
                                        self.present(newViewController, animated: false, completion: nil)
                                        
                                        
                                        
                                    }
                                    else{
                                        //self.spinner.stopAnimating()
                                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.device_info_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert2, animated: true, completion: nil)
                                    }
                                    
                                }
                                else{
                                    self.spinner.stopAnimating()
                                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                    
                                }
                                
                            }
                            
                            
                            
                            //DEVICE INFO   DEVICE INFO  DEVICE INFO
                            
                            
                            
                            
                            
                            
                        }
                        else{
                            
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.login_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                    
                                    
                                    var dat = userdata_services.instance.login_data["data"] as! Dictionary<String,Any>
                                    var usr_id = "\(dat["id"]!)"
                                    
                                    
                                    
                                    self.spinner.startAnimating()
                                    authentication_services.instance.alreadyreg_user_otp(userid:usr_id ){ (success) in
                                        if success {
                                            self.spinner.stopAnimating()
                                            if userdata_services.instance.registered_user_otp_data["status"]  as! String == "otpsent"{
                                                
                                                
                                                let alert2 = UIAlertController(title: "", message: "OTP send to your mobile number", preferredStyle: UIAlertControllerStyle.alert)
                                                alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                    switch action.style{
                                                    case .default:
                                                        print("default")
                                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "otp") as! verify_ViewController
                                                        
                                                        newViewController.userid = "\(usr_id)"
                                                        
                                                        self.present(newViewController, animated: false, completion: nil)
                                                        
                                                    case .cancel:
                                                        print("cancel")
                                                        
                                                    case .destructive:
                                                        print("destructive")
                                                        
                                                    }
                                                }))
                                                self.present(alert2, animated: true, completion: nil)
                                                
                                                
                                                
                                            }
                                            else{
                                                self.spinner.stopAnimating()
                                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.registered_user_otp_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                self.present(alert2, animated: true, completion: nil)
                                            }
                                            
                                        }
                                        else{
                                            self.spinner.stopAnimating()
                                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert2, animated: true, completion: nil)
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                }
                            }))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.login_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
        
        
        
        //        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        //
        //        let transition = CATransition()
        //        transition.duration = 0.5
        //        transition.type = kCATransitionPush
        //        transition.subtype = kCATransitionFromRight
        //        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        //        view.window!.layer.add(transition, forKey: kCATransition)
        //        //      present(dashboardWorkout, animated: false, completion: nil)
        //        self.present(newViewController, animated: false, completion: nil)
        
    }
    @IBAction func forgot_click(_ sender: Any) {
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "fpass1") as! forgot_pass_mobile_ViewController
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    @IBAction func register_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "register") as! register_ViewController
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
