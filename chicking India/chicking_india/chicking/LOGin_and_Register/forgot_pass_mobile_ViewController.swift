//
//  forgot_pass_mobile_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 16/10/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit




class forgot_pass_mobile_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var view_txt: UIView!
    @IBOutlet weak var BG_view2: UIView!
    @IBOutlet weak var continue_btn: UIButton!
    @IBOutlet weak var tbl_view: UITableView!
    @IBOutlet weak var pass1: UITextField!
    var countrycodes = NSArray()
    var counry_id = "2"
    var tap = UITapGestureRecognizer()
    @IBOutlet var country_lbl: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        tb_height.constant = 0
        
        tbl_view.layer.borderWidth = 0.5
        tbl_view.layer.borderColor = UIColor.red.cgColor
        
        BG_view2.layer.cornerRadius = 15.0
        BG_view2.layer.shadowColor = UIColor.black.cgColor
        BG_view2.layer.shadowOpacity = 0.2
        BG_view2.layer.shadowOffset = CGSize(width: -1, height: 1)
        BG_view2.layer.shadowRadius = 3
       // BG_view2.layer.shouldRasterize = true
        
        continue_btn.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        // Do any additional setup after loading the view.
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
        view_txt.layer.borderWidth = 0.5
        view_txt.layer.cornerRadius = 8
        
        
        view_txt.layer.borderColor = UIColor.red.cgColor
        view_txt.layer.cornerRadius = 8
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        spinner.startAnimating()
        
        
        
        authentication_services.instance.countrycodes() { (success) in
            if success {
                if userdata_services.instance.countrycode_data["error"] == nil{
                    if userdata_services.instance.countrycode_data["status"] as! String == "ok"{
                        self.spinner.stopAnimating()
                        self.countrycodes = userdata_services.instance.countrycode_data["data"] as! NSArray
                        
                        
                        if self.countrycodes.count>0{
                            
                            self.country_lbl.titleLabel!.text = "\(((self.countrycodes.value(forKey: "country_code") as! NSArray).object(at: 0)) )"
                            self.counry_id = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: 0)) )"
                            
                        }
                        
                        self.tbl_view.reloadData()
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.countrycode_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            
        }
    }
    
    
    @IBOutlet weak var C_code_clicked: UIButton!
    @IBOutlet weak var tb_height: NSLayoutConstraint!
    @IBAction func back_click(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countrycodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! forgot_c_cellTableViewCell
        
        cell.lbl.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //view.addGestureRecognizer(tap)
       
        
        self.counry_id = "\(((self.countrycodes.value(forKey: "id") as! NSArray).object(at: indexPath.row)) )"
        country_lbl.titleLabel!.text = "\(((countrycodes.value(forKey: "country_code") as! NSArray).object(at: indexPath.row)) )"
        
        tb_height.constant = 0
    }
    
    @IBAction func c_code_click(_ sender: Any) {
        if tb_height.constant == 0
        {
            
          
        tb_height.constant = 80
    }
    else {
        tb_height.constant = 0
//            view.addGestureRecognizer(tap)
    }
    }
    
    @IBAction func continue_click(_ sender: Any) {
             
        if self.pass1.text == "" || pass1.text!.count < 9 ||  pass1.text!.count > 10{
            let alert2 = UIAlertController(title: "", message: "Enter Mobile number correctly.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        }
        else{
            self.spinner.startAnimating()
            authentication_services.instance.forgot_pwd(mobile: pass1.text!,coutry_id:counry_id){ (success) in
                if success {
                    self.spinner.stopAnimating()
                    if userdata_services.instance.forgot_pwd_data["status"]  as! String == "ok"{
                        authentication_services.instance.userid = "\(userdata_services.instance.forgot_pwd_data["user_id"]!)"
                        
                        let alert2 = UIAlertController(title: "", message: "OTP send to your mobile number", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "fpass") as! Forgot_passViewController
                                
                                newViewController.userid = "\(userdata_services.instance.forgot_pwd_data["user_id"]! )"
                                
                                self.present(newViewController, animated: false, completion: nil)
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                            }
                        }))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.forgot_pwd_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//        
//        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "otp2") as! forget_verify_ViewController
//        
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromRight
//        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//        //      present(dashboardWorkout, animated: false, completion: nil)
//        self.present(newViewController, animated: false, completion: nil)
    }
   
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    /*
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
