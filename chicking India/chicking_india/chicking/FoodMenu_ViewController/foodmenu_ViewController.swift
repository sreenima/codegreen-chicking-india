//
//  foodmenu_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 14/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import SWRevealViewController
import AlamofireImage
import Alamofire

class foodmenu_ViewController: UIViewController ,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var cart_pop: UIButton!
    @IBOutlet weak var collection1: UICollectionView!
    @IBOutlet weak var collection2: UICollectionView!
    @IBOutlet weak var collection3: UICollectionView!
    @IBOutlet weak var foodmenu_icon: UIButton!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var height_colection3: NSLayoutConstraint!
    @IBOutlet weak var height_col: NSLayoutConstraint!
    
    var image_array = NSArray()
    var image_array2 = NSArray()
    var image_array3 = NSArray()
    var dict = Dictionary <String , Any>()
    var destination_id = String()
    var id_array = [NSInteger]()

    
    let imageCache1 = NSCache<NSString, UIImage>()
    let imageCache2 = NSCache<NSString, UIImage>()
    let imageCache3 = NSCache<NSString, UIImage>()
    var scrolling_index = 0
    var cell_count = NSInteger()
    var isload = false
    
    var item = ["name":"","price":"","image":"","id":"","quantity":"1"]
    var cellStatus:NSMutableDictionary = NSMutableDictionary()
    var cellSelectionDict = Dictionary<Int,Bool>()
    var selectedCellIndex = 0
    var i = NSInteger()
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        
        
        
        
        if image_array.count > 0{
            
            if scrolling_index < (image_array.count - 1){
                
                scrolling_index = scrolling_index + 1
                
                collection1.scrollToItem(at: [0,scrolling_index], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                
            }
            else{
                scrolling_index = 0
                
                collection1.scrollToItem(at: [0,scrolling_index], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            }
            
            
        }
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        
        let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector((scrollToNextCell)), userInfo: nil, repeats: true);
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collection1 {
            return image_array.count
        }
        else if collectionView == collection2{
            
            return image_array2.count
        }
        else{
            
            return image_array3.count
           
        }
        
        
        
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView == collection3
        {
            return CGSize(width: collection3.frame.width/2.1, height: 300)
        }
        else if collectionView == collection2{
            return CGSize(width: 89,height: 45)
            
        }
        else{
            return collection1.frame.size
        }
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        if collectionView == collection2 {
            return UIEdgeInsets(top: 10, left:10, bottom: 10, right: 3)
        }
        else if collectionView == collection1 {
            
            return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        }
            
        else{
            return UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
        }
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if collectionView == collection2 {
            return 10
        }
        else if collectionView == collection1{
            return 0
        }
        else{
            return 10
            
        }
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        if collectionView == collection1 {
            return 0
        }
        
        
        return 5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        // cell arrangement design #######################################################
        if collectionView == collection3 {
            
            
            let layout2: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            
            layout2.sectionInset = UIEdgeInsets(top: 10, left:2, bottom: 10, right: 2)
            layout2.itemSize = CGSize(width: collection3.frame.width/2.1, height: 300)
            layout2.minimumInteritemSpacing = 5
            layout2.minimumLineSpacing = 30
          
        }
     
        if collectionView == collection1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll1", for: indexPath) as! foodmenu_CollectionViewCell
            
            
            cell.layer.cornerRadius = 12
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false
            
            cell.banner_img.roundCornersForAspectFit(radius: 12)
            
            cell.banner_img.image = UIImage()
            
            cell.banner_img.layer.cornerRadius=8
            
            let img_name = (((image_array.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String)
            
            
            if let cacheImage = imageCache1.object(forKey: img_name as NSString ){
                print("image cache")
                cell.banner_img.image = cacheImage
                let corneredImage = generateRoundCornerImage(image: cacheImage, radius: 12)
                
                //Set cornered Image
                cell.banner_img.image = corneredImage
                
                
                cell.banner_img.clipsToBounds = true
                //return
            }
            else{
                print("image not cache")
                //self.spinner.startAnimating()
                print("\(BANNER_IMAGE_URL)\(String(describing: img_name))")
                
                Alamofire.request("\(BANNER_IMAGE_URL)\(String(describing: img_name))").responseImage { response in
                    
                    
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.banner_img.image = image
                        
                        let corneredImage = self.generateRoundCornerImage(image: image, radius: 12)
                        
                        //Set cornered Image
                        cell.banner_img.image = corneredImage
                        
                        
                        cell.banner_img.clipsToBounds = true
                        self.imageCache1.setObject(image, forKey: img_name as NSString)
                        //      }
                    }
                    
                }
                
            }
            
            
            
            return cell
            
        }
          
        else if collectionView == collection2{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll2", for: indexPath) as! foodmenu_CollectionViewCell
            
            
            cell.isSelected = (cellStatus[indexPath.row] as? Bool) ?? false
            
            cell.layer.cornerRadius = 8
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.masksToBounds = true
            cell.col2view.layer.cornerRadius = 8
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false
            
            cell.col2view.layer.cornerRadius = 8
            
            cell.col2view.layer.shadowColor = UIColor.black.cgColor
            cell.col2view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.col2view.layer.shadowRadius = 3.0
            cell.col2view.layer.shadowOpacity = 0.2
            cell.col2view.layer.masksToBounds = true
            
            
            cell.col2lbl.text = (((image_array2.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String)
            
            
            
            
            
      //start
            cell.col2view.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.col2lbl.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            if(self.cellSelectionDict[indexPath.row] == true){
                cell.col2view.layer.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
                cell.col2lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.selectedCellIndex = indexPath.row ;
            }
            //end
            
            
            return cell
        }
        else {
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coll3", for: indexPath) as! foodmenu_CollectionViewCell

            let dateFormatter = DateFormatter()
            let amFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            amFormatter.dateFormat = "a"
            
            let dateInFormat = dateFormatter.string(from: NSDate() as Date)
            let dateInFor4 = dateFormatter.date(from: dateInFormat)!
            
            
            let check_date = dateInFor4.timeIntervalSince1970
            
            let amInFormat = amFormatter.string(from: NSDate() as Date)
            
            
            
            print( dateInFormat)
            print(amInFormat)
            
            
            cell.col3img.image = UIImage()
            
            if image_array3.count % 2 == 0{
                
                let height = ((((image_array3.count/2)) * 300)+70)
                height_colection3.constant = CGFloat(Float(height))
                
            }
            else{
                
                let height = (((((image_array3.count+1)/2)) * 300)+70)
                height_colection3.constant = CGFloat(Float(height))
            }
            if image_array3.count > 5 || image_array3.count == 5 {
                
                let height = (((((image_array3.count+1)/2)) * 300)+70)
                height_colection3.constant = CGFloat(Float(height))
            }
            if image_array3.count > 13 {
                
                let height = (((((image_array3.count+1)/2)) * 300)+100)
                height_colection3.constant = CGFloat(Float(height))
            }
            if image_array3.count > 20 {
                
                let height = (((((image_array3.count+1)/2)) * 300)+100)
                height_colection3.constant = CGFloat(Float(height))
            }
            
            
            cell.layer.cornerRadius = 8
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false
            
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            cell.contentView.clipsToBounds = false
            
            
            
            
            cell.layer.backgroundColor = UIColor.white.cgColor
            
            
            let data2 = userdata_services.instance.list_cakeitems_data["data"] as! NSArray
            
            print(data2)
            
            self.image_array3 = data2
            
            
            
            cell.col2name.text = (((self.image_array3.value(forKey: "title") as!
                NSArray).object(at: indexPath.row)) as! String)
            
            self.item["name"] = "\((((self.image_array3.value(forKey: "title") as! NSArray).object(at: indexPath.row)) as! String))"
            
            cell.add_to_cart_btn.tag = indexPath.row
            
            
            self.item["id"] = "\((((image_array3.value(forKey: "id") as! NSArray).object(at: indexPath.row))))"
            print(image_array3)
            cell.price_lbl
                .text = ((image_array3.value(forKey: "price") as! NSArray).object(at: indexPath.row)) as? String
            
            
            self.item["price"] = "\((image_array3.value(forKey: "price") as! NSArray).object(at: 0) as! String)"
            
            let img_name = (((self.image_array3.value(forKey: "image") as! NSArray).object(at: indexPath.row)) as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            self.item["image"] = img_name
            
            if let cacheImage = self.imageCache2.object(forKey: img_name as NSString ){
                print("image cache")
                cell.col3img.image = cacheImage
                
                cell.col3img.clipsToBounds = true
                //return
            }
            else{
                print("image not cache")
                //self.spinner.startAnimating()
                print("\(IMAGE_URL)\(String(describing: img_name))")
                
                Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                    
                    
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.col3img.image = image
                        self.isload = true
                        
                        cell.col3img.clipsToBounds = true
                        self.imageCache2.setObject(image, forKey: img_name as NSString)
                    }
                }
            }
            
            
            
            return cell
        }
        
    }
    
    
    func generateRoundCornerImage(image : UIImage , radius : CGFloat) -> UIImage {
        
        let imageLayer = CALayer()
        imageLayer.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        imageLayer.contents = image.cgImage
        imageLayer.masksToBounds = true
        imageLayer.cornerRadius = radius
        
        UIGraphicsBeginImageContext(image.size)
        imageLayer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collection1{
            let nam = self.image_array[indexPath.row] as! Dictionary<String,Any>
            let naa = nam["destination_type"] as! String
            let noo = nam["destination_id"] as! String
            
            if naa == "specific"{
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
                newViewController.cake_id = "\(noo)"
                self.present(newViewController, animated: false, completion: nil)
                
            }
            
            
        }
        
        if collectionView == collection2 {
            
            let selectedCell = collectionView.cellForItem(at: indexPath) as! foodmenu_CollectionViewCell
            image_array3 = NSArray()
            
            
            self.cellSelectionDict[self.selectedCellIndex] = false ;
            
            self.cellSelectionDict[indexPath.row]  = true
            spinner.startAnimating()
            authentication_services.instance.listcake_items(id:"\((((self.image_array2.value(forKey: "id") as! NSArray).object(at: indexPath.row)) ))"){ (success) in
                if success {
                    self.spinner.stopAnimating()

                    
                    if userdata_services.instance.list_cakeitems_data["error"] == nil{
                        
                        if userdata_services.instance.list_cakeitems_data["status"]  as! String == "ok"{
                            
                            let data2 = userdata_services.instance.list_cakeitems_data["data"] as! NSArray
                            
                            self.image_array3 = data2
                            
                            print("listcake_items = \(data2)")
                            
                            self.collection3.reloadData()
                            self.collection2.reloadData()
                            
                            
                        }
                    }
                }
            }
            
            self.collection2.reloadData()
            self.collection3.reloadData()
            
        }
        else 
        {
            if collectionView != collection1{
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
                newViewController.cake_id = "\((((image_array3.value(forKey: "id") as! NSArray).object(at: indexPath.row)) ))"
                self.present(newViewController, animated: false, completion: nil)
            }
            
        }
        
        
    }

    
    @IBOutlet weak var menu_btn: UIButton!
    
    
    @IBAction func fav_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
    }
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
    }
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
              userdata_services.instance.offer_check = "offer"
              // newViewController.contain = "offer"
              revealViewController()?.revealToggle(nil)
              self.present(newViewController, animated: false, completion: nil)
        
        
        
    }
    
    
    
    @IBAction func cart_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    @IBAction func menu_click(_ sender: Any) {
        
        
        UserDefaults.standard.set(2, forKey: "keepvalue")
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cellSelectionDict[0] = true ;
        userdata_services.instance.make_address = "no"
        userdata_services.instance.no_address = "no"
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        self.spinner.startAnimating()
        authentication_services.instance.banner_image_cake(){ (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.banner_cake_images["error"] == nil{
                    
                    if userdata_services.instance.banner_cake_images["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.banner_cake_images["data"] as! NSArray
                        
                        self.image_array = data1
                        print("banner_image_cake = \(data1)")
                        
                        self.collection1.reloadData()
                        self.startTimer()
                        
                        
                    }
                    else{
                        self.spinner
                            .stopAnimating()
                        
                        
                        if userdata_services.instance.list_cakeitems_data["message"]  as! String == "No Active Cakes Under this category"{
                            // self.noitem_lbl.isHidden = false
                            
                            
                        }
                            
                        else{
                            
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.list_cakeitems_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)}
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                // self.spinner2.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        self.spinner.startAnimating()
        
        authentication_services.instance.cake_categories(){ (success) in
            if success {
                self.spinner.stopAnimating()
                
                
                if userdata_services.instance.cake_categories_data["error"] == nil{
                    
                    if userdata_services.instance.cake_categories_data["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.cake_categories_data["data"] as! NSArray
                        
                      
                        
                      
                        print("cake_categories = \(data1)")
                        self.image_array2 = data1
                        
                        self.collection2.reloadData()
                        
                        
                        
                        for dict in data1 as! [[String:Any]]{
                            
                            let id = dict["id"]
                            print("id = \(id)")
                            self.id_array.append(id as? NSInteger ?? 0 )

                        }
                        print("id_array = \(self.id_array)")
                        
                        let myInt3 = (self.destination_id as NSString).integerValue
                        print("destiString = \(myInt3)")
                        
                        if self.id_array.contains(myInt3) {
                          //do something
                            print("true")
                            let selectedIndex = self.id_array.index(of:myInt3)
                            self.cellSelectionDict[0] = false ;
                            self.cellSelectionDict[selectedIndex!] = true
                        
                            // loading cake categories
                            authentication_services.instance.listcake_items(id:"\(myInt3)"){ (success) in
                            if success {

                            //   self.spinner.stopAnimating()

                            if userdata_services.instance.list_cakeitems_data["error"] == nil{

                            if userdata_services.instance.list_cakeitems_data["status"]  as! String == "ok"{
                               
                               let data2 = userdata_services.instance.list_cakeitems_data["data"] as! NSArray
                               print("data2 = \(data2)")
                               
                               self.image_array3 = data2
                               
                               
                               
                               self.collection3.reloadData()
                               
                               
                               
                            }
                            else{
                               
                               if userdata_services.instance.list_cakeitems_data["message"]  as! String == "No Active Cakes Under this category"{
                                   
                                   
                                   
                               }
                                   
                               else{
                                   
                                   let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.list_cakeitems_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                   alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                   self.present(alert2, animated: true, completion: nil)}
                            }
                            }

                            else{
                            userdata_services.instance.logout()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")



                            self.present(newViewController, animated: false, completion: nil)


                            }



                            }
                            else{
                            //          self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)

                            }

                            }

                        }else{
                            print("false")
                        
                if self.image_array2.count>0{
                    


                // loading cake categories
                authentication_services.instance.listcake_items(id:"\((((self.image_array2.value(forKey: "id") as! NSArray).object(at: 0)) ))"){ (success) in
                if success {

                //   self.spinner.stopAnimating()

                if userdata_services.instance.list_cakeitems_data["error"] == nil{

                if userdata_services.instance.list_cakeitems_data["status"]  as! String == "ok"{

                let data2 = userdata_services.instance.list_cakeitems_data["data"] as! NSArray
                print("data2 = \(data2)")

                self.image_array3 = data2
                self.collection3.reloadData()

                }
                else{

                if userdata_services.instance.list_cakeitems_data["message"]  as! String == "No Active Cakes Under this category"{
                }

                else{

                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.list_cakeitems_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)}
                }
                }

                else{
                userdata_services.instance.logout()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                self.present(newViewController, animated: false, completion: nil)

                }
                }
                else{
                //          self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)

                }

                }


                }
                 
                        }
                        
             
                        
           // end
                        
                    }
                    else{
                        //        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.cake_categories_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                    
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        UserDefaults.standard.set(2, forKey: "keepvalue")
        foodmenu_icon.setBackgroundImage(#imageLiteral(resourceName: "cheese-burger-3"), for: [])
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
             self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func add_to_cart_clicked(_ sender: UIButton) {
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
        newViewController.cake_id = "\((((image_array3.value(forKey: "id") as! NSArray).object(at: sender.tag)) ))"
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

