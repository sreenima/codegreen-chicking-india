//
//  customize_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 15/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class customize_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var table2: UITableView!
    
    @IBOutlet weak var cart_pop: UIButton!
    @IBOutlet weak var BG_view: UIView!
    @IBOutlet weak var table1: UITableView!
    @IBOutlet weak var contibtn: UIButton!
    let cel = customize_TableViewCell()
    
    @IBAction func back_clicked(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    
    
    @IBAction func cart_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == table1{
        return 3
    }
        else{
            
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table1
        {
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! customize_TableViewCell
            if indexPath.row == 0
            {
                let image = UIImage(named: "Group 449") as UIImage?
            
               cell.tb1selector.setBackgroundImage(image, for:[])
                
            }
            
            
             return cell
        }
        else
        {
             let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! customize_TableViewCell
            if indexPath.row == 0
            {
                let image = UIImage(named: "Group 449") as UIImage?
                cell.tb2selector.setBackgroundImage(image, for:[])
               
                
            }
            
             return cell
    }
}
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contibtn.layer.cornerRadius = 8
        BG_view.layer.cornerRadius = 16.0
        BG_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        BG_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        BG_view.layer.shadowRadius = 3.0
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.masksToBounds = false
       
    }

    
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    
    @IBAction func account_click(_ sender: Any) {
        
   /*     let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        self.present(newViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
         newViewController.check_name = "favorite"
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
