//
//  customize_TableViewCell.swift
//  chicking
//
//  Created by Ios CGT on 15/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class customize_TableViewCell: UITableViewCell {

    
    @IBOutlet weak var tb1selector: UIButton!
    @IBOutlet weak var tb2selector: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      
        // Initialization code
    }
    
    @IBAction func buttonclick1(_ sender: Any) {
        let image = UIImage(named: "Group 449") as UIImage?
        let image1 = UIImage(named: "Ellipse 8") as UIImage?
        if tb1selector.currentBackgroundImage == image {
            tb1selector.setBackgroundImage(image1, for:[])
        }
        else{
            
             tb1selector.setBackgroundImage(image, for:[])
        }
    }
    @IBAction func buttonclick2(_ sender: Any) {
        
        let image = UIImage(named: "Group 449") as UIImage?
        let image1 = UIImage(named: "Ellipse 8") as UIImage?
        if tb2selector.currentBackgroundImage == image {
            tb2selector.setBackgroundImage(image1, for:[])
        }
        else{
            
            tb2selector.setBackgroundImage(image, for:[])
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
