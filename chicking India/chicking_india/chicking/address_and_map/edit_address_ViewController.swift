//
//  edit_address_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 17/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlacePicker

class edit_address_ViewController: UIViewController, UITextViewDelegate , UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var no_FA = String()
    @IBOutlet weak var new_address_height: NSLayoutConstraint!
    @IBOutlet weak var new_ibutton: UIButton!
    @IBOutlet weak var new_address_txtfld: UITextField!
    @IBOutlet weak var new_address_vw: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var i_btn_building: UIButton!
    @IBOutlet weak var i_btn1: UIButton!
    @IBOutlet weak var i_btn2: UIButton!
    @IBOutlet weak var address_txt: UILabel!
    @IBOutlet weak var house_no_txtfld: UITextField!
    @IBOutlet weak var landmark_txtfld: UITextField!
    @IBOutlet weak var building_name_txt_fld: UITextField!
    @IBOutlet weak var home_tick: UIButton!
    @IBOutlet weak var work_tick: UIButton!
    @IBOutlet weak var any_other_details_txtfld: UITextView!
    @IBOutlet weak var building_name_view: UIView!
    @IBOutlet weak var house_no_vw: UIView!
    @IBOutlet weak var landmark_vw: UIView!
    @IBOutlet weak var add_btn: UIButton!
    @IBOutlet weak var other_tick: UIButton!
    @IBOutlet weak var height_contrain: NSLayoutConstraint!
    @IBOutlet weak var refresh_map_btn: UIButton!
    var no_add = NSString()
    var current_address = String()
    var test = NSString()
    var ttest = NSString()
    let texxt = "If any other details..."
    var typee = String()
    var check = String()
    var checker = String()
    var id_check = String()
    var now_check = String()
    var the_way = String()
    var locationManager = CLLocationManager()
    var place_id = String()
    var addr_coordinates = CLLocationCoordinate2D()
    
    var longi = String()
    var lati = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        typee = "other"
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        
        longi = "\(addr_coordinates.longitude)"
        lati = "\(addr_coordinates.latitude)"
        
        //Zoom to user location
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation, 200, 200)
            mapView.setRegion(viewRegion, animated: false)
        }
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
        
        i_btn1.isHidden = true
        i_btn2.isHidden = true
        i_btn_building.isHidden = true
        new_ibutton.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        address_txt.text = test as String
        new_address_txtfld.text = test as String
        view.addGestureRecognizer(tap)
        
        add_btn.layer.cornerRadius = 8
        
        
        building_name_view.layer.cornerRadius = 4
        building_name_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        building_name_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        building_name_view.layer.shadowRadius = 3.0
        building_name_view.layer.shadowOpacity = 0.2
        building_name_view.layer.masksToBounds = false
        building_name_txt_fld.delegate = self
        
        
        new_address_vw.layer.cornerRadius = 4
        new_address_vw.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        new_address_vw.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        new_address_vw.layer.shadowRadius = 3.0
        new_address_vw.layer.shadowOpacity = 0.2
        new_address_vw.layer.masksToBounds = false
        new_address_txtfld.delegate = self
        
        house_no_vw.layer.cornerRadius = 4
        house_no_vw.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        house_no_vw.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        house_no_vw.layer.shadowRadius = 3.0
        house_no_vw.layer.shadowOpacity = 0.2
        house_no_vw.layer.masksToBounds = false
        house_no_txtfld.delegate = self
        
        landmark_vw.layer.cornerRadius = 4
        landmark_vw.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        landmark_vw.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        landmark_vw.layer.shadowRadius = 3.0
        landmark_vw.layer.shadowOpacity = 0.2
        landmark_vw.layer.masksToBounds = false
        landmark_txtfld.delegate = self
        
        any_other_details_txtfld.layer.cornerRadius = 4
        any_other_details_txtfld.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        any_other_details_txtfld.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        any_other_details_txtfld.layer.shadowRadius = 3.0
        any_other_details_txtfld.layer.shadowOpacity = 0.2
        any_other_details_txtfld.layer.masksToBounds = false
        any_other_details_txtfld.delegate = self
        
        any_other_details_txtfld.text = texxt
      
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        region.center = self.mapView.userLocation.coordinate
        
        let userLlocation = (locationManager.location?.coordinate)!
        let viewRegion = MKCoordinateRegionMakeWithDistance(userLlocation, 200, 200)
        mapView.setRegion(viewRegion, animated: true)
        
        //self.mapView.setRegion(region, animated: true)
    }
   
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func homee_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    
    
    @IBAction func account_click(_ sender: Any) {
        
    /*    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

 
        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func map_refresh_click(_ sender: Any) {
//        
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//        
//        placePicker.delegate = self as GMSPlacePickerViewControllerDelegate
//        present(placePicker, animated: true, completion: nil)
        dismiss(animated: false, completion: nil)
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        
        print(location)
    }
    
    @IBAction func back_clicked(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if any_other_details_txtfld.text == "If any other details..."{
            any_other_details_txtfld.text = ""
        }
        height_contrain.constant = 200
        updateViewConstraints()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if any_other_details_txtfld.text == ""{
            any_other_details_txtfld.text = "If any other details..."
        }
        height_contrain.constant = 43
        updateViewConstraints()
    }
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if house_no_txtfld.text != "" {
            i_btn1.isHidden = true
        }
        if landmark_txtfld.text != "" {
            i_btn2.isHidden = true
        }
        if new_address_txtfld.text != "" {
            new_ibutton.isHidden = true
        }
        if building_name_txt_fld.text != ""{
            
            i_btn_building.isHidden = true
        }
        
        
    }
    
    @IBAction func other_click(_ sender: Any) {
        let image = UIImage(named: "Group 449") as UIImage?
        let image1 = UIImage(named: "Ellipse 10") as UIImage?
        typee = "other"
        if other_tick.currentBackgroundImage == image {
            //other_tick.setBackgroundImage(image1, for:[])
            
        }
        else{
            
            other_tick.setBackgroundImage(image, for:[])
            work_tick.setBackgroundImage(image1, for:[])
            home_tick.setBackgroundImage(image1, for:[])
            
        }
    }
    
    @IBAction func work_click(_ sender: Any) {
        let image = UIImage(named: "Group 449") as UIImage?
        let image1 = UIImage(named: "Ellipse 10") as UIImage?
        typee = "work"
        if  work_tick.currentBackgroundImage == image {
            // work_tick.setBackgroundImage(image1, for:[])
            
            
        }
        else{
            
            work_tick.setBackgroundImage(image, for:[])
            other_tick.setBackgroundImage(image1, for:[])
            home_tick.setBackgroundImage(image1, for:[])
        }
    }
    
    @IBAction func home_click(_ sender: Any) {
        let image = UIImage(named: "Group 449") as UIImage?
        let image1 = UIImage(named: "Ellipse 10") as UIImage?
        typee = "home"
        if home_tick.currentBackgroundImage == image {
         
            
        }
        else{
            
            home_tick.setBackgroundImage(image, for:[])
            work_tick.setBackgroundImage(image1, for:[])
            other_tick.setBackgroundImage(image1, for:[])
        }
    }
    
    @IBAction func confirm_add_click(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            print("Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order") as! order_customizeViewController
        let storyBoard2: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController2 = storyBoard2.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        let storyBoard3: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController3 = storyBoard3.instantiateViewController(withIdentifier: "svd_adr") as! saved_address_ViewController
        
        if self.check == "yes"{
            
            if no_add == "yes" {
                
                if house_no_txtfld.text == "" || landmark_txtfld.text == "" || new_address_txtfld.text == "" || building_name_txt_fld.text == "" {
                    
                    if house_no_txtfld.text == ""{
                        i_btn1.isHidden = false
                    }
                    if landmark_txtfld.text == ""{
                        i_btn2.isHidden = false
                    }
                    if building_name_txt_fld.text == ""{
                        i_btn_building.isHidden = false
                    }
                    if new_address_txtfld.text == ""{
                        new_ibutton.isHidden = false
                    }
                    
                    
                    
                    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    animation.duration = 0.6
                    animation.values = [-10.0, 10.0, -10.0, 10.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
                    new_ibutton.layer.add(animation, forKey: "shake")
                    i_btn2.layer.add(animation, forKey: "shake")
                    i_btn1.layer.add(animation, forKey: "shake")
                    i_btn_building.layer.add(animation, forKey: "shake")
                }
                    
                else{
                    
                    if any_other_details_txtfld.text == "If any other details..."{
                        any_other_details_txtfld.text = ""
                    }
                    current_address = new_address_txtfld.text as! String
                    authentication_services.instance.add_address(address: "\(new_address_txtfld.text!),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)", building: "\(building_name_txt_fld.text!)", longi: longi, lati: lati){ (success) in
                        if success {
                            //  self.spinner.stopAnimating()
                            if userdata_services.instance.add_address_data["error"] == nil{
                                
                                if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                    
                                    newViewController.address_keep = "\(self.new_address_txtfld.text)\(self.house_no_txtfld.text!),\(self.landmark_txtfld.text!)" as NSString
                                    if userdata_services.instance.the_way == "no_ad"{
                                        self.present(newViewController, animated: false, completion: nil)
                                    }
                                    
                                    if self.checker == "yes"{
                                        newViewController3.no_back = "yes"
                                        self.present(newViewController3, animated: false, completion: nil)
                                    }
                                    else{
                                        self.present(newViewController2, animated: false, completion: nil)
                                    }
                                    
                                    
                                    
                                }
                                else{
                                    //  self.spinner.stopAnimating()
                                    
                                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                }
                            }
                                
                                
                            else{
                                userdata_services.instance.logout()
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                                
                                //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                                
                                self.present(newViewController, animated: false, completion: nil)
                                
                                
                            }
                            
                        }
                        else{
                            // self.spinner.stopAnimating()
                            
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                    }
                    
                    
                    
                }
            }
            else if no_add == "no"{
                if house_no_txtfld.text == "" || landmark_txtfld.text == "" || new_address_txtfld.text == "" || building_name_txt_fld.text == ""{
                    
                    if house_no_txtfld.text == ""{
                        i_btn1.isHidden = false
                    }
                    if landmark_txtfld.text == ""{
                        i_btn2.isHidden = false
                    }
                    if building_name_txt_fld.text == ""{
                        i_btn_building.isHidden = false
                    }
                    if new_address_txtfld.text == ""{
                        new_ibutton.isHidden = false
                    }
                    
                    
                    
                    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    animation.duration = 0.6
                    animation.values = [-10.0, 10.0, -10.0, 10.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
                    
                    i_btn2.layer.add(animation, forKey: "shake")
                    i_btn1.layer.add(animation, forKey: "shake")
                    i_btn_building.layer.add(animation, forKey: "shake")
                    new_ibutton.layer.add(animation, forKey: "shake")
                    
                }
                else
                {
                    
                    
                    
                    if any_other_details_txtfld.text == "If any other details..."{
                        any_other_details_txtfld.text = ""
                    }
                    
                    
                    authentication_services.instance.add_address(address: "\(test),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)",building: "\(building_name_txt_fld.text!)", longi: longi, lati: lati){ (success) in
                        if success {
                            //  self.spinner.stopAnimating()
                            if userdata_services.instance.add_address_data["error"] == nil{
                                
                                if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                    
                                    newViewController.address_keep = self.address_txt.text! as NSString
                                    
                                    
                                    if self.checker == "yes"{
                                        newViewController3.no_back = "yes"
                                        self.present(newViewController3, animated: false, completion: nil)
                                    }
                                    else{
                                        self.present(newViewController2, animated: false, completion: nil)
                                    }
                                    
                                }
                                else{
                                    //  self.spinner.stopAnimating()
                                    
                                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                }
                            }
                                
                                
                            else{
                                userdata_services.instance.logout()
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                                
                                
                                
                                self.present(newViewController, animated: false, completion: nil)
                                
                                
                            }
                            
                        }
                        else{
                            // self.spinner.stopAnimating()
                            
                            print("\(userdata_services.instance.edit_address_data["message"]  as! String)")
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                }
                
            }
                
            else{
                
                
                if any_other_details_txtfld.text == "If any other details..."{
                    any_other_details_txtfld.text = ""
                }
                
                
                
                authentication_services.instance.edit_address(id: "\(self.id_check)",address: "\(new_address_txtfld.text!),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)"){ (success) in
                    if success {
                        //  self.spinner.stopAnimating()
                        if userdata_services.instance.add_address_data["error"] == nil{
                            
                            if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                
                                newViewController.address_keep = self.address_txt.text! as NSString
                                
                                
                                if self.checker == "yes"{
                                    newViewController3.no_back = "yes"
                                    self.present(newViewController3, animated: false, completion: nil)
                                }
                                else{
                                    self.present(newViewController2, animated: false, completion: nil)
                                    
                                }
                                
                            }
                            else{
                                //  self.spinner.stopAnimating()
                                
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                            }
                        }
                            
                            
                        else{
                            userdata_services.instance.logout()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                            
                            
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
                
                
                
            }
            
        }
        else{
            
            if no_add == "yes" {
                
                
                
                if house_no_txtfld.text == "" || landmark_txtfld.text == "" || new_address_txtfld.text == "" || building_name_txt_fld.text == "" {
                    
                    
                    if house_no_txtfld.text == ""{
                        i_btn1.isHidden = false
                    }
                    if landmark_txtfld.text == ""{
                        i_btn2.isHidden = false
                    }
                    if building_name_txt_fld.text == ""{
                        i_btn_building.isHidden = false
                    }
                    if new_address_txtfld.text == ""{
                        new_ibutton.isHidden = false
                    }
                 
                    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    animation.duration = 0.6
                    animation.values = [-10.0, 10.0, -10.0, 10.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
                    i_btn_building.layer.add(animation, forKey: "shake")
                    new_ibutton.layer.add(animation, forKey: "shake")
                    i_btn2.layer.add(animation, forKey: "shake")
                    i_btn1.layer.add(animation, forKey: "shake")
                }
                    
                else{
                    
                    if any_other_details_txtfld.text == "If any other details..."{
                        any_other_details_txtfld.text = ""
                    }
                    current_address = new_address_txtfld.text as! String
                    authentication_services.instance.add_address(address: "\(new_address_txtfld.text!),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)",building: "\(building_name_txt_fld.text!)", longi: longi, lati: lati){ (success) in
                        if success {
                            //  self.spinner.stopAnimating()
                            if userdata_services.instance.add_address_data["error"] == nil{
                                
                                if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                    
                                    newViewController.address_keep = "\(self.new_address_txtfld.text)\(self.house_no_txtfld.text!),\(self.landmark_txtfld.text!)" as NSString
                                    
                                    
                                    if self.checker == "yes"{
                                        if userdata_services.instance.no_address == "yes"{
                                            self.present(newViewController, animated: false, completion: nil)
                                            
                                        }else{
                                            newViewController3.no_back = "yes"
                                            self.present(newViewController3, animated: false, completion: nil)
                                        }
                                    }
                                    else{
                                        self.present(newViewController, animated: false, completion: nil)
                                    }
                                    
                                    
                                }
                                else{
                                    //  self.spinner.stopAnimating()
                                    
                                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                }
                            }
                                
                                
                            else{
                                userdata_services.instance.logout()
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
                                
                                
                                
                                self.present(newViewController, animated: false, completion: nil)
                                
                                
                            }
                            
                        }
                        else{
                            // self.spinner.stopAnimating()
                            
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                    }
                    
                    
                    
                }
            }
            else if no_add == "no"{
                if house_no_txtfld.text == "" || landmark_txtfld.text == "" || building_name_txt_fld.text == "" || new_address_txtfld.text == "" {
                    
                    if house_no_txtfld.text == ""{
                        i_btn1.isHidden = false
                    }
                    if landmark_txtfld.text == ""{
                        i_btn2.isHidden = false
                    }
                    if building_name_txt_fld.text == ""{
                        i_btn_building.isHidden = false
                    }
                    if new_address_txtfld.text == ""{
                        new_ibutton.isHidden = false
                    }
                    
                    
                    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    animation.duration = 0.6
                    animation.values = [-10.0, 10.0, -10.0, 10.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
                    
                    i_btn_building.layer.add(animation, forKey: "shake")
                    new_ibutton.layer.add(animation, forKey: "shake")
                    i_btn2.layer.add(animation, forKey: "shake")
                    i_btn1.layer.add(animation, forKey: "shake")
                }
                else
                {
                 
                    if any_other_details_txtfld.text == "If any other details..."{
                        any_other_details_txtfld.text = ""
                    }
                    
                    authentication_services.instance.add_address(address: "\(test),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)",building: "\(building_name_txt_fld.text!)", longi: longi, lati: lati){ (success) in
                        if success {
                            //  self.spinner.stopAnimating()
                            if userdata_services.instance.add_address_data["error"] == nil{
                                
                                if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                    
                                    newViewController.address_keep = self.address_txt.text! as NSString
                                    
                                    if self.checker == "yes"{
                                        if userdata_services.instance.no_address == "yes"{
                                            self.present(newViewController, animated: false, completion: nil)
                                        }else{
                                            newViewController3.no_back = "yes"
                                            self.present(newViewController3, animated: false, completion: nil)
                                        }
                                    }
                                    else{
                                        
                                        self.present(newViewController, animated: false, completion: nil)
                                    }
                                    
                                    
                                }
                                else{
                                    //  self.spinner.stopAnimating()
                                    
                                    let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                }
                            }
                                
                                
                            else{
                                userdata_services.instance.logout()
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                                
                                
                                self.present(newViewController, animated: false, completion: nil)
                                
                                
                            }
                            
                        }
                        else{
                            // self.spinner.stopAnimating()
                            
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                        
                    }
                    
                }
                
            }
                
            else{
                
                if any_other_details_txtfld.text == "If any other details..."{
                    any_other_details_txtfld.text = ""
                }
                
                authentication_services.instance.add_address(address: "\(test),", flat_name: "\(house_no_txtfld.text!),", landmark: "\(landmark_txtfld.text!)", other_details:"\(any_other_details_txtfld.text!)", tick_type: "\(typee)", area_id: "\(userdata_services.instance.area_id)",building: "\(building_name_txt_fld.text!)", longi: longi, lati: lati){ (success) in
                    if success {
                        //  self.spinner.stopAnimating()
                        if userdata_services.instance.add_address_data["error"] == nil{
                            
                            if userdata_services.instance.add_address_data["status"]  as! String == "ok"{
                                
                                newViewController.address_keep = self.address_txt.text! as NSString
                                
                                
                                if self.checker == "yes"{
                                    newViewController3.no_back = "yes"
                                    self.present(newViewController3, animated: false, completion: nil)
                                }
                                else{
                                    self.present(newViewController, animated: false, completion: nil)
                                    
                                }
                                
                            }
                            else{
                                //  self.spinner.stopAnimating()
                                
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.add_address_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                            }
                        }
                            
                            
                        else{
                            userdata_services.instance.logout()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                            
                            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension edit_address_ViewController : GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
       
        viewController.dismiss(animated: true, completion: nil)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        
        
        if place.formattedAddress == nil {
            
            
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(place.coordinate) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                } else {
                    if let places = response?.results() {
                        if let place = places.first {
                            
                            
                            if let lines = place.lines {
                                print("GEOCODE: Formatted Address: \(lines)")
                                var plc = "\(lines[0])"
                                
                                self.no_FA = "yes"
                                
                                viewController.dismiss(animated: true, completion: nil)
                                self.ttest = self.test
                                self.address_txt.text = plc as String
                                self.new_address_txtfld.text = plc as String
                                
                            }
                            
                        } else {
                            
                            
                            print("GEOCODE: nil first in places")
                            
                        }
                    } else {
                        
                        
                        print("GEOCODE: nil in places")
                        
                        
                    }
                }
            }
            
        }else{
            
            
            no_FA = "no"
            test = place.formattedAddress! as NSString
            viewController.dismiss(animated: true, completion: nil)
            ttest = test
            var art = ("\(place.name), \(place.formattedAddress!)") as NSString
            address_txt.text = art as String
            new_address_txtfld.text = art as String
        }
        
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        //dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
}

