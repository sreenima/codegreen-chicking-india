//
//  terms_and_condition_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/12/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage



extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


class terms_and_condition_ViewController: UIViewController {
    
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var txt_vw: UITextView!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        var header = [
            
            "Content-Type": "application/json; charset=utf-8"
        ]
        spinner.startAnimating()
        Alamofire.request("\("\(BASE_URL)showTerms")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
            
            if response.result.error == nil {
                self.spinner.stopAnimating()
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                let doom = json as! [String : Any]
                if doom["error"] == nil{
                    
                    if doom["status"] as! String == "ok"{
                        print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                        print(doom)
                        let data1 = doom["data"] as! Dictionary<String,Any>
                        let text = data1["attribute_value"] as! String
                        print(text)
                        let txt = text.htmlToString
                        print(txt)
                        
                        self.txt_vw.text = txt
                    }
                    else{
                        
                        var errorMsg = ""
                        errorMsg = doom["message"] as! String
                        if errorMsg != ""{
                            
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                                                   
                        }
                        else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                }
                else{
                    self.spinner.stopAnimating()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                   
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                // Do any additional setup after loading the view.
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
                
                
            }
        }
        
        
    }
    
    
    @IBAction func back_clicked(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

