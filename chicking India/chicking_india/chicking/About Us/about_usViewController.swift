//
//  about_usViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/12/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import SWRevealViewController

class about_usViewController: UIViewController {
    
    @IBOutlet weak var about_us_txt_fld: UITextView!
    
    @IBOutlet weak var menu_btn: UIButton!
    @IBOutlet weak var read_more_lbl: UILabel!
    
    @IBOutlet weak var read_more_down_arrow: UIButton!
    
    @IBOutlet weak var tect_vw_height: NSLayoutConstraint!
    
    @IBOutlet weak var read_more_btn: UIButton!
    var a = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
             self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
        self.tect_vw_height.constant = 228
        
        read_more_down_arrow.setBackgroundImage(#imageLiteral(resourceName: "down-arrow (3)"), for: .normal)
        
        
        
        
        
    }
    
    @IBAction func read_more_click(_ sender: Any) {
        if a % 2 == 0{
            self.tect_vw_height.constant = 550
            read_more_lbl.text = "Read Less"
            read_more_down_arrow.setBackgroundImage(#imageLiteral(resourceName: "down-arrow (3) (3)"), for: .normal)
            a = 1
        }
        else {
            self.tect_vw_height.constant = 228
            read_more_lbl.text = "Read More"
            read_more_down_arrow.setBackgroundImage(#imageLiteral(resourceName: "down-arrow (3)"), for: .normal)
            a = 2
        }
        
    }
    
    @IBAction func privecy_click(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "privacy")
        
       
        
        self.present(newViewController, animated: false, completion: nil)
    }
    @IBAction func terms_click(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "terms")
        
        
        self.present(newViewController, animated: false, completion: nil)
    }
    @IBAction func cancel_click(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "cancel")
        
        
        
        self.present(newViewController, animated: false, completion: nil)
    }
    @IBAction func refund_click(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "refund")
        
        
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func skip_and_click(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ship")
        
        
        
        self.present(newViewController, animated: false, completion: nil)
    }
    @IBAction func back_click(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
