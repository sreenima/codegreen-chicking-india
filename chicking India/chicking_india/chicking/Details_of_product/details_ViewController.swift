//
//  details_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 15/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage




class details_ViewController: UIViewController {
    @IBOutlet weak var BG_view: UIView!
    var cake_id = String()
    @IBOutlet var name: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet weak var availablility_lbl: UILabel!
    
    @IBOutlet var count_lbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var isload = false
    var condition_time = TimeInterval()
    var check_date = TimeInterval()
    var condition_time2 = TimeInterval()
    var item = ["name":"","price":"","image":"","id":"","quantity":"1"]
    var aa = String()
    
    var doomed = Dictionary<String,Any>()
    
    @IBOutlet var cart_btn: UIButton!
    @IBOutlet weak var cart_pop: UIButton!
    @IBOutlet var price: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet weak var decriment_btn: UIButton!
    
    @IBOutlet weak var incriment_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        incriment_btn.isUserInteractionEnabled = false
        
        decriment_btn.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        cart_btn.isHidden = true
        BG_view.layer.cornerRadius = 16.0
        BG_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        BG_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        BG_view.layer.shadowRadius = 3.0
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.masksToBounds = false
       
        // Do any additional setup after loading the view.
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        let amInFormat = amFormatter.string(from: NSDate() as Date)
        
        
        
        print( dateInFormat)
        print(amInFormat)
        
        
        
        self.spinner.startAnimating()
        
        
        print(cake_id)
        
        authentication_services.instance.cake_by_id(id: cake_id){ (success) in
            if success {
                UIApplication.shared.endIgnoringInteractionEvents()
                self.spinner.stopAnimating()
                if userdata_services.instance.cake_by_id["error"] == nil{
                    
                    if userdata_services.instance.cake_by_id["status"]  as! String == "ok"{
                        // self.cart_btn.isHidden = false
                        let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
                        
                        let dat2 = dat
                        
                        self.doomed = dat2
                        self.isload = true
                       
                        let cake_attributes = dat2
                        
                        self.price.text = "\(cake_attributes["price"]!)"
                        
                        self.item["price"] = "\(cake_attributes["price"]!)"
                        
                        let txt = "\(dat["description"]!)"
                        
                        
                        var res = txt.htmlToString
                        
                        self.description_lbl.text = res.trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        print(txt)
                        print(self.description_lbl.text ?? "NO description")
                        print("have some space")
                        
                        self.name.text = "\(dat["title"]!)"
                        self.item["name"] = "\(dat["title"]!)"
                        self.item["id"] = "\(dat["id"]!)"
                        
                        let img_name = ("\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                        
                        self.item["image"] = ("\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                        
                        
                        print("\(IMAGE_URL)cakes/\(img_name)")
                        
                        Alamofire.request(("\(IMAGE_URL)cakes/\(img_name)")).responseImage { response in
                            //debugPrint(response)
                            
                            //print(response.request)
                            //print(response.response)
                            //debugPrint(response.result)
                            
                            if let image = response.result.value {
                                print("image downloaded: \(image)")
                                self.img.image = image
                                
                                //      }
                            }
                            
                        }
                        
                        
                        
                        self.incriment_btn.isUserInteractionEnabled = true
                        
                        self.decriment_btn.isUserInteractionEnabled = true
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.cake_by_id["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                            switch action.style{
                            case .default:
                                self.dismiss(animated: false, completion: nil)
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        
                        
                        
                        
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                UIApplication.shared.endIgnoringInteractionEvents()
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        
        self.spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            self.spinner.stopAnimating()
            if success {
                
                
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                        
                        
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        let t1 = dat["storeOpen"] as! String
                        let t2 = dat["storeClose"] as! String
                        
                        
                        
                        
                        let da = DateFormatter()
                        
                        da.timeZone = TimeZone.current
                        da.locale = Locale(identifier: "en-US")
                        
                        da.dateFormat = "hh:mm a"
                        
                        
                        if let dateInFormatt = da.date(from: t1){
                            let pure_date1 = da.string(from:dateInFormatt as Date)
                            
                            let dateInFor = da.date(from: pure_date1)!
                            
                            self.condition_time = dateInFor.timeIntervalSince1970
                            //////////////////////////////////////////////////////////////
                            let da2 = DateFormatter()
                            
                            da2.timeZone = TimeZone.current
                            da2.locale = Locale(identifier: "en-US")
                            
                            da2.dateFormat = "hh:mm a"
                            
                            let dateInFormattt = da2.date(from: t2)!
                            let pure_date2 = da2.string(from:dateInFormattt as Date)
                            
                            let dateInForr = da2.date(from: pure_date2)!
                            
                            self.condition_time2 = dateInForr.timeIntervalSince1970
                            
                            let texx = "Next Available At \(t1 )"
                            self.availablility_lbl.text = texx
                            
                            
                            
                            print(dateInFormat)
                            let a:Int? = Int(dateInFormat)
                            if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                                
                                self.cart_btn.isHidden = false
                                self.availablility_lbl.isHidden = true
                                
                            }else{
                                self.cart_btn.isHidden = true
                                self.availablility_lbl.isHidden = false
                            }
                            
                            
                        }else{
                            
                            
                            self.viewDidLoad()
                            
                        }
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    @IBAction func moveto_cart(_ sender: UIButton) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        
        self.present(newViewController, animated: false, completion: nil)
        
        
        
    }
    
    @IBAction func cart_click(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        
        
        spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            self.spinner.stopAnimating()
            if success {
                
                
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                       
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        
                        var t1 = dat["storeOpen"] as! String
                        var t2 = dat["storeClose"] as! String
                        
                        
                        
                        
                        let da = DateFormatter()
                        da.timeZone = TimeZone.current
                        da.locale = Locale(identifier: "en-US")
                        da.dateFormat = "hh:mm a"
                        
                        let dateInFormatt = da.date(from: t1)!
                        var pure_date1 = da.string(from:dateInFormatt as Date)
                        
                        let dateInFor = da.date(from: pure_date1)!
                        
                        self.condition_time = dateInFor.timeIntervalSince1970
                        //////////////////////////////////////////////////////////////
                        let da2 = DateFormatter()
                        da2.timeZone = TimeZone.current
                        da2.locale = Locale(identifier: "en-US")
                        da2.dateFormat = "hh:mm a"
                        
                        let dateInFormattt = da2.date(from: t2)!
                        var pure_date2 = da2.string(from:dateInFormattt as Date)
                        
                        let dateInForr = da2.date(from: pure_date2)!
                        
                        self.condition_time2 = dateInForr.timeIntervalSince1970
                        
                        let texx = "Next Available At \(t1 )"
                        self.availablility_lbl.text = texx
                        
                        print(dateInFormat)
                        if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                            
                            self.cart_btn.isHidden = false
                            self.availablility_lbl.isHidden = true
                            
                            
                            print(self.doomed)
                            let pp: NSArray = self.doomed["item_list"] as! NSArray
                            print(pp)
                            let rr = pp[0] as! Dictionary<String,Any>
                            print(rr)
                            let oo = rr["item_attributes"] as! NSArray
                            print(oo)
                            if oo.count != 0{
                                
                                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "customize") as! customization_VC
                                
                                
                                userdata_services.instance.quantity = "\(self.count_lbl.text!)"
                                newViewController.quantity = "\(self.count_lbl.text!)"
                                
                                self.present(newViewController, animated: false, completion: nil)
                             
                                self.item["quantity"] = "\(self.count_lbl.text!)"
                           
                            }
                            else{
                                
                                //  self.no_attributes()
                                self.no_addon()
                                
                            }
                            
                        }else{
                            
                            
                            
                            self.cart_btn.isHidden = true
                            self.availablility_lbl.isHidden = false
                            
                        }
                        
                        
                        
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
    }
    
    
    @IBAction func decriment_count(_ sender: UIButton) {
        
        if Int(count_lbl.text!)! > 1 {
            
            
            count_lbl.text = "\(Int(count_lbl.text!)! - 1)"
            //  let aa = price.text
            
            //   price.text = "\(Double(aa!)! / 2)"
            price.text = "\(Double(price.text!)! - Double(aa)!)"
            
        }
    }
    
    
    
    @IBAction func increment_count(_ sender: UIButton) {
        
        if count_lbl.text == "1"{
            aa = price.text!
        }
        count_lbl.text = "\(Int(count_lbl.text!)! + 1)"
        
        print(aa)
        print(count_lbl.text)
        price.text = "\(Double(aa)! * Double(count_lbl.text!)!)"
    }
   
    @IBAction func back_click(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    @IBAction func account_click(_ sender: Any) {
        
      /*  let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    
    
    func no_addon() {
        
        
        
        let storyBoard11: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewControllerr = storyBoard11.instantiateViewController(withIdentifier: "addon") as! addon_vc
        
        let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
        print(dat)
        let d = dat["addons"] as! NSArray
   
        if d.count > 0{
            
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "addon")
            
            userdata_services.instance.quantity = "\(self.count_lbl.text!)"
           
            
            self.present(newViewController, animated: false, completion: nil)
            
        }else{
            var img_name = ( "\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            userdata_services.instance.quantity = "\(self.count_lbl.text!)"
            userdata_services.instance.product = [
                
                "product_id" : "\(dat["id"]!)",
                "quantity" :  "\(userdata_services.instance.quantity)",
                "title":"\(dat["title"]!)",
                "description":"\(dat["description"]!)",
                "image":"\(img_name)",
                "price":"\(price.text)",
                "order_item_addon":[],
                "order_item_list": []
                
            ]
            
     
            let order_item_addon = NSMutableArray()
            print(newViewControllerr.all_data)
            print(newViewControllerr.all_data.count)
            
            
            userdata_services.instance.product["order_item_addon"] = NSArray()
        
            let order_item_list = NSMutableArray()
            let item_list = dat["item_list"] as! NSArray
            
       
            userdata_services.instance.quantity = "\(self.count_lbl.text!)"
            
            userdata_services.instance.product["order_item_list"] = NSArray()
            
            
            userdata_services.instance.product["price"] = "\(price.text!)"
            print("my product list : \(userdata_services.instance.product)")
            
            userdata_services.instance.cart[userdata_services.instance.cart.count] = userdata_services.instance.product
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
            
            
            self.present(newViewController, animated: false, completion: nil)
            
            
        }
        
      
        
    }
    
    
    func no_attributes(){
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewControllerrr = storyBoard1.instantiateViewController(withIdentifier: "customize") as! customization_VC
        
        
        
        let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
        var img_name = ( "\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        userdata_services.instance.product = [
            
            "product_id" : "\(dat["id"]!)",
            "quantity" :  "\(userdata_services.instance.quantity)",
            "title":"\(dat["title"]!)",
            "description":"\(dat["description"]!)",
            "image":"\(img_name)",
            "price":"\(newViewControllerrr.total_price)",
            "order_item_addon":[],
            "order_item_list": []
            
        ]
        
        //1
        let order_item_list = NSMutableArray()
        let item_list = dat["item_list"] as! NSArray
        
        
        for i1 in 0...(item_list.count - 1){
            
            let item = item_list[i1] as! Dictionary<String,Any>
            
            
            let item_attributes = item["item_attributes"] as! NSArray
            if item_attributes.count > 0{
                
                
                for k in 0...(item_attributes.count - 1){
                    
                    
                    var attributes = item_attributes[k] as! Dictionary<String,Any>
                    
                    
                    
                    let attribute_values = attributes["attribute_values"] as! NSArray
                    
                    //2
                    let order_item_list_attributes = NSMutableArray()
                    
                    
                    let order_item_list_attribute_values = NSMutableArray()
                    if attribute_values.count > 0{
                        print(attribute_values)
                        for i2 in 0...(attribute_values.count-1){
                            
                            var  attribute_value_particular = attribute_values[i2] as! Dictionary<String,Any>
                            let attribute_counts = userdata_services.instance.attributes_count[i1] as! NSArray
                            
                            
                            print(attribute_counts[k])
                            if "\(attribute_counts[k])" != "0"{
                                
                                if attribute_counts.count - 1 >= i2{
                                    newViewControllerrr.att = attribute_counts[i2] as! NSArray
                                }
                                let at = newViewControllerrr.att[i2]
                                print(at)
                                
                                newViewControllerrr.total_price = newViewControllerrr.total_price + (Double("\(attribute_value_particular["price"]!)")! * Double("\(at)")!)
                                print(newViewControllerrr.total_price)
                                order_item_list_attribute_values[order_item_list_attribute_values.count] = [
                                    
                                    "item_list_attribute_value_id" : "\(attribute_value_particular["id"]!)",
                                    "quantity" :  "\(newViewControllerrr.att[i2])"
                                    
                                ]
                                
                            }
                            
                        }
                        if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                            
                            order_item_list_attributes[order_item_list_attributes.count] = ["item_list_attribute_id" : "\(attributes["id"]!)",
                                "quantity" : "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)"  ,
                                
                                "order_item_list_attribute_values" : order_item_list_attribute_values
                                
                                
                            ]
                        }
                        if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                            order_item_list[order_item_list.count] = [
                                
                                "item_list_id" : "\(item["id"]!)",
                                "quantity" :  "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)",
                                "order_item_list_attributes" :order_item_list_attributes //[]
                                
                                ] as Dictionary<String,Any>
                        }
                        
                    }
                }
            }
            
        }
        
        userdata_services.instance.product["order_item_list"] = order_item_list
        
        
        userdata_services.instance.product["price"] = "\(newViewControllerrr.total_price)"
        
        print("my product list : \(userdata_services.instance.product)")
        
        userdata_services.instance.cart[userdata_services.instance.cart.count] = userdata_services.instance.product
      
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

