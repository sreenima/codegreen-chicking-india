//
//  side_drawer_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 13/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import StoreKit

class side_drawer_ViewController: UIViewController {
    @IBOutlet weak var profile_pic: UIImageView!
    @IBOutlet weak var about_us_btn: UIButton!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var about_us_lbl: UILabel!
    @IBOutlet weak var about_us_img: UIImageView!
    @IBOutlet weak var edit_btn: UIButton!
    @IBOutlet weak var home_btn: UIButton!
    @IBOutlet weak var food_menu_btn: UIButton!
    @IBOutlet weak var track_order_btn: UIButton!
    @IBOutlet weak var saved_address_btn: UIButton!
    @IBOutlet weak var notification_btn: UIButton!
    @IBOutlet weak var promotion_btn: UIButton!
    @IBOutlet weak var rate_app_btn: UIButton!
    @IBOutlet weak var about_app_btn: UIButton!
    @IBOutlet weak var BG_view: UIView!
    
    @IBOutlet weak var home_img: UIImageView!
    @IBOutlet weak var home_lbl: UILabel!
    @IBOutlet weak var foodmenu_img: UIImageView!
    @IBOutlet weak var foodmenu_lbl: UILabel!
    @IBOutlet weak var trackorder_img: UIImageView!
    @IBOutlet weak var trackorder_lbl: UILabel!
    @IBOutlet weak var savedaddress_img: UIImageView!
    @IBOutlet weak var savedaddress_lbl: UILabel!
    @IBOutlet weak var notification_img: UIImageView!
    @IBOutlet weak var notification_lbl: UILabel!
    @IBOutlet weak var promotion_img: UIImageView!
    @IBOutlet weak var promotion_lbl: UILabel!
    @IBOutlet weak var contact_us_image: UIImageView!
    @IBOutlet weak var contact_us_lbl: UILabel!
    @IBOutlet weak var rateapp_img: UIImageView!
    @IBOutlet weak var rateapp_lbl: UILabel!
    @IBOutlet weak var aboutapp_img: UIImageView!
    @IBOutlet weak var aboutapp_lbl: UILabel!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var keep = NSInteger()
    var kap = NSInteger()
    
    var link = String()
    var condition_time = TimeInterval()
    var condition_time2 = TimeInterval()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        if authentication_services.instance.isloggedin == false{
            rateapp_lbl.text = "Register"
            rateapp_img.image = #imageLiteral(resourceName: "about")
        }
        else{
            rateapp_lbl.text = "Rate The App"
            rateapp_img.image = #imageLiteral(resourceName: "star grey")
        }
        
        if authentication_services.instance.isloggedin == false{
            aboutapp_lbl.text = "LogIn"
            aboutapp_img.image = #imageLiteral(resourceName: "log-in")
        }
        else{
            aboutapp_lbl.text = "Log Out"
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
        }
        
        if  authentication_services.instance.isloggedin != false {
            
            if userdata_services.instance.USER_NAME == "" && userdata_services.instance.USER_MOBILE == "" {
                
                spinner.startAnimating()
                authentication_services.instance.user_details(){ (success) in
                    self.spinner.stopAnimating()
                    if success {
                        self.spinner.stopAnimating()
                        if userdata_services.instance.user_details_data["error"]  == nil{
                            
                            
                            if userdata_services.instance.user_details_data["status"]  as! String == "ok"{
                                
                                let data1 : NSArray = userdata_services.instance.user_details_data["data"] as! NSArray
                                print(userdata_services.instance.user_details_data["data"])
                                
                                print (data1)
                                
                                let firstdata = data1[0] as! Dictionary <String,Any>
                                
                                userdata_services.instance.USER_MOBILE = (firstdata["mobileno"] as? String)!
                                self.phone.text = "0\(firstdata["mobileno"] as! String)"
                                
                                let secound_data = firstdata["userprofile"] as! Dictionary<String,Any>
                                print(secound_data)
                                // let thrd_data = secound_data[0] as! Dictionary <String,Any>
                                
                                userdata_services.instance.USER_NAME = (secound_data["name"] as? String)!
                                self.Name.text =  secound_data["name"] as? String
                                
                                
                                
                            }
                            else{
                                self.spinner.stopAnimating()
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.user_details_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            //userdata_services.instance.logout()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                            
                            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        
                    }
                        
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
                
                
                
            }else{
                
                
                self.Name.text = userdata_services.instance.USER_NAME
                self.phone.text = userdata_services.instance.USER_MOBILE
                
            }
            
        }
        
        
        
        
        
        
        profile_pic.layer.cornerRadius =  profile_pic.frame.size.width / 2
        profile_pic.clipsToBounds = true
        
        var returnValue: NSInteger = UserDefaults.standard.object(forKey: "keepvalue") as! NSInteger
        kap = returnValue
        if kap == 1{
            
            home_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            home_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            home_img.image = #imageLiteral(resourceName: "icon (1)")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            about_us_img.image = #imageLiteral(resourceName: "Custom-V2 (1)")
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            
            
            
            
        }
        else if kap == 2{
            
            
            foodmenu_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            foodmenu_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger (1)")
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
        }
        else if kap == 3{
            
            trackorder_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            trackorder_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike (1)")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
        }
        else if kap == 56{
            
            
            notification_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            notification_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            notification_img.image = #imageLiteral(resourceName: "notification-1")
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            
            
        }
        else if kap == 7{
            
            
            contact_us_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            contact_us_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-2")
            
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger (1)-1")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            
            
        }
        else if kap == 8{
            
            
            about_us_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            about_us_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            about_us_img.image = #imageLiteral(resourceName: "Custom-V2")
            
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger (1)-1")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            
            
        }
        else if kap == 4{
            
            savedaddress_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            savedaddress_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)-1")
            
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            about_us_img.image = #imageLiteral(resourceName: "Custom-V2 (1)")
            
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger (1)-1")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            
            rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            
            if authentication_services.instance.isloggedin == false{
                rateapp_lbl.text = "Register"
                rateapp_img.image = #imageLiteral(resourceName: "about")
            }
            else{
                rateapp_lbl.text = "Rate The App"
                rateapp_img.image = #imageLiteral(resourceName: "star grey")
            }
            
            if authentication_services.instance.isloggedin == false{
                aboutapp_lbl.text = "LogIn"
                aboutapp_img.image = #imageLiteral(resourceName: "log-in")
            }
            else{
                aboutapp_lbl.text = "Log Out"
                aboutapp_img.image = #imageLiteral(resourceName: "logout")
            }
            
            
            
            
        }
        BG_view.layer.shadowColor = UIColor.black.cgColor
        BG_view.layer.shadowOpacity = 0.5
        BG_view.layer.shadowOffset = CGSize(width: 0, height: 1)
        BG_view.layer.shadowRadius = 3
        //
        //  BG_view.layer.shouldRasterize = true
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // DESIGN OF THIS PAGE********************************************************************
    
    @IBAction func home_click(_ sender: Any) {
        
        
        UserDefaults.standard.set(1, forKey: "keepvalue")
        home_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        home_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        home_img.image = #imageLiteral(resourceName: "icon (1)")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        if authentication_services.instance.isloggedin == false{
            rateapp_lbl.text = "Register"
            rateapp_img.image = #imageLiteral(resourceName: "about")
        }
        else{
            rateapp_lbl.text = "Rate The App"
            rateapp_img.image = #imageLiteral(resourceName: "star grey")
        }
        
        if authentication_services.instance.isloggedin == false{
            aboutapp_lbl.text = "LogIn"
            aboutapp_img.image = #imageLiteral(resourceName: "log-in")
        }
        else{
            aboutapp_lbl.text = "Log Out"
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
        }
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func foo_click(_ sender: Any) {
        
        UserDefaults.standard.set(2, forKey: "keepvalue")
        foodmenu_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        foodmenu_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger (1)")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        
        if authentication_services.instance.isloggedin == false{
            rateapp_lbl.text = "Register"
            rateapp_img.image = #imageLiteral(resourceName: "about")
        }
        else{
            rateapp_lbl.text = "Rate The App"
            rateapp_img.image = #imageLiteral(resourceName: "star grey")
        }
        
        if authentication_services.instance.isloggedin == false{
            aboutapp_lbl.text = "LogIn"
            aboutapp_img.image = #imageLiteral(resourceName: "log-in")
        }
        else{
            aboutapp_lbl.text = "Log Out"
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
        }
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func track_click(_ sender: Any) {
        
        
        
        UserDefaults.standard.set(3, forKey: "keepvalue")
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        
        userdata_services.instance.fav_check = "trac"
        
        revealViewController()?.revealToggle(nil)
        
        
        
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func savedadd_click(_ sender: Any) {
        UserDefaults.standard.set(4, forKey: "keepvalue")
        userdata_services.instance.side_btn_needed = "yes"
        userdata_services.instance.no_address = "no"
        userdata_services.instance.make_address = "no"
        savedaddress_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        savedaddress_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)-1")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        
        
        if authentication_services.instance.isloggedin == false{
            rateapp_lbl.text = "Register"
            rateapp_img.image = #imageLiteral(resourceName: "about")
        }
        else{
            rateapp_lbl.text = "Rate The App"
            rateapp_img.image = #imageLiteral(resourceName: "star grey")
        }
        
        if authentication_services.instance.isloggedin == false{
            aboutapp_lbl.text = "LogIn"
            aboutapp_img.image = #imageLiteral(resourceName: "log-in")
        }
        else{
            aboutapp_lbl.text = "Log Out"
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
        }
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "side_saved_add") 
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    @IBAction func Account_Click(_ sender: Any) {
        
        
       let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)
        
        
        
        
        
        
    }
    
    
    
    
    
    
    @IBAction func noti_click(_ sender: Any) {
        
        UserDefaults.standard.set(56, forKey: "keepvalue")
        
        notification_lbl.textColor = UIColor(red:194, green: 0.0, blue: 0.0, alpha: 1.0)
        notification_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        notification_img.image = #imageLiteral(resourceName: "notification-1")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        rateapp_img.image = #imageLiteral(resourceName: "star grey")
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        aboutapp_img.image = #imageLiteral(resourceName: "about")
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5")// as! Notification_ViewController
        userdata_services.instance.offer_check = "noti"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)
    }
    @IBAction func promo_click(_ sender: Any) {
        
        UserDefaults.standard.set(56, forKey: "keepvalue")
        
        promotion_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        promotion_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)-1")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        
        if authentication_services.instance.isloggedin == false{
            rateapp_lbl.text = "Register"
            rateapp_img.image = #imageLiteral(resourceName: "about")
        }
        else{
            rateapp_lbl.text = "Rate The App"
            rateapp_img.image = #imageLiteral(resourceName: "star grey")
        }
        
        if authentication_services.instance.isloggedin == false{
            aboutapp_lbl.text = "LogIn"
            aboutapp_img.image = #imageLiteral(resourceName: "log-in")
        }
        else{
            aboutapp_lbl.text = "Log Out"
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
        }
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)
        
        
        
        
    }
    
    // promotion_lbl.textColor = UIColor(red: 190, green: 190, blue: 190, alpha: 1.0)
    
    @IBAction func rateapp_click(_ sender: Any) {
        
        
        if authentication_services.instance.isloggedin == false{
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            about_us_img.image = #imageLiteral(resourceName: "Custom-V2 (1)")
            
            rateapp_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            rateapp_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            rateapp_img.image = #imageLiteral(resourceName: "star")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            aboutapp_img.image = #imageLiteral(resourceName: "about")
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "register") as! register_ViewController
            
            self.present(newViewController, animated: false, completion: nil)
            
            
        }
        else{
            
            
            about_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            about_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            about_us_img.image = #imageLiteral(resourceName: "Custom-V2 (1)")
            
            rateapp_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
            rateapp_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            rateapp_img.image = #imageLiteral(resourceName: "star")
            
            promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            promotion_img.image = #imageLiteral(resourceName: "gift (1)")
            
            notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            notification_img.image = #imageLiteral(resourceName: "notification")
            
            savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
            
            trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
            
            foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
            
            home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            home_img.image = #imageLiteral(resourceName: "icon-1")
            
            contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
            aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
            aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
            aboutapp_img.image = #imageLiteral(resourceName: "logout")
            
           
            
            let urlString = "itms-apps://itunes.apple.com/app/id1451570624"
           
            let url = URL(string: urlString)!
            if #available(iOS 10.0, *) {
               
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               
                UIApplication.shared.openURL(url)
            }
            
            if #available( iOS 10.3,*){
               
                SKStoreReviewController.requestReview()
            }
            
            
        }
    }
    @IBAction func about_us_click(_ sender: Any) {
        UserDefaults.standard.set(8, forKey: "keepvalue")
        about_us_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        about_us_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        about_us_img.image = #imageLiteral(resourceName: "Custom-V2")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal6")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func contact_us_click(_ sender: Any) {
        UserDefaults.standard.set(7, forKey: "keepvalue")
        
        contact_us_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        contact_us_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-2")
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        aboutapp_img.image = #imageLiteral(resourceName: "logout")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        promotion_img.image = #imageLiteral(resourceName: "gift (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        
        aboutapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        aboutapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "side_callus")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func aboutapp_click(_ sender: Any) {
        
        aboutapp_lbl.textColor = UIColor(red: 194, green: 0.0, blue: 0.0, alpha: 1.0)
        aboutapp_lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        aboutapp_img.image = #imageLiteral(resourceName: "about (1)")
        
        rateapp_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        rateapp_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        rateapp_img.image = #imageLiteral(resourceName: "star grey")
        
        promotion_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        promotion_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        
        notification_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        notification_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        notification_img.image = #imageLiteral(resourceName: "notification")
        
        savedaddress_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        savedaddress_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        savedaddress_img.image = #imageLiteral(resourceName: "placeholder (1)")
        
        trackorder_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        trackorder_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        trackorder_img.image = #imageLiteral(resourceName: "delivery-motorbike-1")
        
        foodmenu_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        foodmenu_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        foodmenu_img.image = #imageLiteral(resourceName: "cheese-burger-1")
        contact_us_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        contact_us_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        contact_us_image.image = #imageLiteral(resourceName: "phone-book-1 (1)")
        
        home_lbl.textColor = #colorLiteral(red: 0.5019607843, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        home_lbl.font = UIFont(name: "Helvetica Neue", size: 14)
        home_img.image = #imageLiteral(resourceName: "icon-1")
        
        if aboutapp_lbl.text == "Log Out"{
            
            authentication_services.instance.isloggedin = false
            userdata_services.instance.USER_MOBILE = ""
            userdata_services.instance.USER_NAME = ""
            userdata_services.instance.logout()
        }
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
        
       
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    //******************************************************************************************
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
