//
//  offers_noti_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class offers_noti_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var table_vw: UITableView!
    var data1 = NSArray()
    var imageCache = NSCache<NSString,UIImage>()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if  authentication_services.instance.isloggedin == false
        {
            
            
            print("Not logged in!")
            
        }
        else{
            var header = [
                "Authorization":"Bearer \(authentication_services.instance.token)",
                "Content-Type": "application/json; charset=utf-8"
            ]
            spinner.startAnimating()
            Alamofire.request("\("\(BASE_URL)PromotionalNotificationList")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                
                if response.result.error == nil {
                    self.spinner.stopAnimating()
                    guard let response_data = response.data else {return}
                    
                    let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                    print(json as Any)
                    let doom = json as! [String : Any]
                    if doom["error"] == nil{
                        
                        if doom["status"] as! String == "ok"{
                            print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                            print(doom)
                            self.data1 = doom["data"] as! NSArray
                            print(self.data1)
                            print(authentication_services.instance.token)
                            self.table_vw.reloadData()
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                            
                        }
                    }
                    else{
                        self.spinner.stopAnimating()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    // Do any additional setup after loading the view.
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                    
                    
                }
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.data1.count > 15{
            return 15
        }else{
            return data1.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView1 = UIView()
        headerView1.backgroundColor = UIColor.clear
        return headerView1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! offers_noti_TableViewCell
        
        
        cell.offer_img_vw.layer.borderWidth = 1
        cell.offer_img_vw.layer.borderColor = UIColor.clear.cgColor
        cell.offer_img_vw.layer.masksToBounds = true
        cell.offer_img_vw.layer.cornerRadius = 8
        cell.offer_img_vw.layer.shadowColor = UIColor.black.cgColor
        cell.offer_img_vw.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.offer_img_vw.layer.shadowRadius = 3.0
        cell.offer_img_vw.layer.shadowOpacity = 0.2
        cell.offer_img_vw.layer.masksToBounds = false
        cell.offer_img_vw.layer.backgroundColor = UIColor.white.cgColor
        
        var dat = data1[indexPath.section] as! Dictionary<String,Any>
        
        
        
        var image_name = dat["image"] as? String
        
        if let cacheImage = imageCache.object(forKey: image_name! as NSString ){
            print("image cache")
            cell.offer_img_vw.image = cacheImage
            //return
        }
        else{
            
            
            print("\(IMAGE_URL)promotions/\(String(describing: image_name!))")
            self.spinner.startAnimating()
            Alamofire.request("\(IMAGE_URL)promotions/\(String(describing: image_name!))").responseImage { response in
                //debugPrint(response)
                self.spinner.stopAnimating()
               
                
                if let image = response.result.value {
                    self.spinner.stopAnimating()
                    print("image downloaded: \(image)")
                    cell.offer_img_vw.image = image
                    self.imageCache.setObject(image, forKey: image_name! as NSString)
                }
                
            }
        }
        
        return cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dt = data1[indexPath.section] as! Dictionary<String,Any>
        
        var type = dt["type"] as! String
        
        if type == "product"{
            
            var id = dt["common_id"] as! String
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
            
            newViewController.cake_id = id
            self.present(newViewController, animated: false, completion: nil)
            
        }
        else{
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
            
            self.present(nnewViewController, animated: false, completion: nil)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
