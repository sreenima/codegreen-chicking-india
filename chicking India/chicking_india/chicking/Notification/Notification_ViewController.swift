//
//  Notification_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SWRevealViewController

class Notification_ViewController: UIViewController {
    @IBOutlet weak var button_bg_view: UIView!
    @IBOutlet weak var general_btn: UIButton!
    @IBOutlet weak var offers_btn: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var menu_btn: UIButton!
    
    @IBOutlet weak var offers_noti: UIView!
    @IBOutlet weak var general_noti: UIView!
    
    var contain = String()
    
    override func viewWillAppear(_ animated: Bool) {
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        back_btn.isHidden = true
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
             self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
        
        
        if contain == "offer" || userdata_services.instance.offer_check == "offer" {
            button_bg_view.layer.borderColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
            button_bg_view.layer.borderWidth = 1
            button_bg_view.layer.cornerRadius = 8
            
            general_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            offers_btn.backgroundColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
            offers_btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            general_btn.setTitleColor(#colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1), for: .normal)
            
            general_noti.isHidden = true
            offers_noti.isHidden = false
        }
        else{
            
            button_bg_view.layer.borderColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
            button_bg_view.layer.borderWidth = 1
            button_bg_view.layer.cornerRadius = 8
            
            offers_btn.titleLabel?.textColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
            general_btn.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            offers_btn.setTitleColor(#colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1), for: .normal)
            general_btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            general_noti.isHidden = false
        }
       
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func general_click(_ sender: Any) {
        
        general_btn.backgroundColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
        offers_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        offers_btn.setTitleColor(#colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1), for: .normal)
        general_btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        general_noti.isHidden = false
        offers_noti.isHidden = true
        
    }
    
    @IBAction func offers_click(_ sender: Any) {
        
        general_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        offers_btn.backgroundColor = #colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1)
        offers_btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        general_btn.setTitleColor(#colorLiteral(red: 0.7540450508, green: 0, blue: 0.007059185433, alpha: 1), for: .normal)
        
        general_noti.isHidden = true
        offers_noti.isHidden = false
        
    }
    
    
    
    @IBAction func back_click(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal3") //as! account_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    @IBAction func account_click(_ sender: Any) {
        
        /*let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
        
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
