//
//  general_noti_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 20/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class general_noti_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tbl_vw: UITableView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var imageCache = NSCache<NSString,UIImage>()
    var data1 = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if  authentication_services.instance.isloggedin == false
        {
            
            
            print("Not logged in!")
            
        }
        else{
            var header = [
                "Authorization":"Bearer \(authentication_services.instance.token)",
                "Content-Type": "application/json; charset=utf-8"
            ]
            spinner.startAnimating()
            Alamofire.request("\("\(BASE_URL)GeneralNotificationList")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                
                if response.result.error == nil {
                    self.spinner.stopAnimating()
                    guard let response_data = response.data else {return}
                    
                    let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                    print(json as Any)
                    let doom = json as! [String : Any]
                    if doom["error"] == nil{
                        
                        if doom["status"] as! String == "ok"{
                            print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                            print(doom)
                            self.data1 = doom["data"] as! NSArray
                            print(self.data1)
                            print(authentication_services.instance.token)
                            self.tbl_vw.reloadData()
                            
                        }
                        else{
                        let errorMsg = doom["message"]as? String
                        if errorMsg != ""{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: errorMsg, preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        }else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)

                        }

                            
                        }
                    }
                    else{
                        self.spinner.stopAnimating()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                       
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    // Do any additional setup after loading the view.
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                    
                    
                }
            }
            
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.data1.count > 15{
            return 15
        }else{
            return self.data1.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! general_noti_TableViewCell
        
        cell.vieww.layer.borderWidth = 1
        cell.vieww.layer.borderColor = UIColor.clear.cgColor
        cell.vieww.layer.masksToBounds = true
        cell.vieww.layer.cornerRadius = 8
        cell.vieww.layer.shadowColor = UIColor.black.cgColor
        cell.vieww.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.vieww.layer.shadowRadius = 3.0
        cell.vieww.layer.shadowOpacity = 0.2
        cell.vieww.layer.masksToBounds = false
        cell.vieww.layer.backgroundColor = UIColor.white.cgColor
        
        cell.img_vw.image = UIImage()
        
        var dat = data1[indexPath.section] as! Dictionary<String,Any>
        
        cell.date_lbl.text = dat["date_of_notification"] as? String
        cell.product_name.text = dat["description"] as? String
        var image_name = dat["image"] as? String
        
       
        if let cacheImage = imageCache.object(forKey: image_name! as NSString ){
            print("image cache")
            cell.img_vw.image = cacheImage
            //return
        }
        else{
            print("\(IMAGE_URL)cakes/\(String(describing: image_name!))")
            self.spinner.startAnimating()
            Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: image_name!))").responseImage { response in
                //debugPrint(response)
                self.spinner.stopAnimating()
                
                
                if let image = response.result.value {
                    self.spinner.stopAnimating()
                    print("image downloaded: \(image)")
                    cell.img_vw.image = image
                    self.imageCache.setObject(image, forKey: image_name! as NSString)
                }
            }
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "odp") as! Order_details_page_ViewController
        
        let touch = data1[indexPath.section] as! Dictionary<String,Any>
        let or_id = touch["common_id"] as! NSString
        newViewController.order_id = or_id
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
