//
//  contact_us_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/12/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import SWRevealViewController


class contact_us_ViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var phone_btn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var send_btn: UIButton!
    @IBOutlet weak var message_text_vw: UITextView!
    var pulseArray = [CAShapeLayer]()
    var phoneNumber = String()
    var tap = UITapGestureRecognizer()
    
    @IBOutlet weak var drawer_btn: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var BG_new: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        //  self.createPulse()
        view.addGestureRecognizer(tap)
        
        
        BG_new.layer.cornerRadius = 12
        
        //BG_new.layer.cornerRadius = 8
        BG_new.layer.borderWidth = 1
        BG_new.layer.borderColor = UIColor.clear.cgColor
        BG_new.layer.masksToBounds = true
        
        BG_new.layer.shadowColor = UIColor.black.cgColor
        BG_new.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        BG_new.layer.shadowRadius = 3.0
        BG_new.layer.shadowOpacity = 0.2
        BG_new.layer.masksToBounds = false
        BG_new.layer.backgroundColor = UIColor.white.cgColor
        
        
        message_text_vw.layer.cornerRadius = 6
        message_text_vw.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        message_text_vw.layer.borderWidth = 0.5
        
        message_text_vw.delegate = self
        
        phone_btn.layer.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        phone_btn.layer.cornerRadius = 15
        message_text_vw.text = "Type Here"
        message_text_vw.textColor = UIColor.lightGray
        
        if  authentication_services.instance.isloggedin == false {
            print("not logged in")
        }
        else{
            var header = [
                "Authorization":"Bearer \(authentication_services.instance.token)",
                "Content-Type": "application/json; charset=utf-8"
            ]
            spinner.startAnimating()
            Alamofire.request("\("\(BASE_URL)showContactus")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                
                if response.result.error == nil {
                    self.spinner.stopAnimating()
                    guard let response_data = response.data else {return}
                    
                    let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                    print(json as Any)
                    let doom = json as! [String : Any]
                    if doom["error"] == nil{
                        
                        if doom["status"] as! String == "ok"{
                            print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                            print(doom)
                            let data1 = doom["data"] as! Dictionary<String,Any>
                            let ttext = data1["attribute_value"]!
                            print(data1)
                            print(ttext)
                            self.phoneNumber = ttext as! String
                            self.phone_btn.setTitle("   \((ttext as? String)!)", for: .normal)
                            //                        self.phone_btn.titleLabel!.text = "Tel: \(ttext as? String)"
                            // self.txt_vw.text = txt
                        }
                        else{
                            
                            
                            var errorMsg = ""
                            
                            errorMsg = doom["message"]as? String ?? ""
                            
                            if errorMsg != ""{
                                
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: errorMsg, preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)

                            }else{
                                
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)

                                
                            }
                           
                            
                        }
                    }
                    else{
                        self.spinner.stopAnimating()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    // Do any additional setup after loading the view.
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                    
                    
                }
            }
        }
        if self.revealViewController() != nil {
            drawer_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
    }
    
    
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if message_text_vw.textColor == UIColor.lightGray {
            message_text_vw.text = nil
            message_text_vw.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if message_text_vw.text.isEmpty {
            message_text_vw.text = "message"
            message_text_vw.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func back_click(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func send_click(_ sender: Any) {
        let body = ["message": message_text_vw.text]
        
        
        
        
        let header = [
            "Authorization":"Bearer \(authentication_services.instance.token)",
            "Content-Type": "application/json; charset=utf-8"
        ]
        
        spinner.startAnimating()
        
        if message_text_vw.text == ""||message_text_vw.text == "message"||message_text_vw.text == "Type Here" {
            
            let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            animation.duration = 0.6
            animation.values = [-10.0, 10.0, -10.0, 10.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
            self.spinner.stopAnimating()
            self.send_btn.layer.add(animation, forKey: "shake")
            self.send_btn.layer.add(animation, forKey: "shake")
        }
        else{
            Alamofire.request("\("\(BASE_URL)contactUsStore")", method: .post, parameters: body as Parameters, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
                print(response)
                if response.result.error == nil {
                    self.spinner.stopAnimating()
                    guard let response_data = response.data else {return}
                    
                    let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                    print(json as Any)
                    let doom = json as! [String : Any]
                    if doom["error"] == nil{
                        
                        if doom["status"] as! String == "ok"{
                            
                            self.message_text_vw.text = ""
                            self.message_text_vw.endEditing(true)
                            print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                            print(doom)
                            let alert = UIAlertController(title: "", message: "Your Message Has Been Successfully Sent !", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        else{
                            
                            var errorMsg = ""
                            errorMsg = doom["message"] as! String
                            
                            if errorMsg != ""{
                                
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: errorMsg, preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                                
                                
                            }else{
                                
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                                                          
                                
                            }
                            
                          
                            
                        }
                    }
                    else{
                        self.spinner.stopAnimating()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    // Do any additional setup after loading the view.
                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
            }
        }
        
        
        
    }
    @IBAction func phone_click(_ sender: Any) {
        
        
        
        let numbersOnly = phoneNumber.replacingOccurrences(of: " ", with: "")
        
        if let url = URL(string: "tel://\(numbersOnly)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
                print("calling")
            } else {
                UIApplication.shared.openURL(url)
                print("calling")
            }
        }
        
        
    }
    
    
}
