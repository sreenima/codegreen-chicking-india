//
//  order_customizeViewController.swift
//  chicking
//
//  Created by Ios CGT on 15/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlacePicker

class order_customizeViewController: UIViewController, UITextViewDelegate {
    var type_of_delivery = String()
    var branches = NSDictionary()

    
    @IBOutlet var toast_view: UIView!
    @IBOutlet weak var aad_type_lbl: UILabel!
    var full_text = String()
    var addr = NSString()
    var addr_pos = NSString()
    var no_ad = NSString()
    var toch = String()
    var isAnimationClosePressed = false
    var isPromoCodeCheckCompleted = false ;
    
    @IBOutlet weak var promoApplyBtn: UIButton!
    @IBOutlet weak var BG_view: UIView!
    @IBOutlet weak var total_height: NSLayoutConstraint!
    @IBOutlet weak var cart_pop: UIButton!
    @IBOutlet weak var add_text: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var percentage_img: UIImageView!
    @IBOutlet weak var delivery_del_lbl: UILabel!
    var pickup_time = String()
    var ToT_Amount = String()
    @IBOutlet var line_view: UIView!
    
    var Promo_Applied = "no"
    
    @IBOutlet weak var man_height: NSLayoutConstraint!
    @IBOutlet weak var man_width: NSLayoutConstraint!
    @IBOutlet weak var man_img: UIImageView!
    @IBOutlet weak var pop_up_height: NSLayoutConstraint!
    @IBOutlet weak var po_height: NSLayoutConstraint!
    @IBOutlet weak var pop_txtfld: UITextView!
    @IBOutlet weak var pop_vw: UIView!
    @IBOutlet weak var gray_view: UIView!
    @IBOutlet var price_lbl: UILabel!
    @IBOutlet weak var add_address: UIButton!
    @IBOutlet weak var continue_btn: UIButton!
    @IBOutlet var delivery_lbl: UILabel!
    @IBOutlet var total_lbl: UILabel!
    @IBOutlet var delivery_ttle_lbl: UILabel!
    
    @IBOutlet var pick_up_time_lbl: UILabel!
    var border = String()
    var address_keep = NSString()
    var back_from = String()
    var branchID = ""
    
    @IBOutlet var promo: UILabel!
    @IBOutlet var havea_promo: UIButton!
    var first_address = Dictionary<String,Any>()
    
    @IBOutlet var discountstack: UIStackView!
    @IBOutlet var discount_lbl: UILabel!
    @IBOutlet var promo_txt: UITextField!
    @IBOutlet var promo_stack: UIStackView!
    @IBOutlet var delivery_height: NSLayoutConstraint!
    
    @IBOutlet var delivery_top_height: NSLayoutConstraint!
    ////////////////
    @objc func keyboard(){
        view.endEditing(true)
    }
    @IBOutlet var main_bg_height: NSLayoutConstraint!
    
    @IBAction func apply_promo(_ sender: UIButton) {
        self.isPromoCodeCheckCompleted = true
        keyboard()
        
        if promo_txt.text == ""{
            
            userdata_services.instance.order_details["promo_code_id"] = ""
            userdata_services.instance.order_details["promo_code_value"] = ""
            
            
            let alert2 = UIAlertController(title: "", message: "Please enter a valid promo code", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        
        }
        else{
            
            
            
            spinner.startAnimating()
            
            authentication_services.instance.promocode(promocode: "\(promo_txt.text!)", amount: self.border) { (success) in
                if success {
                if userdata_services.instance.promocode_data["status"] as! String == "ok"{

                let refreshAlert = UIAlertController(title: "", message: "Promocode Applied Successfully", preferredStyle: UIAlertControllerStyle.alert)

                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")

                self.Promo_Applied = "yes"

                self.spinner.stopAnimating()
                self.promo_txt.text = ""


                self.discountstack.isHidden = false
                self.havea_promo.isHidden = true
                self.promo_stack.isHidden = true



                let data1 = userdata_services.instance.promocode_data["data"] as! Dictionary<String,Any>


                if data1["type"] as! String == "flat"{


                if let an = data1["maximum_discount_amount"] as? String{

                let ann = Double(an)!

                let con = Double("\(data1["value"]!)")!

                if ann >= con{

                self.discount_lbl.text = "- INR \(data1["value"]!)"

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(data1["value"]!)"





                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg)))



                self.promo.text = "\(data1["code"]!)"

                self.line_view.isHidden = false

                }else{

                let ann = an as! String

                self.discount_lbl.text = "- INR \(ann)"

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(ann)"



                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(ann)")! as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(ann)")! as CVarArg)))





                self.promo.text = "\(data1["code"]!)"

                self.line_view.isHidden = false

                }

                }else{


                self.discount_lbl.text = "- INR \(data1["value"]!)"

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(data1["value"]!)"





                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg)))



                self.promo.text = "\(data1["code"]!)"

                self.line_view.isHidden = false
                }

                }
                else{

                if let an = data1["maximum_discount_amount"] as? String{

                let ann = Double(an)!

                let con =  Double("\(data1["value"]!)")!

                if ann >= con {


                let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100)))

                print(disc)

                let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100))))!


                self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(data1["value"]!)")!)/100 as CVarArg)))"

                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(discount)"

                self.promo.text = "\(data1["code"]!)"

                self.line_view.isHidden = false

                }else{

                let ann = an as! String

                let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(ann)")!)/100)))

                print(disc)

                let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(ann)")!)/100))))!


                self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(ann)")!)/100 as CVarArg)))"

                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(discount)"

                self.promo.text = "\(data1["code"]!)"



                self.line_view.isHidden = false


                }
                }else{


                let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100)))

                print(disc)

                let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100))))!


                self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(data1["value"]!)")!)/100 as CVarArg)))"

                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"

                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))

                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                userdata_services.instance.order_details["promo_code_value"] = "\(discount)"

                self.promo.text = "\(data1["code"]!)"

                self.line_view.isHidden = false

                }

                }


                }))

                self.present(refreshAlert, animated: true, completion: nil)
                }
                else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.promocode_data["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)

                }

                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                }
                
            }
         
        }
        
        
    }
    
    
    @IBAction func remove_promo(_ sender: Any) {
        
        self.Promo_Applied = "no"
        
        discountstack.isHidden = true
        havea_promo.isHidden = false
        promo_stack.isHidden = true
        promo.text = ""
        self.discount_lbl.text = ""
        
        
        self.total_lbl.text = "INR \(userdata_services.instance.total)"
        //        self.total_footer.text = "AED \(userdata_services.instance.total)"
        userdata_services.instance.order_details["promo_code_id"] = ""
        userdata_services.instance.order_details["promo_code_value"] = ""
    }
    
    
    @IBAction func cancel_promo(_ sender: Any) {
        keyboard()
        promo_txt.text = ""
        Promo_Applied = "no"
        promo_stack.isHidden = true
        havea_promo.isHidden = false
        percentage_img.isHidden = false
        
        
    }
    
    @IBAction func hvea_promocode(_ sender: UIButton) {
        
        
        havea_promo.isHidden = true
        promo_stack.isHidden = false
        percentage_img.isHidden = true
        promoApplyBtn.isHidden = false
        promoApplyBtn.isUserInteractionEnabled = true
        
        self.line_view.isHidden = true
        
    }
 
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
        
        if userdata_services.instance.cart.count != 0 {
            cart_pop.isHidden = false
            cart_pop.layer.cornerRadius = cart_pop.frame.height/2
            cart_pop.setTitle("\(userdata_services.instance.cart.count)", for:UIControlState.normal)
        }
        else{
            
            cart_pop.isHidden = true
        }
    }
    
    
    func toast_view2() {
        
        
            toast_view.isHidden = false
            toast_view.alpha = 1.0
            
            UIView.animate(withDuration: 0.5, delay: 2.0, options: [], animations: {
                
                self.toast_view.alpha = 0.0
                
            }) { (finished: Bool) in
                
                self.toast_view.isHidden = true
            }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        promoApplyBtn.isHidden = false
        promoApplyBtn.isUserInteractionEnabled = true
        delivery_del_lbl.isHidden = false
//        promo_stack.isHidden = false
        
      
//         toast_view.layer.cornerRadius = 16
        
        print("\(UIDevice.current.screenType)")
        
        let dev = "\(UIDevice.current.screenType)"
        
        
        if "iPhones_4_4S" == dev{
            
            self.delivery_del_lbl.isHidden = true
            
        }
        
        if "iPhones_5_5s_5c_SE" == dev{
            
            self.delivery_del_lbl.isHidden = true
        }
        
        
        
        userdata_services.instance.order_details["promo_code_id"] = ""
        userdata_services.instance.order_details["promo_code_value"] = ""
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(keyboard))
        
        // Do any additional setup after loading the view.
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
        
        
        userdata_services.instance.the_way = ""
        
        print(userdata_services.instance.product)
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        self.total_height?.constant = 0
        self.man_img.frame.size.height = 204
        self.man_img.frame.size.width = 184
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector((tap(gesture:))))
        gray_view.addGestureRecognizer(tapGesture)
        
        
        pop_txtfld.delegate = self 
        
        pop_vw.isHidden = true
        gray_view.isHidden = true
        po_height?.constant = 0
        pop_vw.layer.cornerRadius = 8.0
        pop_txtfld.layer.cornerRadius = 8.0
        pop_vw.layer.borderWidth = 1
        pop_vw.layer.borderColor = #colorLiteral(red: 0.7019607843, green: 0, blue: 0, alpha: 1)
        pop_vw.layer.shadowColor = UIColor.black.cgColor
        pop_vw.layer.shadowOpacity = 0.2
        pop_vw.layer.shadowOffset = CGSize(width: -1, height: 1)
        continue_btn.setTitle("Continue", for: .normal)
        pop_vw.layer.shadowRadius = 3
        
     // default Address
        
        if  authentication_services.instance.isloggedin != false {
            self.spinner.startAnimating()
            authentication_services.instance.show_first_address(){ (success) in
                if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.show_first_address_data["error"] == nil{

                if userdata_services.instance.show_first_address_data["status"]  as! String == "ok"{


                let dat  = userdata_services.instance.show_first_address_data["data"] as! Dictionary<String,Any>

                print("homeData = \(dat)")
                var first_address = userdata_services.instance.show_first_address_data["data"] as! Dictionary<String,Any>

// HOME DELIVERY ADDRESS ADDING

                if authentication_services.instance.Delivery_type != "store"{

                self.main_bg_height.constant = CGFloat( self.main_bg_height.constant - 10)
                self.pick_up_time_lbl.isHidden = true
                let txt = dat["address"] as? String
                print(first_address)
                if txt == ""
                {

                //  self.add_text.text = "\(dat["address_line_2"]!)"

                }
                else{
                //      self.add_text.text = "\(dat["address"]!)"
                }


                print(first_address)

                let type = first_address["type"] as! String
                let add = first_address["street"] as! String
                let flat = first_address["flat_name"] as! String
                let land = first_address["landmark"] as! String
                let building = first_address["building"] as! String

                self.branchID = dat["area_id"]as? String ?? ""
                print("self.branchID = \(self.branchID)")

                // call branch data for company id

                self.LoadBranch()



                if type == "home"{


                self.aad_type_lbl.text = "HOME"

                }
                else if type == "work"{
                self.aad_type_lbl.text = "WORK"

                }
                else{

                self.aad_type_lbl.text = "OTHER"
                }

                if building != ""{

                self.full_text = "\(building),"
                self.add_text.text = self.full_text
                }
                if add != ""{

                self.full_text = "\(self.full_text)\(add),"
                self.add_text.text = self.full_text
                }
                if flat != ""{

                self.full_text =  "\(self.full_text)\(flat),"
                self.add_text.text = self.full_text
                }
                if land != ""{

                self.full_text = "\(self.full_text)\(land)"
                self.add_text.text = self.full_text
                }

                else{
                var add2 = dat["name"] as! String
                if add2 != ""{
                self.add_text.text = add2
                }
                }
                }else{

                self.aad_type_lbl.text = "STORE"

                self.delivery_del_lbl.isHidden = true

               /* self.delivery_ttle_lbl.isHidden = true

                self.delivery_lbl.isHidden = true*/

                self.delivery_top_height.constant = 20

                self.delivery_height.constant = 30

                self.add_text.text = self.type_of_delivery.htmlToString

                self.pick_up_time_lbl.isHidden = false
                self.pick_up_time_lbl.text = "Pick Up Time: \(self.pickup_time)"

                self.add_address.setTitle("CHANGE PICKUP STORE", for: .normal)


                //Adding packing charge


                }
                userdata_services.instance.order_details["user_address_id"] = "\(dat["id"]!)"

                }
                else{

                print("error")



                }
                }

                else{
                userdata_services.instance.logout()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")

                //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String

                self.present(newViewController, animated: false, completion: nil)


                }

                }
                else{
                    self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
            
            if authentication_services.instance.Delivery_type != "store"{
                
                self.Load_Deliverydata()
                
            }
            else{
                self.Load_packingCharge()
            }
                              
            
        }
        
        
        if address_keep == "" {
            add_text.text = "No Address Saved in your Account  Please Add New Address"
            
        }
        else{
            add_text.text = address_keep as String
        }
        add_address.layer.cornerRadius = 8
        continue_btn.layer.cornerRadius = 8
        BG_view.layer.cornerRadius = 16.0
        BG_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        BG_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        BG_view.layer.shadowRadius = 3.0
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.masksToBounds = false
        // Do any additional setup after loading the view.
        
}
    
                func Load_packingCharge(){
                //get packing charge start
                self.spinner.startAnimating()
                authentication_services.instance.show_PackingCharge(){ (success) in
                if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.show_packing_charges["error"] == nil{

                if userdata_services.instance.show_packing_charges["status"]  as! String == "ok"{

                let packing_data  = userdata_services.instance.show_packing_charges["data"] as! Dictionary<String,Any>

                print("packing charge Data = \(packing_data)")

                var total = 0.00

                for i in 0...userdata_services.instance.cart.count-1{

                let itam = userdata_services.instance.cart[i] as! Dictionary<String,Any>

                total = total + Double("\(itam[ "price"]!)")!
                userdata_services.instance.order_details["total_price"] = "\(total)"

                let floa = Float(total)
                self.price_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.order_details["total_price"] = "\(total)"

                self.border = "\(total)"
                }


                if "\(packing_data["attribute_value"]!)" == "0"{

                self.delivery_lbl.text = "FREE"
                self.delivery_ttle_lbl.text = "Packing Charges"
                let floa = Float(total)

                self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!



                }
                else{
                    
                   
                    
                //added packing charge
                self.delivery_ttle_lbl.text = "Packing Charges"
                self.delivery_lbl.text = "INR \(packing_data["attribute_value"]!)"
                print("charge = \(packing_data["attribute_value"])")
                let total = Double("\(packing_data["charge"]!)")!+total
                userdata_services.instance.order_details["total_price"] = "\(total)"
                let floa = Float(total)
                self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                    
                    
                    
                    
/*
                if authentication_services.instance.Delivery_type == "store"{

                //added packing charge
          
                self.delivery_lbl.text = "INR \(packing_data["attribute_value"]!)"
                print("charge = \(packing_data["attribute_value"])")
                let total = Double("\(packing_data["charge"]!)")!+total
                userdata_services.instance.order_details["total_price"] = "\(total)"
                let floa = Float(total)
                self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                     
                     
                }else{
                self.delivery_lbl.text = "0.00"
               

                let floa = Float(total)

                self.total_lbl.text = "INR \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                }
                    */

                }
                }
                else{

                }
                }
                else{
                userdata_services.instance.logout()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")

                //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String

                self.present(newViewController, animated: false, completion: nil)
                }

                }
                else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)

                }
                //get delivery charge end


                }




                }
    
    
    
    
    
    
    
    
    
    
    func Load_Deliverydata(){
        
                   
         //get delivery charge start
                    self.spinner.startAnimating()
                    authentication_services.instance.show_delivery_charges(){ (success) in
                        if success {
                        self.spinner.stopAnimating()
                        if userdata_services.instance.show_delivery_charges["error"] == nil{

                        if userdata_services.instance.show_delivery_charges["status"]  as! String == "ok"{

                        let delivery_dat  = userdata_services.instance.show_delivery_charges["data"] as! Dictionary<String,Any>
                        print(delivery_dat)
                            
                            print("delivery charge Data = \(delivery_dat)")

                        var total = 0.00

                        for i in 0...userdata_services.instance.cart.count-1{

                            let itam = userdata_services.instance.cart[i] as! Dictionary<String,Any>

                        total = total + Double("\(itam[ "price"]!)")!
                        userdata_services.instance.order_details["total_price"] = "\(total)"
                            
                            let floa = Float(total)
                        self.price_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                        userdata_services.instance.order_details["total_price"] = "\(total)"

                        self.border = "\(total)"
                        }


                        if "\(delivery_dat["charge"]!)" == "0"{

                        self.delivery_lbl.text = "FREE"
                        self.delivery_ttle_lbl.text = "Delivery Charges"

                        var floa = Float(total)

                        self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                        userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!



                        }
                        else{
                            
                //added delivery charge
                self.delivery_lbl.text = "INR \(delivery_dat["charge"]!)"
                self.delivery_ttle_lbl.text = "Delivery Charges"
                print("charge = \(delivery_dat["charge"])")
                let total = Double("\(delivery_dat["charge"]!)")!+total
                userdata_services.instance.order_details["total_price"] = "\(total)"
                let floa = Float(total)
                self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                            
                            
                            
/*
                   if authentication_services.instance.Delivery_type != "store"{
                        
           //added delivery charge
                    self.delivery_lbl.text = "INR \(delivery_dat["charge"]!)"
                    print("charge = \(delivery_dat["charge"])")
                    let total = Double("\(delivery_dat["charge"]!)")!+total
                    userdata_services.instance.order_details["total_price"] = "\(total)"
                    let floa = Float(total)
                    self.total_lbl.text = "INR  \((String(format: "%.2f", (floa as CVarArg))))"

                    userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                    }
                    else{
                        self.delivery_lbl.text = "0.00"
                            
                            let floa = Float(total)

                        self.total_lbl.text = "INR \((String(format: "%.2f", (floa as CVarArg))))"

                        userdata_services.instance.total =  Double((String(format: "%.2f", (floa as CVarArg))))!
                            }
                            
                            */

                        }
                        }
                        else{
                            
                            

                        }
                        }
                        else{
                        userdata_services.instance.logout()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")

                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String

                        self.present(newViewController, animated: false, completion: nil)
                        }

                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
        //get delivery charge end

                        
                    }
                    
    }
    
    
    
    func LoadBranch (){
         
         
         authentication_services.instance.show_branches(cityid: "\(branchID)"){ (success) in
             if success {
                 self.spinner.stopAnimating()
                 if userdata_services.instance.show_branch_data["error"] == nil{
                     
                     if userdata_services.instance.show_branch_data["status"]  as! String == "ok"{
                         userdata_services.instance.data1 = userdata_services.instance.show_branch_data
                        
                         let selectedBranchData  = userdata_services.instance.show_branch_data["data"] as! NSDictionary
                         print("dict = \(selectedBranchData)")
                         self.branches = selectedBranchData
                        
                        if self.branches != nil{
                                          
                                let titleStr = self.branches["title"]as? String
                                          print("titleStr = \(titleStr)")
                                  let CompanyId = self.branches["company_id"] as? String
                                  print("Company_Id = \(CompanyId)")
                                  
                                  UserDefaults.standard.set(CompanyId, forKey: "companyID")
                                  
                                                
                                  }
                         
                     
                         
                     }
                     else{
                         self.spinner.stopAnimating()
                         let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_branch_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                         alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                         self.present(alert2, animated: true, completion: nil)
                     }
                 }
                     
                     
                 else{
                     userdata_services.instance.logout()
                     let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                     let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                     
                     //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                     
                     self.present(newViewController, animated: false, completion: nil)
                     
                     
                 }
                 
             }
             else{
                 self.spinner.stopAnimating()
                 let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                 alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                 self.present(alert2, animated: true, completion: nil)
                 
             }
         }
         
         
     }
    
    
    func AnimateBackgroundHeight() {
        
        UIView.animate(
            withDuration: 1,
            delay: 0.0,
            options: .curveEaseInOut,
            animations:  {
                self.continue_btn.setTitle("Skip", for: .normal)
                self.pop_vw.isHidden = false
                self.gray_view.isHidden = false
                self.promoApplyBtn.isHidden = true
                self.havea_promo.isHidden = true
                self.promoApplyBtn.isUserInteractionEnabled = false
                self.promo_stack.isHidden = true
                self.po_height.constant = 100
                self.total_height.constant = 829
                self.view.layoutIfNeeded()
        },
            completion: nil)
        
    }
    
    func AnimateBackgroundHeight1() {
        
        UIView.animate(
            withDuration: 0.5,
            animations:  {
                self.man_height.constant = 204
                self.man_width.constant = 184
                self.view.layoutIfNeeded()
        },
            completion: nil)
        
    }
    
    func AnimateBackgroundHeight2() {
        
        UIView.animate(
            withDuration: 0.5,
            animations:  {
                self.man_height.constant = 104
                self.man_width.constant = 84
                self.view.layoutIfNeeded()
        },
            completion: nil)
        
    }
    
   
    @objc func tap(gesture: UITapGestureRecognizer) {
        
        pop_txtfld.endEditing(true)
        
    }
    
    
    func  textViewDidBeginEditing(_ textView: UITextView) {
        
        pop_txtfld.text = ""
        continue_btn.setTitle("Proceed", for: .normal)
        pop_up_height.constant = 170
        self.AnimateBackgroundHeight2()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if pop_txtfld.text == ""{
            pop_txtfld.text = "Any Instructions For Us ?"
            continue_btn.setTitle("Skip", for: .normal)
        }else{
        continue_btn.setTitle("Proceed", for: .normal)
        }
        pop_up_height.constant = 20
        self.AnimateBackgroundHeight1()
    }
    
    @IBAction func close_click(_ sender: Any) {
        
        self.isAnimationClosePressed = true
        self.pop_vw.isHidden = true
        self.gray_view.isHidden = true
        self.po_height?.constant = 0
        self.total_height?.constant = 0
        gray_view.isHidden = true
        pop_vw.isHidden = true
        continue_btn.setTitle("Continue", for: .normal)
        pop_txtfld.endEditing(true)
    }
    
    
    
    @IBAction func add_address_click(_ sender: Any) {
        
if self.add_address.titleLabel?.text == "CHANGE PICKUP STORE"{
            
            let storyBoard2: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard2.instantiateViewController(withIdentifier: "pick_up_store")
            
               self.present(newViewController, animated: false, completion: nil)
            
        }else{
        
        let storyBoard2: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController1 = storyBoard2.instantiateViewController(withIdentifier: "svd_adr") as! saved_address_ViewController
        if self.add_text.text == "No Address Saved in your Account  Please Add New Address"{
            userdata_services.instance.no_address = "yes"
            userdata_services.instance.the_way = "no_ad"
            //            let config = GMSPlacePickerConfig(viewport: nil)
            //            let placePicker = GMSPlacePickerViewController(config: config)
            //
            //            placePicker.delegate = self as GMSPlacePickerViewControllerDelegate
            //            present(placePicker, animated: true, completion: nil)
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "search") as! search_area_ViewController
            
            newViewController.addr_pos = self.addr_pos
            newViewController.addr = self.addr
            newViewController.no_ad = self.no_ad
            //            newViewController.from_edit = self.from_edit
            
            
            
            self.present(newViewController, animated: false, completion: nil)
            
        }
        else{
            userdata_services.instance.no_address = "no"
            userdata_services.instance.make_address = "yes"
            newViewController1.from_edit = "yes"
            self.present(newViewController1, animated: false, completion: nil)
        }
        
    }
        
    }
    
    @IBAction func continue_click(_ sender: Any) {
        
        
        promoApplyBtn.isHidden = true
        havea_promo.isHidden = true
        percentage_img.isHidden = true
        
        
        
        
        if(self.isAnimationClosePressed == true){
            self.isAnimationClosePressed = false
       
            if pop_txtfld.text == "Any Instructions For Us ?"{
                
                pop_txtfld.text = ""
            }
            
            userdata_services.instance.order_details["order_items"] = userdata_services.instance.cart
            
            print(userdata_services.instance.order_details)
            
            userdata_services.instance.order_details["remarks"] = pop_txtfld.text!
            
            
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "checkout") as! check_out_ViewController
            self.present(newViewController, animated: false, completion: nil)
            
        }
        
        if Promo_Applied == "no"{
        if promo_txt.text == ""{
            self.isPromoCodeCheckCompleted = true
//            userdata_services.instance.order_details["promo_code_id"] = ""
//            userdata_services.instance.order_details["promo_code_value"] = ""
//
//
//            let alert2 = UIAlertController(title: "", message: "Please enter a valid promo code", preferredStyle: UIAlertControllerStyle.alert)
//            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert2, animated: true, completion: nil)
            
        }
        else{
            
            
            
            spinner.startAnimating()
            
            authentication_services.instance.promocode(promocode: "\(promo_txt.text!)", amount: self.border) { (success) in
                if success {
                    
                    if userdata_services.instance.promocode_data["status"] as! String == "ok"{
                        
                
                        
                        
                let refreshAlert = UIAlertController(title: "", message: "Promocode Applied Successfully", preferredStyle: UIAlertControllerStyle.alert)

                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                        
                    

                       // self.AnimateBackgroundHeight()
                    self.isPromoCodeCheckCompleted = true
                        self.Promo_Applied = "yes"
                        
//                        self.toast_view2()
                        
                        self.spinner.stopAnimating()
                        self.promo_txt.text = ""
                        
                        self.discountstack.isHidden = false
                        self.havea_promo.isHidden = true
                        self.promo_stack.isHidden = true
                        
                        
                        
                        let data1 = userdata_services.instance.promocode_data["data"] as! Dictionary<String,Any>
                        
                        
                        if data1["type"] as! String == "flat"{
                            
                            
                            if let an = data1["maximum_discount_amount"] as? String{
                                
                                var ann = Double(an)!
                                
                                var con = Double("\(data1["value"]!)")!
                                
                                if ann >= con{
                                    
                                    self.discount_lbl.text = "- INR \(data1["value"]!)"
                                    
                                    userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                    userdata_services.instance.order_details["promo_code_value"] = "\(data1["value"]!)"
                                    
                                    
                                    
                                    
                                    
                                    self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg))))"
                                    
                                    self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg)))
                                    
                                    
                                    
                                    self.promo.text = "\(data1["code"]!)"
                                    
                                    self.line_view.isHidden = false
                                    
                                }else{
                                    
                                    var ann = an as! String
                                    
                                    self.discount_lbl.text = "- INR \(ann)"
                                    
                                    userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                    userdata_services.instance.order_details["promo_code_value"] = "\(ann)"
                                    
                                    
                                    
                                    self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(ann)")! as CVarArg))))"
                                    
                                    self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(ann)")! as CVarArg)))
                                    
                                    
                                    
                                    self.promo.text = "\(data1["code"]!)"
                                    
                                    self.line_view.isHidden = false
                                    
                                }
                                
                            }else{
                                
                                
                                self.discount_lbl.text = "- INR \(data1["value"]!)"
                                
                                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                userdata_services.instance.order_details["promo_code_value"] = "\(data1["value"]!)"
                                
                                
                                
                                
                                
                                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg))))"
                                
                                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - Double("\(data1["value"]!)")! as CVarArg)))
                                
                                
                                
                                self.promo.text = "\(data1["code"]!)"
                                
                                self.line_view.isHidden = false
                            }
                            
                        }
                        else{
                            
                            if let an = data1["maximum_discount_amount"] as? String{
                                
                                let ann = Double(an)!
                                
                                let con =  Double("\(data1["value"]!)")!
                                
                                if ann >= con {
                                    
                                    
                                    let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100)))
                                    
                                    print(disc)
                                    
                                    let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100))))!
                                    
                                    
                                    self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(data1["value"]!)")!)/100 as CVarArg)))"
                                    
                                    self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"
                                    
                                    self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))
                                    
                                    userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                    userdata_services.instance.order_details["promo_code_value"] = "\(discount)"
                                    
                                    self.promo.text = "\(data1["code"]!)"
                                    
                                    self.line_view.isHidden = false
                                    
                                }else{
                                    
                                    var ann = an as! String
                                    
                                    let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(ann)")!)/100)))
                                    
                                    print(disc)
                                    
                                    let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(ann)")!)/100))))!
                                    
                                    
                                    self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(ann)")!)/100 as CVarArg)))"
                                    
                                    self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"
                                    
                                    self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))
                                    
                                    userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                    userdata_services.instance.order_details["promo_code_value"] = "\(discount)"
                                    
                                    self.promo.text = "\(data1["code"]!)"
                                    
                                    
                                    
                                    self.line_view.isHidden = false
                                    
                                    
                                }
                            }else{
                                
                                
                                let disc = (String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100)))
                                
                                print(disc)
                                
                                let discount = Double((String(format: "%.2f", Double(round(userdata_services.instance.total * Double("\(data1["value"]!)")!)/100))))!
                                
                                
                                self.discount_lbl.text = "- INR \(String(format: "%.2f", ((userdata_services.instance.total * Double("\(data1["value"]!)")!)/100 as CVarArg)))"
                                
                                self.total_lbl.text = "INR \((String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg))))"
                                
                                self.ToT_Amount = (String(format: "%.2f", (userdata_services.instance.total - discount as CVarArg)))
                                
                                userdata_services.instance.order_details["promo_code_id"] = "\(data1["id"]!)"
                                userdata_services.instance.order_details["promo_code_value"] = "\(discount)"
                                
                                self.promo.text = "\(data1["code"]!)"
                                
                                self.line_view.isHidden = false
                                
                            }
                            
                        }
                    
                    if self.continue_btn.titleLabel?.text == "Continue" {
                        if self.add_text.text == "No Address Saved in your Account  Please Add New Address" {
                            let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                            animation.duration = 0.6
                            animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                            self.add_address.layer.add(animation, forKey: "shake")
                            
                        }else{
                            print("animation not show")
                            if(self.isPromoCodeCheckCompleted == true){
                                self.isPromoCodeCheckCompleted = false
                                self.AnimateBackgroundHeight()
                            }
                        }
                    }
                }))

                                              

                       self.present(refreshAlert, animated: true, completion: nil)
                        
                    }
                    else{
                        self.isPromoCodeCheckCompleted = true
                        if userdata_services.instance.promocode_data["message"] as! String == "Cart value is not sufficient" {
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "Cart value is not sufficient to apply promocode", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        
                        }
                        
                        if self.continue_btn.titleLabel?.text == "Continue" {
                            if self.add_text.text == "No Address Saved in your Account  Please Add New Address" {
                                let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                                animation.duration = 0.6
                                animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                                self.add_address.layer.add(animation, forKey: "shake")
                                
                            }else{
                                print("animation not show")
                                if(self.isPromoCodeCheckCompleted == true){
                                    self.isPromoCodeCheckCompleted = false
                                    self.AnimateBackgroundHeight()
                                }
                            }
                        }
                    }
                }else{
                    self.spinner.stopAnimating()
                    
                    let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                }
                
            }
            
        }
        
        
    }
 
        if continue_btn.titleLabel?.text == "Continue" {
            if add_text.text == "No Address Saved in your Account  Please Add New Address" {
                let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                animation.duration = 0.6
                animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                add_address.layer.add(animation, forKey: "shake")
                
            }else{
                print("animation not show")
                if(self.isPromoCodeCheckCompleted == true){
                    self.isPromoCodeCheckCompleted = false
                    self.AnimateBackgroundHeight()
                }
                
            }
        }
        else{
            
            
            
            if add_text.text == "No Address Saved in your Account  Please Add New Address" {
                let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                animation.duration = 0.6
                animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                add_address.layer.add(animation, forKey: "shake")
                
            }
            else{
                
                
            let arr = NSMutableArray()
                
                
                if pop_txtfld.text == "Any Instructions For Us ?"{
                    
                    pop_txtfld.text = ""
                }
                
                
                
                userdata_services.instance.order_details["order_items"] = userdata_services.instance.cart
                
                print(userdata_services.instance.order_details)
                
                userdata_services.instance.order_details["remarks"] = pop_txtfld.text!
                
                
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "checkout") as! check_out_ViewController
                self.present(newViewController, animated: false, completion: nil)
            }
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
    }
    
    @IBAction func back_clicked(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
              userdata_services.instance.offer_check = "offer"
              // newViewController.contain = "offer"
              revealViewController()?.revealToggle(nil)
              self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func cart_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}




extension order_customizeViewController : GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        viewController.dismiss(animated: true, completion: nil)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            addr = place.formattedAddress! as NSString
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        
        
        newViewController.check = "no"
        if no_ad == "yes" {
            newViewController.no_add = no_ad
            newViewController.test = addr_pos
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            add_text.text = addr as String
        }
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        //dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
}




extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
}
