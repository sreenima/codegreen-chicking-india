//
//  check_out_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 18/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class check_out_ViewController: UIViewController {
    var a = 1
    
    var opm = String()
    @IBOutlet weak var cod_bbt: UIButton!
    @IBOutlet weak var hide_view: UIView!
    @IBOutlet weak var text_availabilty_lbl: UILabel!
    
    @IBOutlet weak var conti_bbt: UIButton!
    @IBOutlet weak var fav_tick: UIButton!
    @IBOutlet weak var cod_img: UIImageView!
    @IBOutlet weak var op_img: UIImageView!
    @IBOutlet weak var op_bbt: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var condition_time = TimeInterval()
    var check_date = TimeInterval()
    var condition_time2 = TimeInterval()
    
    
    @IBAction func cod_btn(_ sender: Any) {
        cod_bbt.backgroundColor = #colorLiteral(red: 0.7019607843, green: 0, blue: 0, alpha: 1)
        cod_img.image = #imageLiteral(resourceName: "wallet")
        cod_bbt.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        op_bbt.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        op_img.image = #imageLiteral(resourceName: "credit-card")
        op_bbt.titleLabel?.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.opm = "no"
        
    }
    @IBAction func fav_clik(_ sender: Any) {
        if a % 2 == 0{
            
            fav_tick.setBackgroundImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
            
            userdata_services.instance.order_details["is_favorite"] = "no"
            
            
            a = a+1
            
        }
        else{
            fav_tick.setBackgroundImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
            userdata_services.instance.order_details["is_favorite"] = "yes"
            a = a+1
            
        }
        
    }
    @IBAction func back_clicked(_ sender: Any) {
        
        
        dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func op_click(_ sender: Any) {
        cod_bbt.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cod_img.image = #imageLiteral(resourceName: "wallet (1)")
        cod_bbt.titleLabel?.textColor = #colorLiteral(red: 0.7647058824, green: 0.7647058824, blue: 0.7647058824, alpha: 1)
        
        op_bbt.backgroundColor = #colorLiteral(red: 0.7019607843, green: 0, blue: 0, alpha: 1)
        op_img.image = #imageLiteral(resourceName: "credit-card (1)")
        op_bbt.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.opm = "yes"
        
        
        
        
    }
    @IBAction func continue_btn(_ sender: Any) {
        
        
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        
        dateFormatter.timeZone = TimeZone.current
        
        amFormatter.timeZone = TimeZone.current
        
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        
        
        
        
        spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                        
                        
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        var t1 = dat["storeOpen"] as! String
                        var t2 = dat["storeClose"] as! String
                        
                        
                        
                        
                        let da = DateFormatter()
                        
                        da.timeZone = TimeZone.current
                        
                        da.dateFormat = "hh:mm a"
                        
                        da.locale = Locale(identifier: "en-US")
                        let dateInFormatt = da.date(from: t1)!
                        var pure_date1 = da.string(from:dateInFormatt as Date)
                        
                        let dateInFor = da.date(from: pure_date1)!
                        
                        self.condition_time = dateInFor.timeIntervalSince1970
                        //////////////////////////////////////////////////////////////
                        let da2 = DateFormatter()
                        da2.timeZone = TimeZone.current
                        
                        da2.dateFormat = "hh:mm a"
                        
                        da2.locale = Locale(identifier: "en-US")
                        
                        let dateInFormattt = da2.date(from: t2)!
                        var pure_date2 = da2.string(from:dateInFormattt as Date)
                        
                        let dateInForr = da2.date(from: pure_date2)!
                        
                        self.condition_time2 = dateInForr.timeIntervalSince1970
                        
                        let texx = "Next Available At \(t1 )"
                        self.text_availabilty_lbl.text = texx
                        print(dateInFormat)
                        
                        if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                            
                            self.hide_view.isHidden = true
                            self.text_availabilty_lbl.isHidden = true
                            self.conti_bbt.isUserInteractionEnabled = true
                            
                            
                            
                            
                            print(userdata_services.instance.order_details)
                            if self.opm == "no"{
                                if self.a == 1
                                { userdata_services.instance.order_details["is_favorite"] = "no"
                                }
                                self.spinner.startAnimating()
                                
                                print(userdata_services.instance.order_details)
                                
                                userdata_services.instance.order_details["payment_option"] = "cash on delivery"
                                authentication_services.instance.order_processing(body: userdata_services.instance.order_details){ (success) in
                                    if success {
                                        self.spinner.stopAnimating()
                                        if userdata_services.instance.order_processing["error"] == nil{
                                            
                                            if userdata_services.instance.order_processing["status"]  as! String == "ok"{
                                                
                                                
                                                print(userdata_services.instance.order_details)
                                                
                                                userdata_services.instance.cart = []
                                                
                                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                
                                                
                                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "success")
                                                
                                                
                                                self.present(newViewController, animated: false, completion: nil)
                                                
                                                
                                                
                                            }
                                            else{
                                                self.spinner.stopAnimating()
                                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.order_processing["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                self.present(alert2, animated: true, completion: nil)
                                            }
                                        }
                                            
                                            
                                        else{
                                            userdata_services.instance.logout()
                                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                                            
                                            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                                            
                                            self.present(newViewController, animated: false, completion: nil)
                                            
                                            
                                        }
                                        
                                    }
                                    else{
                                        self.spinner.stopAnimating()
                                        let alert2 = UIAlertController(title: "", message: "Oops! Something Went Wrong.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert2, animated: true, completion: nil)
                                        
                                    }
                                    
                                }
                            }else{
                                if self.a == 1
                                { userdata_services.instance.order_details["is_favorite"] = "yes"
                                }
                                self.spinner.startAnimating()
                                
                                print(userdata_services.instance.order_details)
                                
                                userdata_services.instance.order_details["payment_option"] = "online payment"
                                authentication_services.instance.order_processing(body: userdata_services.instance.order_details){ (success) in
                                    if success {
                                        self.spinner.stopAnimating()
                                        if userdata_services.instance.order_processing["error"] == nil{
                                            
                                            if userdata_services.instance.order_processing["status"]  as! String == "pending"{
                                                
                                                print("orderDetails =\(userdata_services.instance.order_details)")
                                                print("888888888888888888888888888888888888888")
                                                print(userdata_services.instance.order_processing)
                                                let iddd = "\( userdata_services.instance.order_processing["data"]! )"
                                                print(iddd)
                                            
                                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                
                                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "payment") as! payment_ViewController
                                                
                                                newViewController.ORDER_id = iddd as! String
                                                self.present(newViewController, animated: false, completion: nil)
                                                
                                                
                                                
                                            }
                                            else{
                                                self.spinner.stopAnimating()
                                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.order_processing["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                self.present(alert2, animated: true, completion: nil)
                                            }
                                        }
                                            
                                            
                                        else{
                                            userdata_services.instance.logout()
                                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                                            
                                            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                                            
                                            self.present(newViewController, animated: false, completion: nil)
                                            
                                            
                                        }
                                        
                                    }
                                    else{
                                        
   //online payment
                                        
                                        
                                        let idd = "\( userdata_services.instance.order_processing["data"]! )"
                                        print(idd)
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        
                                        
                                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "payment") as! payment_ViewController
                                        
                                        newViewController.user_id = idd 
                                        self.present(newViewController, animated: false, completion: nil)
                                    }
                                    
                                }
                            }
                            
                            
                            
                            
                            
                        }else{
                            self.hide_view.isHidden = false
                            self.text_availabilty_lbl.isHidden = false
                            self.conti_bbt.isUserInteractionEnabled = true
                        }

                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        
        
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cod_bbt.setTitle("Cash On Delivery", for: .normal)
        
//        print("cmpID = \(UserDefaults.standard.object(forKey: "companyID"))")
        
        var companyID = ""
        companyID = UserDefaults.standard.object(forKey: "companyID") as! String
        
        print("companyID = \(companyID)")
        
        if companyID != nil{
            print("yes")
           

        }else{
            print("no online payment")
            
            op_bbt.isHidden = true
            op_img.isHidden = true
            
        }
        
        
        
        if authentication_services.instance.Delivery_type == "store"{
            
            cod_bbt.setTitle("Cash On Pickup", for: .normal)
            
        }
        
        
        opm = "no"
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        conti_bbt.isUserInteractionEnabled = false
        
        text_availabilty_lbl.layer.borderColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        text_availabilty_lbl.layer.cornerRadius = 5
        text_availabilty_lbl.layer.borderWidth = 0.5
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        let amInFormat = amFormatter.string(from: NSDate() as Date)
        
        
        
        print( dateInFormat)
        print(amInFormat)
        
        
        
        print(userdata_services.instance.order_details)
        
        
        
        
        conti_bbt.layer.cornerRadius = 8
        
        
        cod_bbt.layer.cornerRadius = 4
        cod_bbt.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cod_bbt.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cod_bbt.layer.shadowRadius = 3.0
        cod_bbt.layer.shadowOpacity = 0.2
        cod_bbt.layer.masksToBounds = false
        
        
        op_bbt.layer.cornerRadius = 4
        op_bbt.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        op_bbt.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        op_bbt.layer.shadowRadius = 3.0
        op_bbt.layer.shadowOpacity = 0.2
        op_bbt.layer.masksToBounds = false
        
        // Do any additional setup after loading the view.
        
        
        
        spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                        
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        
                        
                        
                        var t1 = dat["storeOpen"] as! String
                        var t2 = dat["storeClose"] as! String
                        
                        
                        
                        
                        let da = DateFormatter()
                        da.timeZone = TimeZone.current
                        da.dateFormat = "hh:mm a"
                        
                        da.locale = Locale(identifier: "en-US")
                        
                        let dateInFormatt = da.date(from: t1)!
                        var pure_date1 = da.string(from:dateInFormatt as Date)
                        
                        let dateInFor = da.date(from: pure_date1)!
                        
                        self.condition_time = dateInFor.timeIntervalSince1970
                        //////////////////////////////////////////////////////////////
                        let da2 = DateFormatter()
                        da2.timeZone = TimeZone.current
                        da2.dateFormat = "hh:mm a"
                        
                        da2.locale = Locale(identifier: "en-US")
                        
                        let dateInFormattt = da2.date(from: t2)!
                        var pure_date2 = da2.string(from:dateInFormattt as Date)
                        
                        let dateInForr = da2.date(from: pure_date2)!
                        
                        self.condition_time2 = dateInForr.timeIntervalSince1970
                        
                        let texx = "Next Available At \(t1 )"
                        self.text_availabilty_lbl.text = texx
                        
                        print(dateInFormat)
                        let a:Int? = Int(dateInFormat)
                        if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                            
                            self.hide_view.isHidden = true
                            self.text_availabilty_lbl.isHidden = true
                            self.conti_bbt.isUserInteractionEnabled = true
                        }else{
                            self.hide_view.isHidden = false
                            self.text_availabilty_lbl.isHidden = false
                            self.conti_bbt.isUserInteractionEnabled = true
                        }
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        
        
        
        
        
    }
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        newViewController.check_name = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
