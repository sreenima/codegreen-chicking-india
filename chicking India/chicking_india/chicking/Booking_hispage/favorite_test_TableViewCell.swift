//
//  favorite_test_TableViewCell.swift
//  chicking
//
//  Created by Ios CGT on 21/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class favorite_test_TableViewCell: UITableViewCell {
    @IBOutlet var price: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var status: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet weak var vview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
