//
//  book_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import SWRevealViewController

class book_ViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var track_text: UILabel!
    @IBOutlet weak var favorite_text: UILabel!
    @IBOutlet weak var menu_view: UIView!
    @IBOutlet weak var contain_vw: UIView!
    @IBOutlet weak var contain_vw2: UIView!
    @IBOutlet weak var contain_vw3: UIView!
    @IBOutlet weak var ongo_img: UIButton!
    @IBOutlet weak var pasto_img: UIButton!
    @IBOutlet weak var favo_img: UIButton!
    @IBOutlet weak var ongo_lbl: UILabel!
    @IBOutlet weak var favo_lbl: UILabel!
    @IBOutlet weak var pasto_lbl: UILabel!
    @IBOutlet weak var track_icon: UIButton!
    @IBOutlet weak var favorite_icon: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var menu_btn: UIButton!
    var check_name = String()
    
    
    override func viewWillAppear(_ animated: Bool) {
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("check_name = \(check_name)")
        
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
      
        UserDefaults.standard.set(3, forKey: "keepvalue")
        if check_name == "favorite" || userdata_services.instance.fav_check == "favorite"{
            
            self.favor_click([])
            
        }else if userdata_services.instance.fav_check == "delivered"{
            
            self.pasto_click([])
        }
        else{
            
            self.ongoing_click([])
        }
       
        menu_view.layer.cornerRadius = 12.0
        menu_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        menu_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        menu_view.layer.shadowRadius = 3.0
        menu_view.layer.shadowOpacity = 0.2
        menu_view.layer.masksToBounds = false
        
        //@##$%^&%$#$%^%$#$%^&%$#@$%^&%$#@$%^%$#@$%^$#$%^%$#$%^$#$%^%$#$%^
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
            
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
       
    }
    
    
    
    @IBAction func track_click(_ sender: Any) {
        contain_vw3.isHidden = false
        contain_vw.isHidden = true
        contain_vw2.isHidden = true
        
        ongo_img.setBackgroundImage(#imageLiteral(resourceName: "Group 463"), for: [])
        favo_img.setBackgroundImage(#imageLiteral(resourceName: "like (1)"), for: [])
        pasto_img.setBackgroundImage(#imageLiteral(resourceName: "clock"), for: [])
        ongo_lbl.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        favo_lbl.textColor = #colorLiteral(red: 0.5764705882, green: 0.6235294118, blue: 0.662745098, alpha: 1)
        pasto_lbl.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        favorite_text.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        track_text.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        track_icon.setBackgroundImage(#imageLiteral(resourceName: "delivery-motorbike-3"), for: [])
        favorite_icon.setBackgroundImage(#imageLiteral(resourceName: "favorite (1)-1"), for:[])
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
              userdata_services.instance.offer_check = "offer"
              // newViewController.contain = "offer"
              revealViewController()?.revealToggle(nil)
              self.present(newViewController, animated: false, completion: nil)
        
    }
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        contain_vw3.isHidden = true
        contain_vw.isHidden = false
        contain_vw2.isHidden = true
        
        
        ongo_img.setBackgroundImage(#imageLiteral(resourceName: "alarm-clock"), for: [])
        favo_img.setBackgroundImage(#imageLiteral(resourceName: "favorite (1)"), for: [])
        pasto_img.setBackgroundImage(#imageLiteral(resourceName: "clock"), for: [])
        ongo_lbl.textColor = #colorLiteral(red: 0.5491397737, green: 0.5898061057, blue: 0.627987688, alpha: 1)
        favo_lbl.textColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        pasto_lbl.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        favorite_icon.setBackgroundImage(#imageLiteral(resourceName: "favorite"), for: [])
        track_icon.setBackgroundImage(#imageLiteral(resourceName: "delivery-motorbike (1)-1"), for: [])
        favorite_text.textColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        track_text.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    
    @IBAction func back_click(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func ongoing_click(_ sender: Any) {
        
        
        contain_vw3.isHidden = false
        contain_vw.isHidden = true
        contain_vw2.isHidden = true
        
        ongo_img.setBackgroundImage(#imageLiteral(resourceName: "Group 463"), for: [])
        favo_img.setBackgroundImage(#imageLiteral(resourceName: "like (1)"), for: [])
        pasto_img.setBackgroundImage(#imageLiteral(resourceName: "clock"), for: [])
        ongo_lbl.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        favo_lbl.textColor = #colorLiteral(red: 0.5764705882, green: 0.6235294118, blue: 0.662745098, alpha: 1)
        pasto_lbl.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        track_text.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        favorite_text.textColor =  #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        track_icon.setBackgroundImage(#imageLiteral(resourceName: "delivery-motorbike-3"), for: [])
        favorite_icon.setBackgroundImage(#imageLiteral(resourceName: "favorite (1)-1"), for: [])
    }
    
    @IBAction func favor_click(_ sender: Any) {
        
        contain_vw3.isHidden = true
        contain_vw.isHidden = false
        contain_vw2.isHidden = true
        
        
        ongo_img.setBackgroundImage(#imageLiteral(resourceName: "alarm-clock"), for: [])
        favo_img.setBackgroundImage(#imageLiteral(resourceName: "favorite (1)"), for: [])
        pasto_img.setBackgroundImage(#imageLiteral(resourceName: "clock"), for: [])
        ongo_lbl.textColor = #colorLiteral(red: 0.5491397737, green: 0.5898061057, blue: 0.627987688, alpha: 1)
        favo_lbl.textColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        pasto_lbl.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        favorite_icon.setBackgroundImage(#imageLiteral(resourceName: "favorite"), for: [])
        favorite_text.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        track_text.textColor = #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        track_icon.setBackgroundImage(#imageLiteral(resourceName: "delivery-motorbike (1)-1"), for: [])
        
    }
    @IBAction func pasto_click(_ sender: Any) {
        
        contain_vw3.isHidden = true
        contain_vw.isHidden = true
        contain_vw2.isHidden = false
        
        ongo_img.setBackgroundImage(#imageLiteral(resourceName: "alarm-clock"), for: [])
        favo_img.setBackgroundImage(#imageLiteral(resourceName: "like (1)"), for: [])
        pasto_img.setBackgroundImage(#imageLiteral(resourceName: "clock (1)"), for: [])
        ongo_lbl.textColor = #colorLiteral(red: 0.5491397737, green: 0.5898061057, blue: 0.627987688, alpha: 1)
        favo_lbl.textColor = #colorLiteral(red: 0.5491397737, green: 0.5898061057, blue: 0.627987688, alpha: 1)
        pasto_lbl.textColor = #colorLiteral(red: 0.8235294118, green: 0, blue: 0, alpha: 1)
        track_text.textColor = #colorLiteral(red: 0.8117647059, green: 0, blue: 0, alpha: 1)
        favorite_text.textColor =  #colorLiteral(red: 0.568627451, green: 0.6235294118, blue: 0.6666666667, alpha: 1)
        favorite_icon.setBackgroundImage(#imageLiteral(resourceName: "favorite (1)-1"), for: [])
        track_icon.setBackgroundImage(#imageLiteral(resourceName: "delivery-motorbike-3"), for: [])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
