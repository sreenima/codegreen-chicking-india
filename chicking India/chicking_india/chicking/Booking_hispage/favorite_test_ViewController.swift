//
//  favorite_test_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class favorite_test_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    var appdelegate = AppDelegate()
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var tab: UITableView!
    var all_orders = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
//        LoadNotification()
        // Do any additional setup after loading the view.
      if authentication_services.instance.isloggedin != false{
        spinner.startAnimating()
        
        authentication_services.instance.favorite_orders(){ (success) in
            self.spinner.stopAnimating()
            if success {
                
                
            if userdata_services.instance.favorite_order_data["error"] == nil{
                    
                    if userdata_services.instance.favorite_order_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.favorite_order_data["data"] as! NSArray
                        
                        self.all_orders = dat
                        print(userdata_services.instance.favorite_order_data)
                        print(dat)
                       
                        
                        self.tab.reloadData()
                        
                        
                    }
                    else{
                       
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
                // self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        }
        }
    


    func numberOfSections(in tableView: UITableView) -> Int {
        

        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        

        print(all_orders)
        if all_orders.count > 0{
            if all_orders.count > 15{
                return 15
            }else{
        return all_orders.count
            }
        }else{
            return 0
        }
        
    }
    var imageCache = NSCache<NSString,UIImage>()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! favorite_test_TableViewCell
        
        cell.img.image = UIImage()
        cell.vview.layer.cornerRadius = 8
        cell.vview.layer.borderWidth = 1
        cell.vview.layer.borderColor = UIColor.clear.cgColor
        cell.vview.layer.masksToBounds = true
        
        cell.vview.layer.shadowColor = UIColor.black.cgColor
        cell.vview.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.vview.layer.shadowRadius = 3.0
        cell.vview.layer.shadowOpacity = 0.2
        cell.vview.layer.masksToBounds = false
        cell.vview.layer.backgroundColor = UIColor.white.cgColor
      
        var all_data = self.all_orders[indexPath.row] as! Dictionary<String,Any>
        
        print(all_data)
      
        var order_status = all_data["orderstatus"] as! Dictionary<String,Any>
        print(order_status)
        cell.status.text = "\(order_status["title"]!)"
        let item = all_orders
        
        cell.name.text = "Order ID : \(((item.value(forKey: "order_id") as! NSArray).object(at: indexPath.row)) )"
        print(cell.name.text as Any)
        
        print(item)
        
        cell.price.text = "\(((item.value(forKey: "total_price") as! NSArray).object(at: indexPath.row)) )"
        cell.date.text = "\(((all_orders.value(forKey: "date_of_purchase") as! NSArray).object(at: indexPath.row)) )"
        var da = "\(((all_orders.value(forKey: "created_at") as! NSArray).object(at: indexPath.row)) )"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en-US")
        let date = dateFormatter.date(from: da)
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        cell.date.text = dateFormatter.string(from: date!)
        
        
        let ordered_items = ((item.value(forKey: "orderitems") as! NSArray).object(at: indexPath.row)) as! NSArray
        
        if ordered_items.count > 0{
            var ordered_item = ordered_items[0] as! Dictionary<String,Any>
            let img_name = ("\(((ordered_item[ "image"]!)) )").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
        
        
        cell.img.layer.cornerRadius = 8
        cell.img.layer.borderColor = #colorLiteral(red: 0.737254902, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        cell.img.layer.borderWidth=1
        cell.img.clipsToBounds = true
        
        
        
        if let cacheImage = imageCache.object(forKey: img_name as NSString ){
            print("image cache")
            cell.img.image = cacheImage
            //return
        }
        else{
            print("image not cache")
            
            print("\(IMAGE_URL)cakes/\(String(describing: img_name))")
            // self.spinner.startAnimating()
            Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
              
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.img.image = image
                    self.imageCache.setObject(image, forKey: img_name as NSString)
                    //      }
                }
                
            }
            
        }
        
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var all_data = self.all_orders[indexPath.row] as! Dictionary<String,Any>
        print(all_data)
        var dataa = all_data["orderitems"] as! NSArray
        print(dataa)
        var datta = dataa[0] as! Dictionary<String,Any>
        var dt = datta["order_id"]
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "odp") as! Order_details_page_ViewController
        newViewController.order_id = dt as! NSString
        newViewController.from_noti = "no"
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
