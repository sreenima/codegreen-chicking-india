//
//  account_edit_page_newViewController.swift
//  chicking
//
//  Created by Ios CGT on 10/05/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class account_edit_page_newViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var email_txt_fld: UITextField!
    @IBOutlet weak var name_txt_fld: UITextField!
    @IBOutlet weak var phone_lbl: UILabel!
    
    @IBOutlet weak var bg_view: UIView!
    
    @IBOutlet weak var proceed_btn: UIButton!
    
    @IBOutlet weak var bg: UIView!
    
    @IBOutlet weak var bg2: UIView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    
    
    var idd = NSInteger()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        email_txt_fld.delegate = self
        
        bg_view.layer.cornerRadius = 15.0
        bg_view.layer.shadowColor = UIColor.black.cgColor
        bg_view.layer.shadowOpacity = 0.2
        bg_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        bg_view.layer.shadowRadius = 3
        // BG_view2.layer.shouldRasterize = true
        
        proceed_btn.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
        // Do any additional setup after loading the view.
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
        
        name_txt_fld.adddBottomBorderWithColor(color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), width: 0.5)
        
        email_txt_fld.adddBottomBorderWithColor(color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), width: 0.5)
        
        
        
        bg.addBottomBorderWithColor(color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), width: 0.5)
        
        bg2.addBottomBorderWithColor(color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), width: 0.5)
        
        
        spinner.startAnimating()
        authentication_services.instance.user_details(){ (success) in
            
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.user_details_data["error"]  == nil{
                    
                    
                    if userdata_services.instance.user_details_data["status"]  as! String == "ok"{
                        
                        let data1 : NSArray = userdata_services.instance.user_details_data["data"] as! NSArray
                        print(userdata_services.instance.user_details_data["data"])
                        print (data1)
                        
                        let firstdata = data1[0] as! Dictionary <String,Any>
                        
                        self.phone_lbl.text = "0\(String(describing: firstdata["mobileno"] as! String))"
                        
                        var secound_data = firstdata["userprofile"] as! Dictionary<String,Any>
                        print(secound_data)
                      
                        self.idd = secound_data["id"] as! NSInteger
                        self.name_txt_fld.text =  secound_data["name"] as? String
                        self.email_txt_fld.text = secound_data["email"] as? String
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.user_details_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == email_txt_fld{
            
            if  email_txt_fld.text!.isValidEmail(){
                
                print("valid email")
                
            }else{
                
                print("invalid email")
            }
            
            
            
        }
        
    }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    
    @IBAction func phone_edit_click(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "change_phone") as! change_phone_ViewController
        
        newViewController.phone = self.phone_lbl.text!
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func pass_edit_click(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "editpass")
        
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    @IBAction func back_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    @IBAction func proceed_click(_ sender: Any) {
        
        
        if name_txt_fld.text != "" && email_txt_fld.text != ""{
            
            if  email_txt_fld.text!.isValidEmail(){
                
                print("valid email")
                
                
                self.spinner.startAnimating()
                authentication_services.instance.account_edit_main(name: name_txt_fld.text!, email: email_txt_fld.text!,id1: "\(idd)" ){ (success) in
                    if success {
                        self.spinner.stopAnimating()
                        if userdata_services.instance.ACCOUNT_EDIT_MAIN_data["status"]  as! String == "ok"{
                            
                            
                            let alert2 = UIAlertController(title: "", message: "Details Updated Successfully", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                    
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal4")
                                    
                                    
                                    self.present(newViewController, animated: false, completion: nil)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                }
                            }))
                            self.present(alert2, animated: true, completion: nil)
                            
                            
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.ACCOUNT_EDIT_MAIN_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
            }else{
                
                let alert2 = UIAlertController(title: "", message: "Invalid Email.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
        }
        else{
            
            let alert2 = UIAlertController(title: "", message: "Please fill the data.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            
            
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}

extension UITextField {
    func adddTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func adddRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func adddBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func adddLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}

