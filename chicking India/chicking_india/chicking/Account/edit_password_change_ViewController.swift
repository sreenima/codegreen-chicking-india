//
//  edit_password_change_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 10/05/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class edit_password_change_ViewController: UIViewController {
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var old_pass_txt_fld: UITextField!
    @IBOutlet weak var new_pass_txt_fld: UITextField!
    @IBOutlet weak var re_enter_pass_txt_fld: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        old_pass_txt_fld.layer.borderWidth = 0.5
        old_pass_txt_fld.layer.borderColor = UIColor.red.cgColor
        old_pass_txt_fld.layer.cornerRadius = 5
        
        new_pass_txt_fld.layer.borderWidth = 0.5
        new_pass_txt_fld.layer.borderColor = UIColor.red.cgColor
        new_pass_txt_fld.layer.cornerRadius = 5
        
        re_enter_pass_txt_fld.layer.borderWidth = 0.5
        re_enter_pass_txt_fld.layer.borderColor = UIColor.red.cgColor
        re_enter_pass_txt_fld.layer.cornerRadius = 5
        
        bg.layer.cornerRadius = 15.0
        bg.layer.shadowColor = UIColor.black.cgColor
        bg.layer.shadowOpacity = 0.2
        bg.layer.shadowOffset = CGSize(width: -1, height: 1)
        bg.layer.shadowRadius = 3
        
        
        addPaddingAndBorder(to: old_pass_txt_fld)
        addPaddingAndBorder(to: new_pass_txt_fld)
        addPaddingAndBorder(to: re_enter_pass_txt_fld)
        
        // Do any additional setup after loading the view.
    }
    
    func addPaddingAndBorder(to textfield: UITextField) {
        
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 15.0, height: 2.0))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    
    
    
    @IBAction func back_click(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        
    }
    @IBAction func continue_click(_ sender: Any) {
        
        if old_pass_txt_fld.text != "" && new_pass_txt_fld.text != "" && re_enter_pass_txt_fld.text != ""{
            
            if re_enter_pass_txt_fld.text == new_pass_txt_fld.text{
                
                let md5Data1 = MD5(string: self.old_pass_txt_fld.text!)
                let md5Data2 = MD5(string: self.new_pass_txt_fld.text!)
                let md5Data3 = MD5(string: self.re_enter_pass_txt_fld.text!)
                
                let  md5_pass1 =  md5Data1.map { String(format: "%02hhx", $0) }.joined()
                let  md5_pass2 =  md5Data2.map { String(format: "%02hhx", $0) }.joined()
                let  md5_pass3 =  md5Data3.map { String(format: "%02hhx", $0) }.joined()
                
                
                // spinner.startAnimating()
                authentication_services.instance.change_new_pass(cc_pass: md5_pass1, new_pass: md5_pass2,con_pass: md5_pass3){ (success) in
                    // self.spinner.stopAnimating()
                    if success {
                        //self.spinner2.stopAnimating()
                        if userdata_services.instance.change_new_pass_DATA["error"]  == nil{
                            
                            
                            if userdata_services.instance.change_new_pass_DATA["status"]  as! String == "ok"{
                                
                                print(userdata_services.instance.change_new_pass_DATA)
                                
                                let data1  = userdata_services.instance.change_new_pass_DATA
                                print()
                                print (data1)
                                
                                authentication_services.instance.token = data1["token"]  as! String
                                
                                BEARER_HEADER = [
                                    "Authorization":"Bearer \(authentication_services.instance.token)",
                                    "Content-Type": "application/json; charset=utf-8"
                                ]
                                BEARER_2HEADER = [
                                    "Authorization":"Bearer \(authentication_services.instance.token)",
                                    "Content-Type": "application/json; charset=utf-8"
                                ]
                                
                                
                                
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.change_new_pass_DATA["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    switch action.style{
                                    case .default:
                                        print("default")
                                        
                                        
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "acc_edit")
                                        
                                        self.present(newViewController, animated: false, completion: nil)
                                        
                                    case .cancel:
                                        print("cancel")
                                        
                                    case .destructive:
                                        print("destructive")
                                        
                                    }
                                }))
                                self.present(alert2, animated: true, completion: nil)
                                
                                
                                
                            }
                            else{
                                // self.spinner.stopAnimating()
                                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.change_new_pass_DATA["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert2, animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            //userdata_services.instance.logout()
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                            
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        
                    }
                        
                    else{
                        //self.spinner2.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }else{
                
                let alert2 = UIAlertController(title: "", message: "Password doesn't match.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
                
                
            }
            
        }else{
            
            let alert2 = UIAlertController(title: "", message: "Please fill all feilds", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
