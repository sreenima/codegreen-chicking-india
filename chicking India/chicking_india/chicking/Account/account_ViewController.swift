//
//  account_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 25/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlacePicker
import SWRevealViewController

class account_ViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var BG_view: UIView!
    
    var full_text = String()
    @IBOutlet weak var menu_btn: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var edit_btn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var address_height: NSLayoutConstraint!
    @IBOutlet weak var botton_height: NSLayoutConstraint!
    @IBOutlet var main_view: UIView!
    @IBOutlet weak var prifile_pic: UIImageView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var phone_txt: UITextField!
    @IBOutlet weak var email_txt: UITextField!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var acc_btn: UIButton!
    @IBOutlet weak var scroll: UIScrollView!
    var addr_pos = NSString()
    var secound_data = Dictionary<String,Any>()
    var all_address: NSArray = []
    var a = 0
    
    var no_ad = NSString()
    var addr = NSString()
    var idd = NSInteger()
    var check = String()
    let cel = account_Address_TableViewCell()
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if  authentication_services.instance.isloggedin == false {
            let aler = UIAlertController(title: "", message: "You need to LogIn for this action !", preferredStyle: UIAlertControllerStyle.alert)
            
            aler.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            aler.addAction(UIAlertAction(title: "LogIn", style: .default, handler: { action in
                switch action.style{
                case .default:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                case .cancel:
                    
                    print("cancel")
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(aler, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        BG_view.layer.cornerRadius = 12
        
        
        BG_view.layer.borderWidth = 1
        BG_view.layer.borderColor = UIColor.clear.cgColor
        BG_view.layer.masksToBounds = true
        
        BG_view.layer.shadowColor = UIColor.black.cgColor
        BG_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        BG_view.layer.shadowRadius = 3.0
        BG_view.layer.shadowOpacity = 0.2
        BG_view.layer.masksToBounds = false
        BG_view.layer.backgroundColor = UIColor.white.cgColor
        
        
        phone_txt.delegate = self
        acc_btn.contentHorizontalAlignment = .fill
        acc_btn.contentVerticalAlignment = .fill
        acc_btn.imageView?.contentMode = .scaleAspectFit
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        if self.revealViewController() != nil {
            menu_btn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
            
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            let swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
            
        }
        
        
        
        table_view.rowHeight = UITableViewAutomaticDimension
        save_btn.isHidden = true
        
        
        
        prifile_pic.layer.cornerRadius =  prifile_pic.frame.size.width / 2
        prifile_pic.clipsToBounds = true
        
        phone_txt.delegate = self 
        email_txt.delegate = self
        
        
        
        if  authentication_services.instance.isloggedin != false {
            spinner.startAnimating()
            authentication_services.instance.user_details(){ (success) in
                self.spinner.stopAnimating()
                if success {
                    //self.spinner2.stopAnimating()
                    if userdata_services.instance.user_details_data["error"]  == nil{
                        
                        
                        if userdata_services.instance.user_details_data["status"]  as! String == "ok"{
                            
                            let data1 : NSArray = userdata_services.instance.user_details_data["data"] as! NSArray
                            print(userdata_services.instance.user_details_data["data"])
                            print (data1)
                            
                            let firstdata = data1[0] as! Dictionary <String,Any>
                            
                            
                            self.phone_txt.text = "0\(String(describing: firstdata["mobileno"] as! String))"
                            
                            self.secound_data = firstdata["userprofile"] as! Dictionary<String,Any>
                            print(self.secound_data)
                            // let thrd_data = secound_data[0] as! Dictionary <String,Any>
                            self.idd = self.secound_data["id"] as! NSInteger
                            self.name_lbl.text =  self.secound_data["name"] as? String
                            self.email_txt.text = self.secound_data["email"] as? String
                            
                            self.all_address = firstdata["user_address"] as! NSArray
                            self.table_view.reloadData()
                            
                            print("your Issue is here\(self.all_address)")
                            if self.all_address.count != 0{
                                if self.all_address.count < 2 {
                                    self.address_height.constant = 140
                                }
                                else{
                                    self.address_height.constant = 250
                                }
                            }
                            else{
                                self.address_height.constant = 80
                                
                            }
                        }
                        else{
                            // self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.user_details_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        //userdata_services.instance.logout()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                        
                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    
                }
                    
                else{
                    //self.spinner2.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                    
                }
                
            }
            
            
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if all_address.count != 0{
            if all_address.count < 3
            {
                return all_address.count+1
            }
            else{
                return 3
            }
        }
        else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! account_Address_TableViewCell
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell2") as! static_cellTableViewCell
        
        if all_address.count != 0{
            if indexPath.row != 2 && indexPath.row < 2{
                
                if all_address.count<3 && indexPath.row > all_address.count-1{
                    print("any")
                }
                else{
                    
                    var dat = all_address[indexPath.row] as! Dictionary<String,Any>
                    
                    
                    
                    var type = dat["type"] as! String
                    var add = dat["street"] as! String
                    
                    if type == "home"{
                        
                        cell.img_vw.image = #imageLiteral(resourceName: "home (1) (1)")
                        
                    }
                    else if type == "work"{
                        
                        cell.img_vw.image = #imageLiteral(resourceName: "suitcase")
                    }
                    else if type == "other"{
                        
                        cell.img_vw.image = #imageLiteral(resourceName: "placeholder (1)-2")
                    }
                    else{
                        
                        cell.img_vw.image = #imageLiteral(resourceName: "placeholder (1)-3")
                        
                    }
                    if type == "home"{
                        
                        
                        cell.add_type_lbl.text = "HOME"
                        
                    }
                    else if type == "work"{
                        cell.add_type_lbl.text = "WORK"
                        
                    }
                    else{
                        
                        cell.add_type_lbl.text = "OTHER"
                    }
                    
                    print(dat)
                    
                    var tyype = dat["type"] as! String
                    var aadd = dat["street"] as! String
                    var flat = dat["flat_name"] as! String
                    var land = dat["landmark"] as! String
                    var building = dat["building"] as! String
                    
                    if building != ""{
                        
                        self.full_text = "\(building),"
                        cell.address_lbl.text = self.full_text
                    }
                    if add != ""{
                        
                        self.full_text = "\(self.full_text)\(add),"
                        cell.address_lbl.text = self.full_text
                    }
                    if flat != ""{
                        
                        self.full_text =  "\(self.full_text)\(flat),"
                        cell.address_lbl.text = self.full_text
                    }
                    if land != ""{
                        
                        self.full_text = "\(self.full_text)\(land)"
                        cell.address_lbl.text = self.full_text
                    }
                        
                    else{
                        var add2 = dat["name"] as! String
                        if add2 != ""{
                            cell.address_lbl.text = add2
                        }
                    }
                    
                    
                }
                
            }
            
            if indexPath.row == 2 {
                
                
                return cell1
            }
            else
            {
                if self.all_address.count < 2 && indexPath.row == 1{
                    
                    return cell1
                }
                else{
                    
                    return cell
                }
                
            }
            
        }else{
            cell1.view_all_address.text = "Add Address!"
            
            
            return cell1
        }
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        address_height.constant = self.table_view .contentSize.height
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row > 5{
            print("selected")
            
            var dat = all_address[indexPath.row] as! Dictionary<String,Any>
            print(dat)
            var che = dat["id"]
            self.check = "\(che!)"
            
            
            let config = GMSPlacePickerConfig(viewport: nil)
            let placePicker = GMSPlacePickerViewController(config: config)
            
            placePicker.delegate = self as! GMSPlacePickerViewControllerDelegate
            present(placePicker, animated: true, completion: nil)
        }
        if self.all_address.count < 2 && indexPath.row == 1{
            let newViewController1 = storyboard?.instantiateViewController(withIdentifier: "svd_adr") as! saved_address_ViewController
            newViewController1.on_my_way_from_account = "yes"
            self.present(newViewController1, animated: false, completion: nil)
        }
        if indexPath.row == 2 {
            let newViewController1 = storyboard?.instantiateViewController(withIdentifier: "svd_adr") as! saved_address_ViewController
            newViewController1.on_my_way_from_account = "yes"
            self.present(newViewController1, animated: false, completion: nil)
        }
        if self.all_address.count < 1 {
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "search") as! search_area_ViewController
            
            newViewController.addr_pos = self.addr_pos
            newViewController.addr = self.addr
            newViewController.no_ad = self.no_ad
            newViewController.from_edit = "no"
            // userdata_services.instance.no_address = "yes"
            
            
            self.present(newViewController, animated: false, completion: nil)
            
        }
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        
        print(location)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phone_txt{
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "change_phone") as! change_phone_ViewController
            
            
            
            self.present(newViewController, animated: false, completion: nil)
            
        }
    }
   
    @IBAction func edit_click(_ sender: Any) {
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "acc_edit")
        
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    @IBAction func save_click(_ sender: Any) {
        
        
        
        
        authentication_services.instance.user_profile_update(name: self.name_lbl.text!, email: self.email_txt.text!, dob: "2017-12-12", loyality: "102030540", id: "\(self.idd)"  ) { (success) in
            self.spinner.stopAnimating()
            if success {
                //self.spinner2.stopAnimating()
                if userdata_services.instance.user_profile_update["error"]  == nil{
                    
                    
                    if userdata_services.instance.user_profile_update["status"]  as! String == "ok"{
                        
                        self.save_btn.isHidden = true
                        self.edit_btn.isHidden = false
                        self.email_txt.isUserInteractionEnabled = false
                        self.phone_txt.isUserInteractionEnabled = false
                        self.menu_btn.isHidden = false
                        self.back_btn.isHidden = true
                        self.viewDidLoad()
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.user_profile_update["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    print(userdata_services.instance.user_profile_update["error"]!)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
                
            else{
                //self.spinner2.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
      
    }
    
    
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    
    
    @IBAction func offer_Click(_ sender: Any) {
        
        
       let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
        
    }
    
    
    
    
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    @IBAction func back_click(_ sender: Any) {
        
        
        email_txt.isUserInteractionEnabled = false
        
        phone_txt.isUserInteractionEnabled = false
        back_btn.isHidden = true
        menu_btn.isHidden = false
        edit_btn.isHidden = false
        self.viewDidLoad()
        // dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension account_ViewController : GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        
        
        
        
        
        
        viewController.dismiss(animated: true, completion: nil)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            addr = place.formattedAddress! as NSString
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        newViewController.check = "yes"
        newViewController.id_check = self.check
        
        if no_ad == "yes" {
            newViewController.no_add = no_ad
            newViewController.test = addr_pos
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            
        }
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        //dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
}
