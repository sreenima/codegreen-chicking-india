//
//  edit_otp_verification_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 10/05/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class edit_otp_verification_ViewController: UIViewController {
    
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var OTP1_txtfld: UITextField!
    @IBOutlet weak var OTP2_txtfld: UITextField!
    @IBOutlet weak var OTP3_txtfld: UITextField!
    @IBOutlet weak var OTP4_txtfld: UITextField!
    @IBOutlet weak var Done_btn: UIButton!
    @IBOutlet weak var resend_btn: UIButton!
    
    
    var phone_number = String()
    var Country_code = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        OTP1_txtfld.delegate = self as? UITextFieldDelegate
        OTP2_txtfld.delegate = self as? UITextFieldDelegate
        OTP3_txtfld.delegate = self as? UITextFieldDelegate
        OTP4_txtfld.delegate = self as? UITextFieldDelegate
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.OTP1_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP2_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP3_txtfld.keyboardType = UIKeyboardType.decimalPad
        self.OTP4_txtfld.keyboardType = UIKeyboardType.decimalPad
        
        
        OTP1_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP2_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP3_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        OTP4_txtfld.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case OTP1_txtfld:
                OTP2_txtfld.becomeFirstResponder()
            case OTP2_txtfld:
                OTP3_txtfld.becomeFirstResponder()
            case OTP3_txtfld:
                OTP4_txtfld.becomeFirstResponder()
            case OTP4_txtfld:
                OTP4_txtfld.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case OTP1_txtfld:
                OTP1_txtfld.becomeFirstResponder()
            case OTP2_txtfld:
                OTP1_txtfld.becomeFirstResponder()
            case OTP3_txtfld:
                OTP2_txtfld.becomeFirstResponder()
            case OTP4_txtfld:
                OTP3_txtfld.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    @IBAction func back_click(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func done_click(_ sender: Any) {
        
        if OTP1_txtfld.text != "" && OTP2_txtfld.text != "" && OTP3_txtfld.text != "" && OTP4_txtfld.text != ""{
            
            var ottp = "\(OTP1_txtfld.text!)\(OTP2_txtfld.text!)\(OTP3_txtfld.text!)\(OTP4_txtfld.text!)"
            
            authentication_services.instance.number_change(otp: ottp) { (success) in
                if success {
                    if userdata_services.instance.mobile_change_data["error"] == nil{
                        if userdata_services.instance.mobile_change_data["status"] as! String == "ok"{
                            //self.spinner.stopAnimating()
                            print(userdata_services.instance.mobile_change_data)
                            
                            
                            
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal4")
                            
                            
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                            
                        }
                        else{
                            //self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.new_OTP_DATE["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                            
                        }
                    }
                    else{
                        userdata_services.instance.logout()
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                        
                        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                        
                        self.present(newViewController, animated: false, completion: nil)
                        
                        
                    }
                    
                    
                    
                }
                else{
                    //self.spinner.stopAnimating()
                    let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert2, animated: true, completion: nil)
                }
                
            }
            
        }else{
            
            let alert2 = UIAlertController(title: "", message: "Invalid OTP", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func resend_click(_ sender: Any) {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        Done_btn.layer.add(animation, forKey: "shake")
        
        
        OTP1_txtfld.text = nil
        OTP2_txtfld.text = nil
        OTP3_txtfld.text = nil
        OTP4_txtfld.text = nil
        
        
        
        
        
        authentication_services.instance.new_otp_call(mobille: phone_number, country: Country_code) { (success) in
            if success {
                if userdata_services.instance.new_OTP_DATE["error"] == nil{
                    if userdata_services.instance.new_OTP_DATE["status"] as! String == "ok"{
                        //self.spinner.stopAnimating()
                        var datt = userdata_services.instance.new_OTP_DATE["data"] as! NSArray
                        
                        print(datt)
                        
                        
                    }
                    else{
                        //self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.new_OTP_DATE["message"] as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                    }
                }
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal1")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
                
            }
            else{
                //self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
