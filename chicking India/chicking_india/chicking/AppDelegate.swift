//
//  AppDelegate.swift
//  chicking
//
//  Created by Ios CGT on 08/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import CoreData
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import MapKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate  {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    let gcmMessageIDKey = "gcm.message_id"
    var notification_status = false
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        

        
        
        if #available(iOS 10.0, *) {
            //  For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
        }
        
       
        
       
        application.registerForRemoteNotifications()
        UIApplication.shared.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
       
        
        // Override point for customization after application launch.
        let userdefaults = UserDefaults.standard
        
//        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        
        
        if let savedValue = userdefaults.value(forKey: ISLOGGEDIN_KEY){
            print("Here you will get saved value")
            if authentication_services.instance.isloggedin == true {
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "reveal1")
                
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
                
                authentication_services.instance.check_for_update(){ (success) in
                    if success {
                        
                        if userdata_services.instance.check_update_data["error"]  == nil{
                            
                            if userdata_services.instance.check_update_data["status"]  as! String == "ok"{
                                
                                print(userdata_services.instance.check_update_data)
                                
                                let prior = userdata_services.instance.check_update_data["data"] as! Dictionary<String,Any>
                                
                                let priority = prior["priority"] as! String
                                
                                let versio = prior["appVersion"] as! String
                                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                                
                                if versio ==  appVersion{
                                    
                                    print("Up To DATE")
                                    
                                }else{
                                    
                                    if priority != "minor"{
                                        
                                        var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                        
                                        topWindow?.rootViewController = UIViewController()
                                        topWindow?.windowLevel = UIWindowLevelAlert + 1
                                        let alert: UIAlertController =  UIAlertController(title: "\nUpdate Chicking ?", message: "\n\n\n\n\n\nChicking recommends that you update to latest version. ", preferredStyle: .alert)
                                        
                                        var height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
                                        alert.view.addConstraint(height)
                                        
                                        alert.view.tintColor = UIColor.red
                                        
                                        var imageView = UIImageView(frame: CGRect(x: 85, y: 45, width: 100, height: 100))
                                        imageView.image = #imageLiteral(resourceName: "chicken")
                                        
                                        alert.view.addSubview(imageView)
                                        
                                        alert.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (alertAction) in
                                            topWindow?.isHidden = true
                                            
                                            if let url = URL(string: "https://itunes.apple.com/in/app/smsdaddy/id1451570624?ls=1&mt=8") {
                                                if #available(iOS 10.0, *) {
                                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                } else {
                                                    // Earlier versions
                                                    if UIApplication.shared.canOpenURL(url as URL) {
                                                        UIApplication.shared.openURL(url as URL)
                                                    }
                                                }
                                            }
                                            
                                            topWindow = nil
                                        }))
                                        
                                        alert.addAction(UIAlertAction.init(title: "Exit", style: .default, handler: { (alertAction) in
                                            topWindow?.isHidden = true
                                            
                                            print("Updation Skipped")
                                            UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                                                   to: UIApplication.shared, for: nil)
                                            topWindow = nil
                                            
                                            
                                        }))
                                        
                                        topWindow?.makeKeyAndVisible()
                                        topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                                        
                                        
                                    }else{
                                        
                                        print("No major updates.")
                                        
                                    }
                                    
                                }
                            }
                            else{
                                
                                var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                
                                topWindow?.rootViewController = UIViewController()
                                topWindow?.windowLevel = UIWindowLevelAlert + 1
                                let alert: UIAlertController =  UIAlertController(title: "", message: "\(userdata_services.instance.check_update_data["message"]  as! String)", preferredStyle: .alert)
                                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                                    topWindow?.isHidden = true
                                    topWindow = nil
                                }))
                                
                                topWindow?.makeKeyAndVisible()
                                topWindow?.rootViewController?.present(alert, animated: true, completion:nil)                            }
                            
                        }
                        else{
                            //userdata_services.instance.logout()
                            self.window = UIWindow(frame: UIScreen.main.bounds)
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                            
                            self.window?.rootViewController = initialViewController
                            self.window?.makeKeyAndVisible()
                            
                            
                        }
                        
                    }
                        
                    else{
                        var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                        
                        topWindow?.rootViewController = UIViewController()
                        topWindow?.windowLevel = UIWindowLevelAlert + 1
                        let alert: UIAlertController =  UIAlertController(title: "", message: "Network Problem", preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                            topWindow?.isHidden = true
                            topWindow = nil
                        }))
                        
                        topWindow?.makeKeyAndVisible()
                        topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                        
                    }
                    
                }
                
                // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
                
          
                
                return true
                
            }else{
                
                // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
                
                authentication_services.instance.check_for_update(){ (success) in
                    if success {
                        
                        if userdata_services.instance.check_update_data["error"]  == nil{
                            
                            
                            if userdata_services.instance.check_update_data["status"]  as! String == "ok"{
                                
                                
                                
                                print(userdata_services.instance.check_update_data)
                                
                                let prior = userdata_services.instance.check_update_data["data"] as! Dictionary<String,Any>
                                
                                let priority = prior["priority"] as! String
                                
                                let versio = prior["appVersion"] as! String
                                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                                
                                if versio ==  appVersion{
                                    
                                 //   authentication_services.instance.isloggedin = false
                                    print("No value in Userdefault,Either you can save value here or perform other operation")
                                    
                                }else{
                                    
                                    if priority != "minor"{
                                        
                                        var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                        
                                        topWindow?.rootViewController = UIViewController()
                                        topWindow?.windowLevel = UIWindowLevelAlert + 1
                                        let alert: UIAlertController =  UIAlertController(title: "\nUpdate Chicking ?", message: "\n\n\n\n\n\nChicking recommends that you update to latest version. ", preferredStyle: .alert)
                                        
                                        var height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
                                        alert.view.addConstraint(height)
                                        alert.view.tintColor = UIColor.red
                                        
                                        var imageView = UIImageView(frame: CGRect(x: 85, y: 45, width: 100, height: 100))
                                        imageView.image = #imageLiteral(resourceName: "chicken")
                                        
                                        alert.view.addSubview(imageView)
                                        alert.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (alertAction) in
                                            topWindow?.isHidden = true
                                            
                                            if let url = URL(string: "https://itunes.apple.com/in/app/smsdaddy/id1451570624?ls=1&mt=8") {
                                                if #available(iOS 10.0, *) {
                                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                } else {
                                                    // Earlier versions
                                                    if UIApplication.shared.canOpenURL(url as URL) {
                                                        UIApplication.shared.openURL(url as URL)
                                                    }
                                                }
                                            }
                                            
                                            topWindow = nil
                                        }))
                                        alert.addAction(UIAlertAction.init(title: "Exit", style: .default, handler: { (alertAction) in
                                            topWindow?.isHidden = true
                                            
                                            authentication_services.instance.isloggedin = false
                                            print("No value in Userdefault,Either you can save value here or perform other operation")
                                            
                                            
                                            UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                                                   to: UIApplication.shared, for: nil)
                                            topWindow = nil
                                          
                                        }))
                                        
                                        topWindow?.makeKeyAndVisible()
                                        topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                                        
                                        
                                    }else{
                                        
                                      //  authentication_services.instance.isloggedin = false
                                        print("No value in Userdefault,Either you can save value here or perform other operation")
                                        
                                    }
                                    
                                }
                            }
                            else{
                                
                                
                                var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                
                                topWindow?.rootViewController = UIViewController()
                                topWindow?.windowLevel = UIWindowLevelAlert + 1
                                let alert: UIAlertController =  UIAlertController(title: "", message: "\(userdata_services.instance.check_update_data["message"]  as! String)", preferredStyle: .alert)
                                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                                    topWindow?.isHidden = true
                                    topWindow = nil
                                }))
                                
                                topWindow?.makeKeyAndVisible()
                                topWindow?.rootViewController?.present(alert, animated: true, completion:nil)                            }
                            
                        }
                        else{
                            //userdata_services.instance.logout()
                            self.window = UIWindow(frame: UIScreen.main.bounds)
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                            authentication_services.instance.isloggedin = false
                            print("No value in Userdefault,Either you can save value here or perform other operation")
                            
                            self.window?.rootViewController = initialViewController
                            self.window?.makeKeyAndVisible()
                            
                            
                        }
                        
                    }
                        
                    else{
                        var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                        
                        topWindow?.rootViewController = UIViewController()
                        topWindow?.windowLevel = UIWindowLevelAlert + 1
                        let alert: UIAlertController =  UIAlertController(title: "", message: "Network Problem", preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                            topWindow?.isHidden = true
                            topWindow = nil
                        }))
                        authentication_services.instance.isloggedin = false
                        print("No value in Userdefault,Either you can save value here or perform other operation")
                        
                        topWindow?.makeKeyAndVisible()
                        topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                        
                    }
                    
                }
                
                
                
                
                // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
                
                return true
                
                
            }
        } else {
            
            
            // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
            
            
            
            authentication_services.instance.check_for_update(){ (success) in
                if success {
                    
                    if userdata_services.instance.check_update_data["error"]  == nil{
                        
                        
                        if userdata_services.instance.check_update_data["status"]  as! String == "ok"{
                            
                            
                            
                            print(userdata_services.instance.check_update_data)
                            
                            let prior = userdata_services.instance.check_update_data["data"] as! Dictionary<String,Any>
                            
                            let priority = prior["priority"] as! String
                            
                            let versio = prior["appVersion"] as! String
                            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                            
                            if versio ==  appVersion{
                                
                               // authentication_services.instance.isloggedin = false
                                print("No value in Userdefault,Either you can save value here or perform other operation")
                                
                            }else{
                                
                                if priority != "minor"{
                                    
                                    var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                    
                                    topWindow?.rootViewController = UIViewController()
                                    topWindow?.windowLevel = UIWindowLevelAlert + 1
                                    let alert: UIAlertController =  UIAlertController(title: "\nUpdate Chicking ?", message: "\n\n\n\n\n\nChicking recommends that you update to latest version. ", preferredStyle: .alert)
                                    
                                    
                                    var height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
                                    alert.view.addConstraint(height)
                                    alert.view.tintColor = UIColor.red
                                    
                                    var imageView = UIImageView(frame: CGRect(x: 85, y: 45, width: 100, height: 100))
                                    imageView.image = #imageLiteral(resourceName: "chicken")
                                    
                                    alert.view.addSubview(imageView)
                                    alert.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (alertAction) in
                                        topWindow?.isHidden = true
                                        
                                        if let url = URL(string: "https://itunes.apple.com/in/app/smsdaddy/id1451570624?ls=1&mt=8") {
                                            if #available(iOS 10.0, *) {
                                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                            } else {
                                                // Earlier versions
                                                if UIApplication.shared.canOpenURL(url as URL) {
                                                    UIApplication.shared.openURL(url as URL)
                                                }
                                            }
                                        }
                                        
                                        topWindow = nil
                                    }))
                                    alert.addAction(UIAlertAction.init(title: "Exit", style: .default, handler: { (alertAction) in
                                        topWindow?.isHidden = true
                                        
                                        authentication_services.instance.isloggedin = false
                                        print("No value in Userdefault,Either you can save value here or perform other operation")
                                        UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                                               to: UIApplication.shared, for: nil)
                                        
                                        topWindow = nil
                                        
                                        
                                    }))
                                    
                                    topWindow?.makeKeyAndVisible()
                                    topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                                    
                                    
                                }else{
                                    
                                   // authentication_services.instance.isloggedin = false
                                    print("No value in Userdefault,Either you can save value here or perform other operation")
                                    
                                }
                                
                            }
                        }
                        else{
                            
                            
                            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                            
                            topWindow?.rootViewController = UIViewController()
                            topWindow?.windowLevel = UIWindowLevelAlert + 1
                            let alert: UIAlertController =  UIAlertController(title: "", message: "\(userdata_services.instance.check_update_data["message"]  as! String)", preferredStyle: .alert)
                            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                                topWindow?.isHidden = true
                                topWindow = nil
                            }))
                            
                            topWindow?.makeKeyAndVisible()
                            topWindow?.rootViewController?.present(alert, animated: true, completion:nil)                            }
                        
                    }
                    else{
                        //userdata_services.instance.logout()
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                        authentication_services.instance.isloggedin = false
                        print("No value in Userdefault,Either you can save value here or perform other operation")
                        
                        self.window?.rootViewController = initialViewController
                        self.window?.makeKeyAndVisible()
                        
                        
                    }
                    
                }
                    
                else{
                    var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                    
                    topWindow?.rootViewController = UIViewController()
                    topWindow?.windowLevel = UIWindowLevelAlert + 1
                    let alert: UIAlertController =  UIAlertController(title: "", message: "Network Problem", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                        topWindow?.isHidden = true
                        topWindow = nil
                    }))
                    authentication_services.instance.isloggedin = false
                    print("No value in Userdefault,Either you can save value here or perform other operation")
                    
                    topWindow?.makeKeyAndVisible()
                    topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                    
                }
                
            }
            
            // CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ??? CHECKING FOR UPDATE ???
            
            
        }
      
        
        GMSPlacesClient.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        GMSServices.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        
//        GMSPlacesClient.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
//        GMSServices.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        
        
       //AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ
        
        GMSPlacesClient.openSourceLicenseInfo()
        GMSServices.openSourceLicenseInfo()
        
        return true
        
        
        
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        
        if userdata_services.instance.cart.count > 0{
            authentication_services.instance.cart_session = userdata_services.instance.cart as NSArray }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        GMSPlacesClient.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        GMSServices.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        
//        GMSPlacesClient.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
//        GMSServices.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        
        
        if   UserDefaults.standard.object(forKey: CART_MAIN_KEY) != nil{
            
            userdata_services.instance.cart =  (authentication_services.instance.cart_session as NSArray).mutableCopy() as! NSMutableArray
            
        }
        
        
        
        authentication_services.instance.check_for_update(){ (success) in
            if success {
                
                if userdata_services.instance.check_update_data["error"]  == nil{
                    
                    
                    if userdata_services.instance.check_update_data["status"]  as! String == "ok"{
                        
                        
                        
                        print(userdata_services.instance.check_update_data)
                        
                        let prior = userdata_services.instance.check_update_data["data"] as! Dictionary<String,Any>
                        
                        let priority = prior["priority"] as! String
                        
                        let versio = prior["appVersion"] as! String
                        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                        
                        if versio ==  appVersion{
                            
                           // authentication_services.instance.isloggedin = false
                            print("No value in Userdefault,Either you can save value here or perform other operation")
                            
                        }else{
                            
                            if priority != "minor"{
                                
                                var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                                
                                topWindow?.rootViewController = UIViewController()
                                topWindow?.windowLevel = UIWindowLevelAlert + 1
                                let alert: UIAlertController =  UIAlertController(title: "\nUpdate Chicking ?", message: "\n\n\n\n\n\nChicking recommends that you update to latest version. ", preferredStyle: .alert)
                                
                                var height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
                                alert.view.addConstraint(height)
                                alert.view.tintColor = UIColor.red
                                
                                var imageView = UIImageView(frame: CGRect(x: 85, y: 45, width: 100, height: 100))
                                imageView.image = #imageLiteral(resourceName: "chicken")
                                
                                alert.view.addSubview(imageView)
                                alert.addAction(UIAlertAction.init(title: "Update", style: .default, handler: { (alertAction) in
                                    topWindow?.isHidden = true
                                    
                                    if let url = URL(string: "https://itunes.apple.com/in/app/smsdaddy/id1451570624?ls=1&mt=8") {
                                        if #available(iOS 10.0, *) {
                                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                        } else {
                                            // Earlier versions
                                            if UIApplication.shared.canOpenURL(url as URL) {
                                                UIApplication.shared.openURL(url as URL)
                                            }
                                        }
                                    }
                                    
                                    topWindow = nil
                                }))
                                alert.addAction(UIAlertAction.init(title: "Exit", style: .default, handler: { (alertAction) in
                                    topWindow?.isHidden = true
                                    
                                    authentication_services.instance.isloggedin = false
                                    print("No value in Userdefault,Either you can save value here or perform other operation")
                                    
                                    
                                    UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                                           to: UIApplication.shared, for: nil)
                                    topWindow = nil
                                    
                                }))
                                
                                topWindow?.makeKeyAndVisible()
                                topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                                
                                
                            }else{
                                
                               // authentication_services.instance.isloggedin = false
                                print("No value in Userdefault,Either you can save value here or perform other operation")
                                
                            }
                            
                        }
                    }
                    else{
                        
                        
                        var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                        
                        topWindow?.rootViewController = UIViewController()
                        topWindow?.windowLevel = UIWindowLevelAlert + 1
                        let alert: UIAlertController =  UIAlertController(title: "", message: "\(userdata_services.instance.check_update_data["message"]  as! String)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                            topWindow?.isHidden = true
                            topWindow = nil
                        }))
                        
                        topWindow?.makeKeyAndVisible()
                        topWindow?.rootViewController?.present(alert, animated: true, completion:nil)                            }
                    
                }
                else{
                    //userdata_services.instance.logout()
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                    authentication_services.instance.isloggedin = false
                    print("No value in Userdefault,Either you can save value here or perform other operation")
                    
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                    
                    
                }
                
            }
                
            else{
                var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
                
                topWindow?.rootViewController = UIViewController()
                topWindow?.windowLevel = UIWindowLevelAlert + 1
                let alert: UIAlertController =  UIAlertController(title: "", message: "Network Problem", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (alertAction) in
                    topWindow?.isHidden = true
                    topWindow = nil
                }))
               // authentication_services.instance.isloggedin = false
                print("No value in Userdefault,Either you can save value here or perform other operation")
                
                topWindow?.makeKeyAndVisible()
                topWindow?.rootViewController?.present(alert, animated: true, completion:nil)
                
            }
            
            
            
            
            
        }
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        GMSPlacesClient.provideAPIKey("AIzaSyBh7PQUfMGg1a_bX6NP63JZtRQPU8ZJGd4")
//        GMSServices.provideAPIKey("AIzaSyBh7PQUfMGg1a_bX6NP63JZtRQPU8ZJGd4")
        
        GMSPlacesClient.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
        GMSServices.provideAPIKey("AIzaSyB8WiUPvKOiP7y4NXg5L4DVHcawew5zLbQ")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "chicking")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
            
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func registerForPushNotifications(application: UIApplication) {
        
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        Messaging.messaging().delegate = self
    }
    
  
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
}

//@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        guard let userInfo = notification.request.content.userInfo as? [String: Any] else {return}
        print("userInfo = \(userInfo)")
        
        if let messageID = userInfo["id"] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("userInfo \(userInfo)")
        print("type is : \(String(describing: userInfo["type"]))")
        
        completionHandler([.alert, .badge, .sound])
        
       
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        
        
        if let messageID = userInfo["id"] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
        
        print("type is : \(String(describing: userInfo["type"]))")
        
        
        
        let userdefaults = UserDefaults.standard
        
        if let savedValue = userdefaults.value(forKey: ISLOGGEDIN_KEY){
            print("Here you will get saved value")
            
            
            
            if let type = userInfo["type"]! as? String{
                
                
                if authentication_services.instance.isloggedin == true {
                    
                    
                    
                    if type == "order"{
                        
                        
                        let dt = "\(userInfo["order_id"]!)"
                        
                        //    userdata_services.instance.ckeorfood = "cake"
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "odp") as! Order_details_page_ViewController
                        initialViewController.order_id = dt as! NSString
                        initialViewController.from_noti = "yes"
                        self.window?.rootViewController = initialViewController
                        self.window?.makeKeyAndVisible()
                        
                        
                    }
                    else if type == "product"{
                        
                        if let common_id = userInfo["common_id"] {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = storyboard.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
                            
                            initialViewController.cake_id = userInfo["common_id"] as! String
                            self.window?.rootViewController = initialViewController
                            self.window?.makeKeyAndVisible()
                        }
                        
                        
                        
                    }
                    else{
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "reveal2")
                        
                        self.window?.rootViewController = initialViewController
                        self.window?.makeKeyAndVisible()
                        
                    }
                    
                }else{
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                    
                }
            }
            else{
                
                //            userdata_services.instance.ckeorfood = "cake"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "reveal1")
                
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                
            }
            
            
        }else{
            
            
            
            if authentication_services.instance.isloggedin != true {
                //self.window = UIWindow(frame: UIScreen.main.bounds)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "log_in")
                
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                
                
            }
        }
        
        
        
        
        completionHandler()
    }
    
}



extension AppDelegate: MessagingDelegate{
    
   
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        userdata_services.instance.firebase_token = "\(fcmToken)"
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        print("dataDict = \(dataDict)")
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data

        print("deviceToken = \(deviceToken)")
    }
    

    
}



