//
//  cart_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 15/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class cart_ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var hide_view: UIView!
    @IBOutlet weak var text_available_lbl: UILabel!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var condition_time = TimeInterval()
    var check_date = TimeInterval()
    var condition_time2 = TimeInterval()
    
    var min_order = Float()
    
    var total_amount_to_block = Float()
    @IBOutlet weak var tbl1: UITableView!
    @IBOutlet weak var continue_btn: UIButton!
    var product_id = Array<Any>()
    @IBAction func back_clicked(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBOutlet weak var add_vw: UIView!
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return userdata_services.instance.cart.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    var imageCache2 = NSCache<NSString,UIImage>()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! cart_TableViewCell
        cell.removecart_btn.tag = indexPath.section
        cell.vview.layer.cornerRadius = 8
        cell.vview.layer.borderWidth = 1
        cell.vview.layer.borderColor = UIColor.clear.cgColor
        cell.vview.layer.masksToBounds = true
        
        cell.vview.layer.shadowColor = UIColor.black.cgColor
        cell.vview.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.vview.layer.shadowRadius = 3.0
        cell.vview.layer.shadowOpacity = 0.2
        cell.vview.layer.masksToBounds = false
        cell.vview.layer.backgroundColor = UIColor.white.cgColor
        
        
        cell.minus_btn.tag = indexPath.section
        cell.plus_btn.tag = indexPath.section
        
        
        var item = userdata_services.instance.cart[indexPath.section] as! Dictionary<String,Any>
        print(item)
        
        cell.name.text = "\(item["title"]!)"
        //        cell.price.text = "\(item["price"]!)"
        var floa = Float(item["price"] as! String)
        cell.price.text = (String(format: "%.2f", (floa as! CVarArg)))
        
        
        total_amount_to_block = total_amount_to_block + floa!
        
        cell.count_lbl.text = "\(item["quantity"]!)"
        var img_name = "\(item["image"]!)"
        
        
        if let cacheImage = imageCache2.object(forKey: img_name as NSString ){
            print("image cache")
            cell.img.image = cacheImage
            
            cell.img.clipsToBounds = true
            //return
        }
        else{
            print("image not cache")
            //self.spinner.startAnimating()
            print("\(IMAGE_URL)\(String(describing: img_name))")
            
            Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: img_name))").responseImage { response in
                
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.img.image = image
                    
                    cell.img.clipsToBounds = true
                    self.imageCache2.setObject(image, forKey: img_name as NSString)
                    //      }
                }
                
            }
            
        }
        
     
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var pro_id = product_id[indexPath.section]
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
        newViewController.cake_id = "\(pro_id)"
        self.present(newViewController, animated: false, completion: nil)
    }
    
    @IBAction func remove_cart(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "Do you really want to remove this item from the cart?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            userdata_services.instance.cart.removeObject(at: sender.tag)
            self.total_amount_to_block = Float()
            self.tbl1.reloadData()
            self.empty_cart()
        
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func decriment_action(_ sender: UIButton) {
        
        
        let cell = tbl1.cellForRow(at: [sender.tag,0]) as! cart_TableViewCell
        
        
        if cell.count_lbl.text != "1"{
            cell.count_lbl.text = "\(Int(cell.count_lbl.text!)!-1)"
        }
        else{
            
            // Create the alert controller
            let alertController = UIAlertController(title: "", message: "Do you really want to remove this item from the cart?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
                userdata_services.instance.cart.removeObject(at: sender.tag)
                self.tbl1.reloadData()
                self.empty_cart()
             
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
   
    }
    @IBOutlet var continue_shopping_btn: UIButton!
    
    
    @IBAction func continue_shopping(_ sender: Any) {
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal2")
        
        //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    func empty_cart()  {
        if userdata_services.instance.cart.count == 0 {
           
            
            // Create the alert controller
            let alertController = UIAlertController(title: "Cart empty!", message: "Please add items to the cart to continue", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "reveal2")
                
                //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                
                self.present(newViewController, animated: false, completion: nil)
            
            }
            
            
            // Add the actions
            alertController.addAction(okAction)
            
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
       
        }
    }
    
    @IBAction func incriment_action(_ sender: UIButton) {
        
        let cell = tbl1.cellForRow(at: [sender.tag,0]) as! cart_TableViewCell
        cell.count_lbl.text = "\(Int(cell.count_lbl.text!)!+1)"
        
        var itm = userdata_services.instance.cart[sender.tag] as! Dictionary<String,Any>
        
        
        
        itm["quantity"] = "\(Int("\(itm["quantity"]!)")!+1)"
        userdata_services.instance.cart[sender.tag] = itm
        
        
    }
    
    
    @IBAction func continue_action(_ sender: Any) {
        
        
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        
        
        spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            self.spinner.stopAnimating()
            if success {
                
                
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                   
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        
                        var t1 = dat["storeOpen"] as! String
                        var t2 = dat["storeClose"] as! String
                 
                        let da = DateFormatter()
                        
                        da.timeZone = TimeZone.current
                        
                        da.dateFormat = "hh:mm a"
                        
                        da.locale = Locale(identifier: "en-US")
                        
                        let dateInFormatt = da.date(from: t1)!
                        var pure_date1 = da.string(from:dateInFormatt as Date)
                        
                        let dateInFor = da.date(from: pure_date1)!
                        
                        self.condition_time = dateInFor.timeIntervalSince1970
                        //////////////////////////////////////////////////////////////
                        let da2 = DateFormatter()
                        da2.timeZone = TimeZone.current
                        da2.dateFormat = "hh:mm a"
                        
                        da2.locale = Locale(identifier: "en-US")
                        let dateInFormattt = da2.date(from: t2)!
                        var pure_date2 = da2.string(from:dateInFormattt as Date)
                        
                        let dateInForr = da2.date(from: pure_date2)!
                        
                        self.condition_time2 = dateInForr.timeIntervalSince1970
                        
                        let texx = "Next Available At \(t1 )"
                        self.text_available_lbl.text = texx
                        
                        print(dateInFormat)
                        let a:Int? = Int(dateInFormat)
                        
                        
                        
                        if self.min_order > self.total_amount_to_block {
                            
                            self.hide_view.isHidden = false
                            self.text_available_lbl.isHidden = false
                            self.text_available_lbl.text = "Minimum Amount Should be INR \(self.min_order)"
                            
                        }else{
                            
                            
                            
                            if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                                
                                self.hide_view.isHidden = true
                                self.text_available_lbl.isHidden = true
                                
                                if userdata_services.instance.cart.count == 0{
                                    
                                    
                                    // self.spinner.stopAnimating()
                                    let alert2 = UIAlertController(title: "", message: "Cart empty", preferredStyle: UIAlertControllerStyle.alert)
                                    alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert2, animated: true, completion: nil)
                                }
                                else{
                                    
                                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "d_method")
                                    
                                    userdata_services.instance.from_cart_store = "yes"
                                    
                                    self.present(newViewController, animated: false, completion: nil)
                                }
                                
                            }else{
                                self.hide_view.isHidden = false
                                self.text_available_lbl.isHidden = false
                                
                            }
                            
                        }
            
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
     
    }
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1") 
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    @IBAction func account_click(_ sender: Any) {
        
       /* let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
        userdata_services.instance.offer_check = "offer"
        // newViewController.contain = "offer"
        revealViewController()?.revealToggle(nil)
        self.present(newViewController, animated: false, completion: nil)

        
    }
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func favorites_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.empty_cart()
        viewDidLoad()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        continue_shopping_btn.layer.cornerRadius = 8
        
        
        
        continue_btn.isUserInteractionEnabled = false
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        
        let amFormatter = DateFormatter()
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        let amInFormat = amFormatter.string(from: NSDate() as Date)
        
        
        
        print( dateInFormat)
        print(amInFormat)
     
        if userdata_services.instance.cart.count == 0{
            
            
            // self.spinner.stopAnimating()
            let alert2 = UIAlertController(title: "", message: "Cart empty", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
        }else {
            var item3 = userdata_services.instance.cart
            
            for i in 0...item3.count-1{
                var item = userdata_services.instance.cart[i] as! Dictionary<String,Any>
                
                var id = item["product_id"] as! String
                product_id.insert(id, at: i)
            }
            
        }
        
        // spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            //  self.spinner.stopAnimating()
            if success {
                
                
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                      
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        
                        self.min_order =  Float(dat["orderMin"] as! String) as! Float
                        var t1 = dat["storeOpen"] as! String
                        var t2 = dat["storeClose"] as! String
                        
                        
                        
                        
                        let da = DateFormatter()
                        
                        da.dateFormat = "hh:mm a"
                        da.timeZone = TimeZone.current
                        da.locale = Locale(identifier: "en-US")
                        
                        if let dateInFormatt = da.date(from: t1){
                            var pure_date1 = da.string(from:dateInFormatt as Date)
                            
                            let dateInFor = da.date(from: pure_date1)!
                            
                            self.condition_time = dateInFor.timeIntervalSince1970
                            //////////////////////////////////////////////////////////////
                            let da2 = DateFormatter()
                            
                            da2.dateFormat = "hh:mm a"
                            
                            da2.timeZone = TimeZone.current
                            da2.locale = Locale(identifier: "en-US")
                            
                            let dateInFormattt = da2.date(from: t2)!
                            var pure_date2 = da2.string(from:dateInFormattt as Date)
                            
                            let dateInForr = da2.date(from: pure_date2)!
                            
                            self.condition_time2 = dateInForr.timeIntervalSince1970
                            
                            let texx = "Next Available At \(t1 )"
                            self.text_available_lbl.text = texx
                            
                            print(dateInFormat)
                            let a:Int? = Int(dateInFormat)
                            if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                                
                                self.hide_view.isHidden = true
                                self.text_available_lbl.isHidden = true
                                self.continue_btn.isUserInteractionEnabled = true
                                
                            }else{
                                self.hide_view.isHidden = false
                                self.text_available_lbl.isHidden = false
                                self.continue_btn.isUserInteractionEnabled = true
                            }
                            
                            
                            
                        }else{
                            
                            self.viewDidLoad()
                            
                        }
                     
                        
                    }
                    else{
                        // self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                   
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
                // self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
