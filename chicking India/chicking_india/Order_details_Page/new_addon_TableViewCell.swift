//
//  new_addon_TableViewCell.swift
//  chicking
//
//  Created by Ios CGT on 27/03/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class new_addon_TableViewCell: UITableViewCell {
    @IBOutlet weak var addon_name: UILabel!
    @IBOutlet weak var addon_price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
