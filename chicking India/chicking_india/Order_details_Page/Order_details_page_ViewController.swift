//
//  Order_details_page_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class Order_details_page_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    
    
    @IBOutlet weak var pomocode: UILabel!
    
    @IBOutlet weak var GST: UILabel!
    @IBOutlet weak var gstIncludeLbL: UILabel!
    @IBOutlet weak var discountAmtLbl: UILabel!
    @IBOutlet weak var discountAmount: UILabel!
    @IBOutlet weak var promoLbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var table_height: NSLayoutConstraint!
    @IBOutlet weak var phone_lbl: UILabel!
    @IBOutlet weak var address_lbl: UILabel!
    @IBOutlet weak var payment_mode_lbl: UILabel!
    @IBOutlet weak var sub_total_lbl: UILabel!
    
    @IBOutlet weak var tbl1: UITableView!
    
    @IBOutlet var address_title: UILabel!
    
    var headerCellSection:Int? //////////
    
    @IBOutlet weak var bg_view: UIView!
    @IBOutlet weak var order_id_lbl: UILabel!
    @IBOutlet weak var order_date_lbl: UILabel!
    @IBOutlet weak var delivery_charge_lbl: UILabel!
    
    
    var addon_array = Array<Any>()
    var new_add_arr1 = [[String: Any]]()
    var final_array = [Array<Any>]()
    
    @IBOutlet weak var order_delivery: UILabel!
    var addon = String()
    var addon1 = String ()
    var addon2 = String ()
    var addon3 = String()
    var products = NSArray()
    var order_id = NSString()
    var food_rating = NSInteger()
    var delivery_rating = NSInteger()
    var taste = String()
    var portion = String()
    var packing = String()
    var delivery_time = String()
    var food_handling = String()
    var display = String()
    var daaata1 = Dictionary<String,Any>()
    
    var from_noti = String()
    
    @IBOutlet weak var st1: UIButton!
    @IBOutlet weak var st2: UIButton!
    
    @IBOutlet weak var st3: UIButton!
    @IBOutlet weak var st5: UIButton!
    
    @IBOutlet weak var st4: UIButton!
    //#################################################
    
    @IBOutlet weak var to_hide: UIView!
    @IBOutlet weak var what_didi_you_lbl: UILabel!
    
    @IBOutlet weak var keyboard_height: NSLayoutConstraint!
    
    
    @IBOutlet weak var popup_vw1: UIView!
    @IBOutlet weak var popup_vw2: UIView!
    @IBOutlet weak var rate_your_food_lbl: UILabel!
    @IBOutlet weak var Food_star1: UIButton!
    @IBOutlet weak var Food_star2: UIButton!
    @IBOutlet weak var Food_star3: UIButton!
    @IBOutlet weak var Food_star4: UIButton!
    @IBOutlet weak var Food_star5: UIButton!
    @IBOutlet weak var Food_rate_height: NSLayoutConstraint!
    @IBOutlet weak var rate_delivery_height: NSLayoutConstraint!
    
    @IBOutlet weak var close_btn: UIButton!
    
    @IBOutlet weak var taste_btn: UIButton!
    
    @IBOutlet weak var taste_tick: UIButton!
    
    @IBOutlet weak var portion_btn: UIButton!
    
    @IBOutlet weak var portion_tick: UIButton!
    
    @IBOutlet weak var packing_btn: UIButton!
    
    @IBOutlet weak var packing_tick: UIButton!
    
    @IBOutlet weak var delivery_star1: UIButton!
    
    @IBOutlet weak var delivery_star2: UIButton!
    
    @IBOutlet weak var delivery_star3: UIButton!
    
    @IBOutlet weak var delivery_star4: UIButton!
    
    @IBOutlet weak var delivery_star5: UIButton!
    
    @IBOutlet weak var delivery_time_btn: UIButton!
    
    @IBOutlet weak var delibery_time_tick: UIButton!
    
    @IBOutlet weak var food_handling_btn: UIButton!
    
    @IBOutlet weak var food_handling_tick: UIButton!
    
    @IBOutlet weak var gray_view: UIView!
    
    @IBOutlet weak var your_suggestions_txt_vw: UITextView!
    
    @IBOutlet weak var submit_btn: UIButton!
    
    var attri = NSMutableArray()
    var attrib = NSMutableArray()
    
    var dada = NSArray()
    
    
                override func viewDidLoad() {
                super.viewDidLoad()


                self.discountAmount.isHidden = false
                self.pomocode.isHidden = false


                if Reachability.isConnectedToNetwork() == true {
                print("Internet connection OK")
                } else {

                var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

                topWindow?.rootViewController = UIViewController()
                topWindow?.windowLevel = UIWindowLevelAlert + 1
                let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)

                var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
                controller.view.addConstraint(height)
                controller.view.tintColor = UIColor.red

                var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
                imageView.image = #imageLiteral(resourceName: "no internet")

                controller.view.addSubview(imageView)

                controller.addAction(ok)


                topWindow?.makeKeyAndVisible()
                topWindow?.rootViewController?.present(controller, animated: true, completion:nil)


                }


                tbl1.delegate = self
                tbl1.dataSource =  self

                food_rating = 1
                delivery_rating = 1



                let tapGesture = UITapGestureRecognizer(target: self, action: #selector((tap(gesture:))))
                view.addGestureRecognizer(tapGesture)
                tapGesture.cancelsTouchesInView = false

                to_hide.isHidden = true

                your_suggestions_txt_vw.layer.cornerRadius = 8
                your_suggestions_txt_vw.layer.borderWidth = 0.7
                your_suggestions_txt_vw.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                taste_btn.layer.cornerRadius = 8
                portion_btn.layer.cornerRadius = 8
                packing_btn.layer.cornerRadius = 8
                delivery_time_btn.layer.cornerRadius = 8
                food_handling_btn.layer.cornerRadius = 8

                //hgt popup1 = 1
                gray_view.isHidden = true
                popup_vw1.layer.cornerRadius = 6.0
                popup_vw1.layer.shadowColor = UIColor.black.cgColor
                popup_vw1.layer.shadowOpacity = 0.2
                popup_vw1.layer.shadowOffset = CGSize(width: -1, height: 1)
                popup_vw1.layer.shadowRadius = 3

                your_suggestions_txt_vw.delegate = self

                self.rate_delivery_height.constant = 4
                self.popup_vw2.isHidden = true
                self.popup_vw1.isHidden = true


                taste_btn.layer.borderWidth = 0.7
                taste_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

                portion_btn.layer.borderWidth = 0.7
                portion_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

                packing_btn.layer.borderWidth = 0.7
                packing_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

                delivery_time_btn.layer.borderWidth = 0.7
                delivery_time_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

                food_handling_btn.layer.borderWidth = 0.7
                food_handling_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

                submit_btn.layer.cornerRadius = 8


                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                //hgt bg = 2
                bg_view.layer.cornerRadius = 16.0
                bg_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                bg_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                bg_view.layer.shadowRadius = 3.0
                bg_view.layer.shadowOpacity = 0.2
                bg_view.layer.masksToBounds = false




                print("*******************************")
                print(order_id)



                let header = [
                "Authorization":"Bearer \(authentication_services.instance.token)",
                "Content-Type": "application/json; charset=utf-8"
                ]
                self.spinner.startAnimating()
                Alamofire.request("\(ORDER_DETAILS_URL)\(order_id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in

                if response.result.error == nil {
                guard let response_data = response.data else {return}

                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.order_detail_data = json as! [String : Any]
                if userdata_services.instance.order_detail_data["error"] == nil{

                if userdata_services.instance.order_detail_data["status"] as! String == "ok"{

                userdata_services.instance.oder_detail_id = "Loaded"

                self.spinner.stopAnimating()

                let data1 : Dictionary = userdata_services.instance.order_detail_data["data"] as! Dictionary<String,Any>

                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                print("output = \(data1)")




                self.dada = data1["orderitems"] as! NSArray

                self.daaata1 = data1
                let roo = data1["order_rating"] as? Dictionary<String,Any>
                print(roo)
                if let r = roo as? Dictionary<String,Any>
                {
                self.popup_vw1.isHidden = true
                print("\(roo!["food_rating"]!)")
                self.display = "\(roo!["food_rating"]!)"

                if self.display == "1"{
                self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                }else if self.display == "2"{
                self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)

                }else if self.display == "3"{
                self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)

                }else if self.display == "4"{
                self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)

                }
                else{
                self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                self.st5.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                }
                }
                else{
                self.popup_vw1.isHidden = false

                }


                let da = data1["created_at"] as! String


                if da != "" {
                let formatter = DateFormatter()
                formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let orderedDatetime = formatter.date(from: da)
                print("orderedDatetime = \(orderedDatetime)")

                formatter.dateFormat = "dd-MM-yyyy"
                let orderedDate = formatter.string(from: orderedDatetime!)
                print("orderedDate = \(orderedDate)")

                formatter.dateFormat = "HH:mm"
                let orderedTime = formatter.string(from: orderedDatetime!)
                print("orderedTime = \(orderedTime)")

                self.order_date_lbl.text = "\(orderedDate)\n\(orderedTime)"
                print("order_date_lbl = \(self.order_date_lbl.text)")
                }


                func getRequiredFormat(dateStrInTwentyFourHourFomat: String) -> String? {

                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"

                if let newDate = dateFormatter.date(from: dateStrInTwentyFourHourFomat) {

                let timeFormat = DateFormatter()
                timeFormat.timeZone = TimeZone.autoupdatingCurrent
                timeFormat.locale = Locale(identifier: "en_US_POSIX")
                timeFormat.dateFormat = "hh:mm a"

                let requiredStr = timeFormat.string(from: newDate)

                print("newDate = \(newDate)")
                return requiredStr
                } else {
                return nil
                }
                }



                if data1["transaction_type"]as? String == "takeaway" {

                let addressData = "\(String(describing: data1["address_other_details"] as! String)), \(String(describing: data1["street"] as! String))"
                //                            self.address_title.text = "Picked Up From Store"
                self.address_title.text = "Store Address"

                self.address_lbl.attributedText = addressData.htmlToAttributedString



                }else{

                self.address_title.text = "Delivery Address"
                self.address_lbl.text = "\(String(describing: data1["building"] as! String)), \(String(describing: data1["street"] as! String)),\(String(describing: data1["landmark"] as! String))"

                }

                self.order_id_lbl.text = ("Order Id:\n \(data1["order_id"] as! String)")
                self.rate_your_food_lbl.text = ("Rate Order Id: \(data1["order_id"] as! String)")
                let order_status = data1["orderstatus"] as? Dictionary<String,Any>

                print("order_status = \(order_status)")
                self.order_delivery.text = order_status?["title"] as? String

               



                self.payment_mode_lbl.text = data1["payment_option"] as? String
                self.sub_total_lbl.text = data1["total_price"] as? String


                if data1["discount_code"]as? String != ""{

                self.discountAmtLbl.text = data1["discount_price"]as? String
                self.promoLbl.text = data1["discount_code"]as? String

                }
                else{
                self.discountAmount.isHidden = true
                self.pomocode.isHidden = true
                }






                let user = data1["user"] as? Dictionary<String,Any>
                self.phone_lbl.text = "0\(String(describing: user?["mobileno"] as! String))"
                print(data1["orderitems"] as Any)
                self.products = data1["orderitems"] as! NSArray

                for j in 0...self.products.count-1 {

                var daa1 = self.products[j] as! Dictionary<String,Any>
                var a1 = daa1["order_item_addon_relation"] as! NSArray
                if a1.count != 0{
                for i in 0...a1.count-1
                {
                let a2 = a1[i] as! Dictionary<String,Any>

                self.addon_array.insert(a2, at: i)



                }
                }
                }

                print(data1["orderitems"])
                print(self.products)


                if self.addon_array.count > 0 {

                }




                if self.products.count != 0{

                for ki in 0...self.products.count-1{
                self.new_add_arr1.removeAll()

                let daat = self.products[ki] as! Dictionary<String,Any>

                print(daat)
                //  let daat = datm["orderitems"] as! NSArray

                // if daat.count != 0 {
                let ok = "\(daat["item_name"] as! String) X \(daat["qty"] as! String)"
                let ok1 = daat["price"] as! String

                let dictt = [
                "title":ok,
                "price":ok1
                ] as NSDictionary

                self.new_add_arr1.append(dictt as! [String : Any])

                // for q in 0...self.products.count-1 {

                var dat = self.products[ki] as! Dictionary<String,Any>


                var a1 = dat["order_item_addon_relation"] as! NSArray
                if a1.count != 0{
                for i in 0...a1.count-1
                {
                let a2 = a1[i] as! Dictionary<String,Any>

                let a3 = ("\(a2["title"] as! String) x \(a2["qty"] as! String)")
                let aa3 = ("\(a2["price"] as! String)")



                let dictt = [
                "title":a3,
                "price":aa3
                ] as NSDictionary

                self.new_add_arr1.append(dictt as! [String : Any])



                }
                }


                let b1 = dat["order_item_list_relation"] as! NSArray
                if b1.count != 0{
                for i in 0...b1.count-1{
                let more = b1[i] as! Dictionary<String,Any>

                let more1 = "\(more["title"] as! String) X \(more["qty"] as! String)"

                let dictt = [
                "title":more1,
                "price":""
                ] as NSDictionary

                self.new_add_arr1.append(dictt as! [String : Any])

                let b2 = more["order_item_list_attribute_relation"] as! NSArray
                if b2.count != 0{
                for i in 0...b2.count-1{
                let more1 = b2[i] as! Dictionary<String,Any>
                let b3 = more1["order_item_list_attribute_value_relation__with_selected"] as! NSArray

                if b3.count != 0{

                for i in 0...b3.count-1{

                let b4 = b3[i] as! Dictionary<String,Any>

                let g1 = "\(b4["title"] as! String) X \(b4["qty"] as! String)"
                let g2 = "\(b4["price"] as! String)"

                let dictt = [
                "title":g1,
                "price":g2
                ] as NSDictionary

                self.new_add_arr1.append(dictt as! [String : Any])


                }

                }
                }
                }
                }
                }




                //   }



                //}


                self.final_array.insert(self.new_add_arr1, at: ki)

                let dataSet = try! JSONSerialization.data(withJSONObject: self.final_array, options: JSONSerialization.WritingOptions.prettyPrinted)
                let jsonString = NSString(data: dataSet, encoding: String.Encoding.utf8.rawValue)!

                print(jsonString)
                print( self.final_array)


                }

                self.tbl1.reloadData()

                }



                }
                else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)


                }
                }
                else{
                self.spinner.stopAnimating()
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")

                //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String

                self.present(newViewController, animated: false, completion: nil)


                }

                       if  self.order_delivery.text  == "Delivered"{

                        print("show rating")
                        

                       }else{
                        
                        self.AnimateBackgroundHeight2()

                    }




                }
                else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)



                }
                }
                }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        print(final_array.count)
        return final_array.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //UITableViewAutomaticDimension
        
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(final_array[section].count)
        return final_array[section].count
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! order_details_page_TableViewCell
        
        headerCell.whole_button.addTarget(self, action: #selector(showDetail(_:)), for: .touchUpInside)
        
        headerCell.whole_button.tag = section
        
        print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
        print(products)
        
        let dat = products[section] as! Dictionary<String,Any>
        delivery_charge_lbl.text = (daaata1["delivery_charge"] as! String)
        headerCell.amound_lbl.text = dat["price"] as? String
        headerCell.name_lbl.text = dat["item_name"] as? String
        
        
        let image_name = dat["image"] as! String
        print("\(IMAGE_URL)cakes/\(String(describing: image_name))")
        self.spinner.startAnimating()
        Alamofire.request("\(IMAGE_URL)cakes/\(String(describing: image_name))").responseImage { response in
            
            
            if let image = response.result.value {
                self.spinner.stopAnimating()
                print("image downloaded: \(image)")
                headerCell.img_vw.image = image
            }
        }
        
        self.table_height.constant = self.tbl1.contentSize.height
        
        // return cell
        
        
        return headerCell
        
    }
    
    
    @objc func showDetail(_ button:UIButton){
       
        
        let ifd = dada[button.tag] as! Dictionary<String,Any>
        
        let id = ifd["product_id"] as! String
        
        print ("\(id)")
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "food_details") as! details_ViewController
        newViewController.cake_id = "\(id)"
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbl1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! order_details_page_TableViewCell
            
            
            
            print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            print(products)
            
            let datm = products[indexPath.section] as! Dictionary<String,Any>
            let dat = products[indexPath.section] as! Dictionary<String,Any>
            let new_final = final_array[indexPath.section]
            //****************************************************
            
            var cc = new_final[indexPath.row] as! Dictionary<String,Any>
            cell.Item_name_lbl.text = cc["title"] as? String
            
            if cc["price"] as? String != "0.00"{
                cell.price_lbl.text = cc["price"] as? String
            }else{
                
                cell.price_lbl.text = ""
            }
           
            
            self.table_height.constant = self.tbl1.contentSize.height
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! UITableViewCell
            
            self.table_height.constant = self.tbl1.contentSize.height
            return cell
            
        }
        
        
        
        
    }
    
    @objc func tap(gesture: UITapGestureRecognizer) {
        your_suggestions_txt_vw.endEditing(true)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.your_suggestions_txt_vw.text == "Please Tell Us Your Suggestions..."{
            self.your_suggestions_txt_vw.text = ""
            
            
        }
        
        self.rate_delivery_height.constant = 590
        self.keyboard_height.constant = 192
        self.your_suggestions_txt_vw.frame.size.height = 130
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.your_suggestions_txt_vw.text ==
            ""{
            self.your_suggestions_txt_vw.text = "Please Tell Us Your Suggestions..."
            
            
        }
        
        
        self.rate_delivery_height.constant = 410
        self.keyboard_height.constant = 12
        self.your_suggestions_txt_vw.frame.size.height = 100 //79.5
    }
    
    func AnimateBackgroundHeight() {
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.0,
            options: .curveEaseInOut,
            animations:  {
                self.to_hide.isHidden = false
                self.what_didi_you_lbl.isHidden = false
                self.gray_view.isHidden = false
                self.popup_vw2.isHidden = false
                self.rate_delivery_height.constant = 410
                self.view.layoutIfNeeded()
        },
            completion: nil)
        
    }
    
    func AnimateBackgroundHeight2() {
        
        UIView.animate(
            withDuration: 2.5,
            delay: 0.0,
            options: .curveEaseInOut,
            animations:  {
                self.what_didi_you_lbl.isHidden = true
                self.rate_delivery_height.constant = 0
                self.Food_rate_height.constant = 0
                self.popup_vw2.isHidden = true
                self.popup_vw1.isHidden = true
                self.gray_view.isHidden = true
                self.view.layoutIfNeeded()
        },
            completion: nil)
        
    }
    
    
    
    
    @IBAction func star_1_click(_ sender: Any) {
        
        self.AnimateBackgroundHeight()
        
        food_rating = 1
        Food_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star2.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star3.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
        
    }
    
    @IBAction func star_2_click(_ sender: Any) {
        self.AnimateBackgroundHeight()
        
        food_rating = 2
        Food_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star3.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
        
    }
    
    @IBAction func star_3_click(_ sender: Any) {
        self.AnimateBackgroundHeight()
        
        food_rating = 3
        Food_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        Food_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
    }
    @IBAction func star_4_click(_ sender: Any) {
        self.AnimateBackgroundHeight()
        
        food_rating = 4
        Food_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
    }
    @IBAction func star_5_click(_ sender: Any) {
        self.AnimateBackgroundHeight()
        
        food_rating = 5
        Food_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        Food_star5.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        
    }
    //=========================================================
    
    
    @IBAction func star_21_click(_ sender: Any) {
        delivery_rating = 1
        delivery_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star2.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star3.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
    }
    
    @IBAction func star_22_click(_ sender: Any) {
        
        delivery_rating = 2
        delivery_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star3.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
    }
    
    @IBAction func star_23_click(_ sender: Any) {
        
        delivery_rating = 3
        delivery_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star4.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        delivery_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
    }
    
    @IBAction func star_24_click(_ sender: Any) {
        
        delivery_rating = 4
        delivery_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star5.setBackgroundImage(#imageLiteral(resourceName: "s2"), for: .normal)
        
        
    }
    @IBAction func star_25_click(_ sender: Any) {
        
        delivery_rating = 5
        delivery_star1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        delivery_star5.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
        
        
    }
    
    @IBAction func taste_click(_ sender: Any) {
        attri.add("taste")
        taste = "taste"
        taste_btn.layer.borderWidth = 1
        if taste_btn.layer.borderColor == #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1){
            taste_btn.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
            taste_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (1)"), for: .normal)
            taste_btn.setTitleColor(#colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1), for: .normal)
        }
        else{
            taste = ""
            attri.remove("taste")
            taste_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            taste_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (2)"), for: .normal)
            taste_btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
            
        }
        
        
    }
    
    @IBAction func portion_click(_ sender: Any) {
        
        portion = "portion"
        attri.add("portion")
        portion_btn.layer.borderWidth = 1
        if portion_btn.layer.borderColor == #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1){
            portion_btn.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
            portion_btn.setTitleColor(#colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1), for: .normal)
            
            portion_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (1)"), for: .normal)
        }
        else{
            portion = ""
            attri.remove("portion")
            portion_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            portion_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (2)"), for: .normal)
            portion_btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        }
        
        
    }
    @IBAction func packing_click(_ sender: Any) {
        
        packing = "packing"
        attri.add("packing")
        packing_btn.layer.borderWidth = 1
        if packing_btn.layer.borderColor == #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1){
            packing_btn.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
            packing_btn.setTitleColor(#colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1), for: .normal)
            packing_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (1)"), for: .normal)
        }
        else{
            
            packing = ""
            attri.remove("packing")
            packing_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            packing_btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
            packing_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (2)"), for: .normal)
            
        }
        
        
    }
    @IBAction func delivery_click(_ sender: Any) {
        delivery_time = "Delivery time"
        
        attrib.add("Delivery time")
        delivery_time_btn.layer.borderWidth = 1
        if delivery_time_btn.layer.borderColor == #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1){
            delivery_time_btn.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
            delivery_time_btn.setTitleColor(#colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1), for: .normal)
            delibery_time_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (1)"), for: .normal)
        }
        else{
            
            delivery_time = ""
            attrib.remove("Delivery time")
            delivery_time_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            delibery_time_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (2)"), for: .normal)
            delivery_time_btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        }
        
        
        
    }
    @IBAction func foodhandle_click(_ sender: Any) {
        
        food_handling = "food handling"
        
        attrib.add("food handling")
        food_handling_btn.layer.borderWidth = 1
        if food_handling_btn.layer.borderColor == #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1){
            food_handling_btn.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
            food_handling_btn.setTitleColor(#colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1), for: .normal)
            food_handling_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (1)"), for: .normal)
        }
        else{
            
            food_handling = ""
            attrib.remove("food handling")
            food_handling_btn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            food_handling_btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
            food_handling_tick.setBackgroundImage(#imageLiteral(resourceName: "checked (2)"), for: .normal)
            
        }
    }
    
    
    
    
    @IBAction func submit_click(_ sender: Any) {
        
        self.AnimateBackgroundHeight2()
        var header = [
            "Authorization":"Bearer \(authentication_services.instance.token)",
            "Content-Type": "application/json; charset=utf-8"
        ]
        
        
        let body: [String: Any] = [
            
            "order_id": order_id,
            "food_rating": food_rating,
            "delivery_rating": delivery_rating,
            "suggestions": "\(your_suggestions_txt_vw.text!)",
            "food_attributes": attri,
            
            "delivery_attributes": attrib
            
            
        ]
        
        print("\(body)")
        Alamofire.request("\(BASE_URL)addNewOrderRating", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            print(response.result.error)
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.rating_data = json as! [String : Any]
                if userdata_services.instance.rating_data["error"] == nil{
                    
                    if userdata_services.instance.rating_data["status"] as! String == "ok"{
                        
                        self.spinner.stopAnimating()
                        
                        let data1 : Dictionary = userdata_services.instance.rating_data["data"] as! Dictionary<String,Any>
                        
                        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                        print(data1)
                        
                        
                        //self.viewDidLoad()
                        
                        
                        if self.food_rating == 1{
                            self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                        }else if self.food_rating == 2{
                            self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            
                        }else if self.food_rating == 3{
                            self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            
                        }else if self.food_rating == 4{
                            self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            
                        }
                        else{
                            self.st1.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st2.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st3.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st4.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                            self.st5.setBackgroundImage(#imageLiteral(resourceName: "s1"), for: .normal)
                        }
                        
                        
                        
                        
                        
                    }
                    else{
                        
                        var errorMsg = ""
                        
                        errorMsg = userdata_services.instance.rating_data["message"] as! String
                        if errorMsg != ""
                        {
                            self.spinner.stopAnimating()
                            let alert2 = UIAlertController(title: "", message: errorMsg, preferredStyle: UIAlertControllerStyle.alert)
                            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert2, animated: true, completion: nil)
                        }else{
                        
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        }
                        
                    }
                }
                else{
                    self.spinner.stopAnimating()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                   
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                // Do any additional setup after loading the view.
            }
                /////////////////
                
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
                
                
            }
        }
        
        
        
        
        
        
    }
    @IBAction func close_click(_ sender: Any) {
        
        self.AnimateBackgroundHeight2()
        
    }
   
    @IBAction func track_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "trac"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func back_clicked(_ sender: Any) {
        
        if from_noti == "yes" {
            
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal3") 
            
            if order_delivery.text == "Delivered"{
                userdata_services.instance.fav_check = "delivered"
            }else{
                userdata_services.instance.fav_check = "ongoing"
            }
            
            self.present(newViewController, animated: false, completion: nil)
            
        }else{
            
            dismiss(animated: false, completion: nil)
            
        }
        
    }
    
    
    @IBAction func food_click(_ sender: Any) {
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal2")
        
        self.present(newViewController, animated: false, completion: nil)
        
    }
    
    
    @IBAction func favorites_click(_ sender: Any) {
        
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "book_test") as! book_ViewController
        
        newViewController.check_name = "favorite"
        
        let srtoryBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nnewViewController = srtoryBoard1.instantiateViewController(withIdentifier: "reveal3") //as! book_ViewController
        userdata_services.instance.fav_check = "favorite"
        self.present(nnewViewController, animated: false, completion: nil)
        
    }
    
    @IBAction func home_click(_ sender: Any) {
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal1")
        
        self.present(newViewController, animated: false, completion: nil)
        
        
    }
    
    
    @IBAction func account_click(_ sender: Any) {
        
      /*  let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "acc") as! account_ViewController
        
        let nnewViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal4") //as! account_ViewController
        
        self.present(nnewViewController, animated: false, completion: nil)*/
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let newViewController = storyBoard1.instantiateViewController(withIdentifier: "reveal5") //as! Notification_ViewController
              userdata_services.instance.offer_check = "offer"
              // newViewController.contain = "offer"
              revealViewController()?.revealToggle(nil)
              self.present(newViewController, animated: false, completion: nil)
        
    }
   
    
}

extension String {
    var htmlToAttributedString1: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString1: String {
        return htmlToAttributedString?.string ?? ""
    }
}

