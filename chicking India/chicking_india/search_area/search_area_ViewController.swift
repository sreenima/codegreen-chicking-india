//
//  search_area_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 27/12/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
class search_area_ViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var search_bar: UISearchBar!
    @IBOutlet weak var table_vw: UITableView!
    @IBAction func back_click(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
   
    
    var addr = NSString()
    var addr_pos = NSString()
    var no_ad = NSString()
    var address_keep = NSString()
    var idd = String()
    var from_edit = String()
    var arr = [String]()
    var arr2 = [NSInteger]()
    var data1 = NSArray()
    var search_country = [String]()
    var searching = false
    var check = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        let header = [
            "Authorization":"Bearer \(authentication_services.instance.token)",
            "Content-Type": "application/json; charset=utf-8"
        ]
        spinner.startAnimating()
        Alamofire.request("\("\(BASE_URL)viewAllDeliveryArea")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{(response) in
            print(response.result.error)
            if response.result.error == nil {
                self.spinner.stopAnimating()
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                let doom = json as! [String : Any]
                if doom["error"] == nil{
                    
                    if doom["status"] as! String == "ok"{
                        print("njaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnn")
                        print(doom)
                        self.data1 = doom["data"] as! NSArray
                        
                        for i in 0...self.data1.count-1{
                            var name = self.data1[i] as! Dictionary<String,Any>
                            print(name)
                            var nam = name["name"]!
                            var nam2 = name["id"]!
                            //  self.arr.insert([nam as! String], at: i)
                            self.arr.insert(nam as! String, at: i)
                            self.arr2.insert(nam2 as! NSInteger, at: i)
                            if i == self.data1.count-1{
                                self.table_vw.reloadData()
                            }
                            
                        }
                
                    }
                    else{
                        self.spinner.stopAnimating()
                        var errorMessage = ""
                        errorMessage = doom["message"]as? String ?? ""
                        if errorMessage != ""{
                        let alert2 = UIAlertController(title: "", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        }else{
                        let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                                                   
                        }
                       
                        
                    }
                }
                else{
                    self.spinner.stopAnimating()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                   
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                // Do any additional setup after loading the view.
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
                
                
            }
        }
        
 
    }
    
   
    
}

extension search_area_ViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return search_country.count
        }else{
            return arr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if searching {
            
            cell?.textLabel?.text = search_country[indexPath.row]
        }
        else{
            
            cell?.textLabel?.text = arr[indexPath.row] as! String
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var pos = NSInteger()
        if search_country.count != 0{
            
            var txt = search_country[indexPath.row]
            for i in 0...data1.count-1{
                var d1 = data1[i] as! Dictionary<String,Any>
                var naam = d1["name"] as! String
                if naam == txt {
                    pos = d1["id"] as! NSInteger
                }
                
            }
        }else{
            pos = arr2[indexPath.row]
        }
        userdata_services.instance.area_id = pos as! NSInteger
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//
//        placePicker.delegate = self as GMSPlacePickerViewControllerDelegate
//        present(placePicker, animated: true, completion: nil)
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "gcm") as! GCM_map_ViewController
        
         newViewController.addr_pos = self.addr_pos
         newViewController.no_ad = self.no_ad
         newViewController.addr = self.addr
         newViewController.from_edit = self.from_edit
         newViewController.check = self.check
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
}
extension search_area_ViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        search_country = arr.filter({( item : String) -> Bool in
            return  item.lowercased().contains(searchText.lowercased())})
        
        searching = true
        table_vw.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        table_vw.reloadData()
    }
    
    
}


extension search_area_ViewController : GMSPlacePickerViewControllerDelegate {
    
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.spinner.startAnimating()
        viewController.dismiss(animated: true, completion: nil)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        
        var lat = place.coordinate.latitude
        var long = place.coordinate.longitude
        var position = place.coordinate
        
        
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            let dadr = ("\(place.name), \(place.formattedAddress!)") as NSString
            addr = dadr
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        if from_edit == "yes" {
            newViewController.checker = "NO"
            
            newViewController.check = "no"
            
        }
        else{
            newViewController.checker = "yes"
            if userdata_services.instance.no_address == "no"{
                newViewController.check = "yes"
                newViewController.id_check = self.check
            }
        }
        
        
        if no_ad == "yes" {
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(position) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                } else {
                    if let places = response?.results() {
                        if let place = places.first {
                            
                            
                            if let lines = place.lines {
                                print("GEOCODE: Formatted Address: \(lines)")
                                var plc = "\(lines[0])"
                                newViewController.no_add = self.no_ad
                                
                                newViewController.test = plc as NSString
                                newViewController.addr_coordinates = place.coordinate
                                self.spinner.stopAnimating()
                                self.present(newViewController, animated: false, completion: nil)
                            }
                            
                        } else {
                            
                            newViewController.test = self.addr_pos
                            print("GEOCODE: nil first in places")
                            self.spinner.stopAnimating()
                            self.present(newViewController, animated: false, completion: nil)
                        }
                    } else {
                        
                        newViewController.test = self.addr_pos
                        print("GEOCODE: nil in places")
                        self.spinner.stopAnimating()
                        self.present(newViewController, animated: false, completion: nil)
                    }
                }
            }
            
            
            
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            newViewController.addr_coordinates = place.coordinate
            self.spinner.stopAnimating()
            self.present(newViewController, animated: false, completion: nil)
            
        }
        
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
      
        print("No place selected")
    }
    
}
