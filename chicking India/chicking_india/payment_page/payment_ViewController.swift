//
//  payment_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 10/01/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit
import WebKit

class payment_ViewController: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var please_wait_txt_lbl: UILabel!
    
    @IBOutlet weak var hold_on_txt_lbl: UILabel!
    @IBOutlet weak var we_are_txt_lbl: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var web_vew: WKWebView!
    var user_id = String()
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var ORDER_id = String()
    @IBOutlet weak var activity_img_vw: UIImageView!
    
    var wait_type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            let topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            let height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            let imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        spinner.transform = CGAffineTransform(scaleX: 3, y: 3)
        
        
        web_vew.navigationDelegate = self
        print(ORDER_id)
        
       // payNowFPay
       //  processPayment
        
       // https://india.chickingdelivery.com:11443/api/
      //  https://mobileapp.chickinguae.com/index.php/api
        
        let url = URL(string: "https://india.chickingdelivery.com:11443/payNowFPay/\(String(describing: ORDER_id))")
        var dictionary = BEARER_2HEADER as Dictionary<String,Any>
        let request = URLRequest(url: url!)
        web_vew.load(request)
        progressView.progress = 0.0
        web_vew.addSubview(progressView)
        web_vew.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
        {
            if keyPath == "estimatedProgress"{
            self.progressView.alpha = 1.0
            progressView.setProgress(Float(web_vew.estimatedProgress), animated: true)

            if self.web_vew.estimatedProgress >= 1.0{


            UIView.animate(withDuration: 0.3, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {() -> Void in self.progressView.alpha = 0.0}, completion: { (finished:Bool) -> Void in self.progressView.alpha = 0 })
            }
            }
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        web_vew.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    
    
    func stopLoading() {
           web_vew.removeFromSuperview()
       }
    
    
    @IBAction func exitBtnAction(_ sender: Any) {
        
        
        // Create the alert controller
              let alertController = UIAlertController(title: "Chicking India", message: "Are you sure you want to exit?", preferredStyle: .alert)

              // Create the actions
              let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                  UIAlertAction in
                  NSLog("OK Pressed")
                self.stopLoading()

                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "checkout") as! check_out_ViewController
                self.present(newViewController, animated: false, completion: nil)
              }
              let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) {
                  UIAlertAction in
                  NSLog("Cancel Pressed")
              }

              // Add the actions
              alertController.addAction(okAction)
              alertController.addAction(cancelAction)

              // Present the controller
              self.present(alertController, animated: true, completion: nil)
        
        
        
        
//        dismiss(animated: false, completion: nil)
        
      

//        let alert2 = UIAlertController(title: "Chicking India", message: "Payment failed", preferredStyle: UIAlertControllerStyle.alert)
//        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert2, animated: true, completion: nil)
        
       
        
    }
    
    
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            print("url check = \(urlStr)")
          
            if let resultString = urlStr as? String,
                resultString.contains("fpay-failed") {
                
                wait_type = "canceling"
                self.call_api()
                
                
            }
            if let rresultString = urlStr as? String,
                rresultString.contains("fpay-success") {
                
                wait_type = "Processing"
                self.call_api()
                
                
            }

            
            
        }
        
        decisionHandler(.allow)
    }
    
    
    @IBAction func back_click(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    
    func call_api(){
        
        if wait_type == "canceling"{
            activity_img_vw.isHidden = false
            please_wait_txt_lbl.isHidden =  false
            we_are_txt_lbl.text = "We Are Canceling Your Payment !"
            we_are_txt_lbl.isHidden =  false
            hold_on_txt_lbl.isHidden =  false
            
            spinner.startAnimating()
            
            
        }else if wait_type == "Processing"{
            
            
            activity_img_vw.isHidden = false
            please_wait_txt_lbl.isHidden =  false
            we_are_txt_lbl.isHidden =  false
            hold_on_txt_lbl.isHidden =  false
            
            spinner.startAnimating()
            
            
        }
        
        
        
        authentication_services.instance.payment_confirmation(id: ORDER_id){ (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.payment_way["error"] == nil{
                    
                    if userdata_services.instance.payment_way["status"]  as! String == "ok"{
                        let data = userdata_services.instance.payment_way["data"] as! Dictionary<String,Any>
                        
                        print(data)
                        
                        
                        
                        
                        
                        let dat = data["title"] as! String
                        
                        
                        print("title = \(dat)")
                        
                        if dat == "Invalid" || dat == "Security Error" || dat == "Failure"{
                            
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "failed")
                            
                            
                            self.present(newViewController, animated: false, completion: nil)
                            
                        }else  if dat == "Success"{
                            userdata_services.instance.cart = []
                            
                            print("userdataCart = \(userdata_services.instance.cart)")
                            
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "success")
                            
                        
                            self.present(newViewController, animated: false, completion: nil)
                            
                        }else if dat == "Aborted"{
                            
                            let alert = UIAlertController(title: "", message: "Your Transaction Is Successfully Canceled.", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    self.dismiss(animated: false, completion: nil)
                                    
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                    
                                }}))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if dat == "Pending"{
                            
                            self.call_api()
                            
                        }
                        
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.payment_way["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                   
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Oops! Something Went Wrong.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
        
        
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



