//
//  new_map_pick_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 09/10/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit
import GooglePlaces

class new_map_pick_ViewController: UIViewController {

    // TODO: Add a button to Main.storyboard to invoke onLaunchClicked.
    
    // Present the Autocomplete view controller when the button is pressed.
    @IBAction func onLaunchClicked(sender: UIButton) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = (self as! GMSAutocompleteViewControllerDelegate)
        present(acController, animated: true, completion: nil)
    }
    
}

extension new_map_pick_ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }

}
