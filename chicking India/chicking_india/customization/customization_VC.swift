//
//  customization_VC.swift
//  chicking
//
//  Created by CGT on 13/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class customization_VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var total_price = 0.00
    let rep = 0
    var timer = Timer()
    
    
    var called = String()
    
    @IBOutlet var view_in_scrollview: UIView!
    @IBOutlet var collection_view_height: NSLayoutConstraint!
    @IBOutlet var collection_view: UICollectionView!
    var current_collection_cell_index = 0
    
    var all_counts  = NSMutableArray()
    var attribute_counts  = NSMutableArray()
    var tab_item_new = NSMutableArray()
    var all_items = NSArray()
    var tab_items = NSArray()
    var att = NSArray()
    var sec = 0
    var quantity = String()
    var tableView = UITableView()
    var timerForShowScrollIndicator: Timer?
    
    var item_attributes:Dictionary<String,Any> = ["title":"","image":"","quantity":"","items":[]]
    var item_structure:Dictionary<String,Any> = ["title":"","price":"","quantity":""]
    var items = NSMutableArray()
    var all_items_customized = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        // Do any additional setup after loading the view.
        view_in_scrollview.bringSubview(toFront: collection_view)
        
        if userdata_services.instance.cake_by_id["error"] == nil{
            
            if userdata_services.instance.cake_by_id["status"]  as! String == "ok"{
                
                let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
                self.total_price = total_price + (Double("\(dat["price"]!)")! * Double(userdata_services.instance.quantity)!)
                if let all = dat["item_list"] as? NSArray{
                    
                    all_items = all
                    
                    print(all_items)
                    
                    
                    for i in 0...all_items.count-1{
                        attribute_counts = NSMutableArray()
                        
                        var dat1 = all_items[i] as! Dictionary<String,Any>
                        if let item_attributes = dat1["item_attributes"] as? NSArray{
                            
                            
                            print(item_attributes)
                            
                            if item_attributes.count > 0{
                                let item_attribute_counts = NSMutableArray()
                                for k in 0...(item_attributes.count-1){
                                    
                                    
                                    
                                    let attributes = item_attributes[k] as! Dictionary<String,Any>
                                    
                                    if let attribute_values = attributes["attribute_values"] as? NSArray{
                                        self.tab_items = attribute_values
                                        
                                        self.tab_item_new[k] = self.tab_items
                                        print(tab_item_new)
                                        
                                        for j in 0...tab_items.count-1{
                                            print(tab_items)
                                            print(tab_items.count)
                                            if j == 0{
                                                
                                                self.attribute_counts[j] = "\(Int("\(dat1["qty"]!)")! * Int(self.quantity)!)"
                                                print(self.attribute_counts)
                                            }
                                            else{
                                                
                                                self.attribute_counts[j] = "0"
                                                
                                            }
                                            
                                        }
                                        item_attribute_counts[k] = attribute_counts
                                 
                                    }
                                    
                                }
                                self.tab_items = item_attributes
                                self.all_counts[i] = item_attribute_counts
                                
                            }
                        
                        }
                    
                    }
                  
                    print(all_counts)
                    
                    collection_view.reloadData()
               
                }
                
                
            }
            else{
                
                let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.cake_by_id["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
        }
        
      
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.all_items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? customization_collection_cell{
            
            cell.tab.indicatorStyle = UIScrollViewIndicatorStyle.black
            cell.tab.flashScrollIndicators()
            
            if indexPath.row == 0{
                
                cell.previous_btn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                cell.next_btn.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.1333333333, blue: 0.1254901961, alpha: 1)
            }
            else{
                cell.previous_btn.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.1333333333, blue: 0.1254901961, alpha: 1)
            }
            
            
            
            cell.img.image = UIImage()
            cell.previous_btn.tag = indexPath.row
            cell.next_btn.tag = indexPath.row
            // cell.tab.tag = indexPath.row
            cell.tab.rowHeight = UITableViewAutomaticDimension
            let dat = all_items[indexPath.row] as! Dictionary<String,Any>
            
            
            
            let count = self.all_counts[indexPath.row] as! NSMutableArray
            
            
            
            var total_count = 0
            if tab_items.count > 0{
                
                
                for h in 0...count.count-1{
                    
                    let c = count[h] as! NSArray
                    
                    for i in 0...c.count-1{
                        
                        
                        
                        total_count = Int("\(c[i])")! + total_count
                        
                    }
                    
                }
            }
           
            var q = "\(dat["qty"]!)"
            
            var t = "\(Int(self.quantity)! * Int("\(dat["qty"]!)")!)"
            
            if total_count < (Int(self.quantity)! * Int("\(dat["qty"]!)")!){
                
                
                
                if (Int(self.quantity)! * Int("\(dat["qty"]!)")!) != (Int(total_count)){
                    cell.warning_lbl.text = "select \((Int(self.quantity)! * Int("\(dat["qty"]!)")!) - (total_count)) more items to continue"}
                else{
                    cell.warning_lbl.text = "Only \((Int(self.quantity)! * Int("\(dat["qty"]!)")!)) items can be select"
                }
                
            
            }
            else
            {
                
                cell.warning_lbl.text = "Only \((Int(self.quantity)! * Int("\(dat["qty"]!)")!)) items can be select"
                
            }
            
            
            self.item_attributes["quantity"] = "\(Int(self.quantity)! * Int("\(dat["qty"]!)")!)"
            
            
            let item_details = dat["item_details"] as! Dictionary<String,Any>
            
            
            
            if let item_attributes = dat["item_attributes"] as? NSArray{
                
                
                
                
                if item_attributes.count > 0{
                    
                    cell.tab.reloadData()
                  
                }
                
             
            }
            
            
            self.item_attributes["title"] = "\(item_details["title"]!)"
            
            let img_name = ("\(item_details["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            self.item_attributes["image"] = "\(item_details["image"]!)"
            print("\(IMAGE_URL)common_items\(img_name)")
            
            Alamofire.request(("\(IMAGE_URL)common_items/\(img_name)")).responseImage { response in
                
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.img.image = image
                    
                }
                
            }
         
            return cell
            
        }
        else{
            return customization_collection_cell()
        }
    }
    //collectionview button actions
    
    @IBAction func next_action(_ sender: UIButton) {
        
        called = "no"
        timer.invalidate()
        let dat = all_items[current_collection_cell_index ] as! Dictionary<String,Any>
        
        print(all_items)
        print(dat)
        var total_quantity = "\(Int(self.quantity)! * Int("\(dat["qty"]!)")!)"
        
        
        let count = self.all_counts[current_collection_cell_index] as! NSMutableArray
        
        print(count)
        print(tab_items)
        var total_count = 0
        if tab_items.count > 0{
            for h in 0...count.count-1{
                
                let c = count[h] as! NSArray
                
                for i in 0...c.count-1{
                    
                    
                    
                    total_count = Int("\(c[i])")! + total_count
                    
                }
                
            }
        }
        
        
        
        if "\(total_count)" == total_quantity{
            
            
            print(sender.tag)
            if sender.tag < all_items.count-1{
                
                let jj = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
                let pp: NSArray = jj["item_list"] as! NSArray
                let rr = pp[current_collection_cell_index+1] as! Dictionary<String,Any>
                let oo = rr["item_attributes"] as! NSArray
                if oo.count == 0{
                    
                    
                    sender.tag = sender.tag+1
                    self.next_action(sender)
                }else{
                    
                    current_collection_cell_index = sender.tag + 1
                    collection_view.scrollToItem(at: [0,sender.tag + 1], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                    
                    collection_view.reloadItems(at:[[0,sender.tag + 1 ]] )
                    
                }
                
                
            }
            else{
                
                let d = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
                userdata_services.instance.attributes_count = self.all_counts
                userdata_services.instance.quantity = self.quantity
                let addon =  d["addons"] as! NSArray
                if addon.count > 0{
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "addon")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                else{
                    
                    
                    let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
                    var img_name = ( "\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    userdata_services.instance.product = [
                        
                        "product_id" : "\(dat["id"]!)",
                        "quantity" :  "\(userdata_services.instance.quantity)",
                        "title":"\(dat["title"]!)",
                        "description":"\(dat["description"]!)",
                        "image":"\(img_name)",
                       "price":"\(self.total_price)",
                        "order_item_addon":[],
                        "order_item_list": []
                        
                    ]
                    
                    let order_item_list = NSMutableArray()
                    let item_list = dat["item_list"] as! NSArray
                    
                    
                    
                    for i1 in 0...(item_list.count - 1){
                        
                        let item = item_list[i1] as! Dictionary<String,Any>
                        
                        
                        let item_attributes = item["item_attributes"] as! NSArray
                        if item_attributes.count > 0{
                            
                            
                            for k in 0...(item_attributes.count - 1){
                                
                                
                                var attributes = item_attributes[k] as! Dictionary<String,Any>
                                
                                
                                
                                let attribute_values = attributes["attribute_values"] as! NSArray
                                
                                //2
                                let order_item_list_attributes = NSMutableArray()
                                
                                
                                let order_item_list_attribute_values = NSMutableArray()
                                if attribute_values.count > 0{
                                    print(attribute_values)
                                    for i2 in 0...(attribute_values.count-1){
                                        
                                        var  attribute_value_particular = attribute_values[i2] as! Dictionary<String,Any>
                                        let attribute_counts = userdata_services.instance.attributes_count[i1] as! NSArray
                                        
                                        
                                        print(attribute_counts[k])
                                        if "\(attribute_counts[k])" != "0"{
                                            
                                            if attribute_counts.count - 1 >= i2{
                                                att = attribute_counts[i2] as! NSArray
                                            }
                                            let at = att[i2]
                                            print(at)
                                            
                                            self.total_price = self.total_price + (Double("\(attribute_value_particular["price"]!)")! * Double("\(at)")!)
                                            print(self.total_price)
                                            order_item_list_attribute_values[order_item_list_attribute_values.count] = [
                                                
                                                "item_list_attribute_value_id" : "\(attribute_value_particular["id"]!)",
                                                "quantity" :  "\(att[i2])"
                                                
                                            ]
                                            
                                        }
                                        
                                    }
                                    if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                                        
                                        order_item_list_attributes[order_item_list_attributes.count] = ["item_list_attribute_id" : "\(attributes["id"]!)",
                                            "quantity" : "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)"  ,
                                            
                                            "order_item_list_attribute_values" : order_item_list_attribute_values
                                            
                                            
                                        ]
                                    }
                                    if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                                        order_item_list[order_item_list.count] = [
                                            
                                            "item_list_id" : "\(item["id"]!)",
                                            "quantity" :  "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)",
                                            "order_item_list_attributes" :order_item_list_attributes //[]
                                            
                                            ] as Dictionary<String,Any>
                                    }
                                    
                                    
                                    
                                }
                            }
                        }
                        
                    }
                    
                    userdata_services.instance.product["order_item_list"] = order_item_list
                    
            //revert total payment
                    
                    userdata_services.instance.product["price"] = "\(self.total_price)"
                    print("my product list : \(userdata_services.instance.product)")
                    userdata_services.instance.cart[userdata_services.instance.cart.count] = userdata_services.instance.product
                    
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            
        }
        else{
            
            
            let alert2 = UIAlertController(title: "", message: "Add more items to continue.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            
            
        }
        
        
        
    }
    
    @IBAction func back_btn_action(_ sender: Any) {
        
        
        self.dismiss(animated: false, completion: nil)
        
    }
    @IBAction func previous_action(_ sender: UIButton) {
        
        
        if sender.tag > 0{
            
            
            current_collection_cell_index = sender.tag - 1
            collection_view.scrollToItem(at: [0,sender.tag - 1], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            
            collection_view.reloadItems(at:[[0,sender.tag - 1 ]] )
        }
        
    }
    
    //collectionview flowdelegates
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        
        
        return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        
        return 0
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
        
    }
  
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return (self.all_counts[current_collection_cell_index] as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as? customization_table_cell{
            let dat = all_items[current_collection_cell_index] as! Dictionary<String,Any>
            
            if let item_attributes = dat["item_attributes"] as? NSArray{
             
                if item_attributes.count > 0{
                    let attributes = item_attributes[section] as! Dictionary<String,Any>
                    print(attributes)
                    
                    let attribute_details = attributes["attribute_details"] as! Dictionary<String,Any>
                    cell.header_title.text
                        = "\(attribute_details["title"]!)"
                    
                    if let attribute_values = attributes["attribute_values"] as? NSArray{
                        self.tab_items = attribute_values
                        print(self.tab_items)
                        
                    }
                    
                }
                
            }
            
            
            return cell
            
        }
        else{
            return customization_table_cell()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let r = self.all_counts[current_collection_cell_index] as! NSArray
        print(self.all_counts)
        print(r)
        
        return (r[section] as! NSArray).count
        //        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? customization_table_cell{
            
            tableView.indicatorStyle = UIScrollViewIndicatorStyle.black
            tableView.flashScrollIndicators()
            tableView.showsVerticalScrollIndicator = true
            cell.plus_btn.tag = indexPath.row
            
            cell.minus_btn.tag = indexPath.row
            var current_collectionitems = self.all_items[current_collection_cell_index] as! Dictionary<String,Any>
            print(current_collectionitems)
            let tab_items_all = current_collectionitems["item_attributes"] as! NSArray
            print(tab_items_all)
            var tab_items_all_dic = tab_items_all[indexPath.section] as! Dictionary<String,Any>
            print(tab_items_all_dic)
            self.tab_items = tab_items_all_dic["attribute_values"] as! NSArray
            print(self.tab_items)
            
            let total_counts = self.all_counts[current_collection_cell_index] as! NSMutableArray
            print(total_counts)
            let counts = total_counts[indexPath.section] as! NSMutableArray
            print(counts)
            
            
            if self.tab_items.count-1 >= indexPath.row{
                self.sec = indexPath.section
                let attributes = self.tab_items[indexPath.row] as! Dictionary<String,Any>
                print(attributes)
                let attribute_value_details = attributes["attribute_value_details"] as! Dictionary<String,Any>
                cell.title_lbl.text = "\(attribute_value_details["title"]!)"
                print(attributes)
                if "\(attributes["price"]!)" == "0.00"{
                    
                    cell.price_lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                else{
                    cell.price_lbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }
                
                if "\(attributes["price"]!)" != "0.00"{
                    
                    
                    if indexPath.row < counts.count{
                        
                        var coutt = "\(counts[indexPath.row])"
                        var cout:Int! = Int(coutt)
                        
                        cell.price_lbl.text = "+\(Double("\(attributes["price"]!)")! * Double(cout)) INR"
                        if cell.price_lbl.text == "+0.0 INR" {
                            
                            cell.price_lbl.text = "+\(String(describing: Double("\(attributes["price"]!)")! )) INR"
                        }
                        
                    }
                    else{
                        
                        cell.price_lbl.text = "+\(Double("\(attributes["price"]!)")!) INR"
                        if cell.price_lbl.text == "+0.0 INR" {
                            
                            cell.price_lbl.text = "+\(String(describing: Double("\(attributes["price"]!)")! )) INR"
                        }
                    }
                }
              
                print("\(indexPath),\(counts)")
                
                if indexPath.row < counts.count{
                    
                    cell.count_lbl.text = "\(counts[indexPath.row])"}
                
            }
            
            return cell
        }
        else{
            return customization_table_cell()
        }
    }
    
    
    @objc func tst(tst:Int)
    {}
    //tableview button actions
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if called == "yes"{
            
            print("not called")
            
            
        }else{
            called = "yes"
            self.timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { [weak self] timer in
                tableView.showsVerticalScrollIndicator = true
                tableView.flashScrollIndicators()
                
            }
        }
        
        
    }
    
    @IBAction func add_action(_ sender: UIButton) {
        
        
        let collection_cell = collection_view.cellForItem(at: [0,current_collection_cell_index]) as! customization_collection_cell
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collection_cell.tab)
        let indexPath1 = collection_cell.tab.indexPathForRow(at: buttonPosition)
      
        let dat = all_items[current_collection_cell_index ] as! Dictionary<String,Any>
        
        print(all_items[current_collection_cell_index ])
        self.item_attributes["quantity"] = "\(Int(self.quantity)! * Int("\(dat["qty"]!)")!)"
        
        let total_counts = self.all_counts[current_collection_cell_index] as! NSMutableArray
        print(total_counts)
        //print(indexPath_tab?.section)
        
        let count = total_counts[(indexPath1?.section)!] as! NSMutableArray
        
        print(count)
        
        var total_count = 0
        if tab_items.count > 0{
            print(tab_items.count)
            for i in 0...count.count-1{
                
                
                
                total_count = Int("\(count[i])")! + total_count
                print(count[i])
                print(total_count)
            }
        }
        
        print(self.quantity)
        if total_count < (Int(self.quantity)! * Int("\(dat["qty"]!)")!){
            
            let c = "\(count[sender.tag])"
            
            print(c)
            
            
            
            count[sender.tag] = "\(Int(c)! + 1)"
            print(count[sender.tag])
            
            total_counts.replaceObject(at: (indexPath1?.section)!, with: count)
            self.all_counts.replaceObject(at: current_collection_cell_index, with: total_counts)
            
            
            
            
            
            if (Int(self.quantity)! * Int("\(dat["qty"]!)")!) != (Int(total_count) + 1){
                
                
            }
            else{
                
            }
            
        }
        else if total_count == (Int(self.quantity)! * Int("\(dat["qty"]!)")!){
            
            
            
        }
        
        collection_view.reloadItems(at: [[0,current_collection_cell_index]])
        
        
        
    }
    
    
    
    @IBAction func substract_action(_ sender: UIButton) {
        let collection_cell = collection_view.cellForItem(at: [0,current_collection_cell_index]) as! customization_collection_cell
        
        
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collection_cell.tab)
        let indexPath1 = collection_cell.tab.indexPathForRow(at: buttonPosition)
        
        print(indexPath1)
        
        
        let dat = all_items[current_collection_cell_index ] as! Dictionary<String,Any>
        
        self.item_attributes["quantity"] = "\(Int(self.quantity)! * Int("\(dat["qty"]!)")!)"
        
        let total_counts = self.all_counts[current_collection_cell_index] as! NSMutableArray
        
       
        
        let count = total_counts[(indexPath1?.section)!] as! NSMutableArray
        
        var total_count = 0
        if tab_items.count > 0{
            
            for i in 0...count.count-1{
                
                
                
                total_count = Int("\(count[i])")! + total_count
                
            }
        }
        
        if total_count > 0{
            
            let c = "\(count[sender.tag])"
            
            if Int(c)! != 0{
                
                count[sender.tag] = "\(Int(c)! - 1)"
                print(count[sender.tag])
                
                total_counts.replaceObject(at: (indexPath1?.section)!, with: count)
                self.all_counts.replaceObject(at: current_collection_cell_index, with: total_counts)
                // self.all_counts.replaceObject(at: current_collection_cell_index, with: count)
                
                if (Int(self.quantity)! * Int("\(dat["qty"]!)")!) != (Int(total_count) - 1){
                    
                    
                }
                else{
                    
                }
            }
        }
        else {
            
        }
        
        collection_view.reloadItems(at: [[0,current_collection_cell_index]])
        
    }
 
    
}
//for indexpath


extension UIResponder {
    
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}
extension UITableViewCell {
    
    var tableView: UITableView? {
        return next(UITableView.self)
    }
    
    var indexPaths: IndexPath? {
        return tableView?.indexPath(for: self)
    }
}

