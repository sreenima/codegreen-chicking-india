//
//  customization_collection_cell.swift
//  chicking
//
//  Created by CGT on 13/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class customization_collection_cell: UICollectionViewCell {
    
    @IBOutlet var warning_lbl: UILabel!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var tab: UITableView!
    @IBOutlet var next_btn: RoundedShadowButton!
    @IBOutlet var previous_btn: RoundedShadowButton!
   
    

}

