//
//  swipe_screen.swift
//  chicking
//
//  Created by Ios CGT on 10/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class swipe_screen: UIViewController {
    
    var timer = Timer()
    @IBAction func skip_to_login(_ sender: Any) {
        timer.invalidate()
        userdata_services.instance.skip_to_login = true
//        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "d_method") as! Delivery_method_ViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        //      present(dashboardWorkout, animated: false, completion: nil)
//        self.present(newViewController, animated: false, completion: nil)
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { [weak self] timer in
            if userdata_services.instance.skip_to_login == false{
                
                
//                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "d_method")
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                self!.view.window!.layer.add(transition, forKey: kCATransition)
                //      present(dashboardWorkout, animated: false, completion: nil)
                
                
//                self!.present(newViewController, animated: false, completion: nil)
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
                
                self!.present(newViewController, animated: false, completion: nil)
            }
        }
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeRight2 = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight2.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight2)
        
        
        
        
        
    }
    
    // function which is triggered when handleTap is called
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped right")
                timer.invalidate()
//
//                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "d_method")
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                self.view.window!.layer.add(transition, forKey: kCATransition)
//                self.present(newViewController, animated: false, completion: nil)
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
                
                self.present(newViewController, animated: false, completion: nil)
                
            case UISwipeGestureRecognizerDirection.right:
                timer.invalidate()
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "swipe2")
                
                
                self.present(newViewController, animated: false, completion: nil)
                
            default:
                break
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
