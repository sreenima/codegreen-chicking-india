//
//  swipe1_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 18/09/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class swipe1_ViewController: UIViewController {
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { [weak self] timer in
            if  userdata_services.instance.skip_to_login == false{
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "swipe2")
                
                
                self!.present(newViewController, animated: false, completion: nil)
                
            }
        }
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        
        
    }
    
    // function which is triggered when handleTap is called
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped Left")
                timer.invalidate()
                if  userdata_services.instance.skip_to_login == false{
                    let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard1.instantiateViewController(withIdentifier: "swipe2")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                }
            default:
                break
            }
        }
    }
    
    
    @IBAction func swipe1_click(_ sender: Any) {
        
        timer.invalidate()
        userdata_services.instance.skip_to_login = true
//        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "d_method") as! Delivery_method_ViewController
//
//
//        self.present(newViewController, animated: false, completion: nil)
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
