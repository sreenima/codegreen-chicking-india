

import Foundation


typealias CompletionHandler = (_ Success:Bool)->()



//let BASE_URL = "http://www.cgtindia.in/labrioche-api/api/public/api/"
//let BANNER_IMAGE_URL = "http://www.cgtindia.in/labrioche-api/api/public/images/banners/"
//let IMAGE_URL = "http://www.cgtindia.in/labrioche-api/api/public/images/"
//let BASE_URL = "http://www.cgtindia.in/chicking-api/api/public/api/"

//let BASE_URL = "http://chickingdxb.dyndns.org:2580/chicking-api/api/public/index.php/api/"
//let BANNER_IMAGE_URL = "http://chickingdxb.dyndns.org:2580/chicking-api/api/public/images/banners/"
//let IMAGE_URL = "http://chickingdxb.dyndns.org:2580/chicking-api/api/public/images/"
//let IMG_URL = "http://chickingdxb.dyndns.org:2580/chicking-api/api/public/images/"

let BASE_URL = "https://india.chickingdelivery.com:11443/api/"
//let BANNER_IMAGE_URL = "https://mobileapp.chickinguae.com/images/banners/"
let BANNER_IMAGE_URL = "https://india.chickingdelivery.com:11443/images/banners/"
//let IMAGE_URL = "https://mobileapp.chickinguae.com/images/"
let IMAGE_URL = "https://india.chickingdelivery.com:11443/images/"
//let IMG_URL = "https://mobileapp.chickinguae.com/images/"
let IMG_URL = "https://india.chickingdelivery.com:11443/images/"


//let IMG_URL = "http://www.cgtindia.in/labrioche-api/api/public/images/"
let REGISTER_URL = "\(BASE_URL)userRegister"
let VERIFY_OTP_REGISTER_URL = "\(BASE_URL)NewUserOtpVerify"
let LOGIN_URL = "\(BASE_URL)userlogin"
let FORGOT_PWD_URL = "\(BASE_URL)forgotPassword"
let FORGOT_RESET_PWD_URL = "\(BASE_URL)PasswordChange"
let BANNERS_URL = "\(BASE_URL)banners/"
let FOOD_CATEGORIES_URL = "\(BASE_URL)foodCategories"
let FOOD_ITEMS_URL = "\(BASE_URL)listFoodItems/"
let CAKE_CATEGORIES_URL = "\(BASE_URL)listAllCakeCategory"
let CAKE_ITEMS_URL = "\(BASE_URL)listCakesByCategory/"
let USER_DETAILS_URL = "\(BASE_URL)userdetails"
let USER_PROFILE_UPDATE = "\(BASE_URL)userProfileUpdate/"
let DISPLAY_CAKE_BY_ID_URL = "\(BASE_URL)displayCakeById/"
let VIEW_FIRST_ADDRESS = "\(BASE_URL)viewDefaultAddress"
let ADD_ADDRESS = "\(BASE_URL)addAddress"
let LIST_ADDRESS = "\(BASE_URL)listAllAddress"
let DELETE_ADDRESS = "\(BASE_URL)deleteAddress/"
let CHANGE_ADDRESS = "\(BASE_URL)changeAddress/"
let DELIVERY_CHARGE = "\(BASE_URL)showDeliveryCharge"
let DISPAY_PackingCharge = "\(BASE_URL)displayPackingCharge"
let ORDER_PROCESSING = "\(BASE_URL)orderStore"
let VIEW_ORDERS_URL = "\(BASE_URL)listOrders"
let ONGOING_ORDER_URL = "\(BASE_URL)listOnGoingOrders"
let PAST_ORDER_URL = "\(BASE_URL)listPastOrders"
let FAVORITE_ORDER_URL = "\(BASE_URL)listFavoriteOrders"
let COUNTRYCODE_URL = "\(BASE_URL)listAllCountryCodes"
let DELIVERY_PARTNERS_URL = "\(BASE_URL)listDeliverPartners"
let EDIT_ADDRESS_URL = "\(BASE_URL)changeAddress/"
let LOGOUT_URL = "\(BASE_URL)logout"
let OTP_REGISTERED_USER_URL = "\(BASE_URL)otpForRegisteredUser"
let PROMOCODE_URL = "\(BASE_URL)checkPromocode"

let CITIES_LIST = "\(BASE_URL)ShowCities"
let BRANCHES_LIST = "\(BASE_URL)ShowBranches/"

let SHOW_CITIES_URL = "\(BASE_URL)ActiveDeliveryAreaList"
let SHOW_BRANCHES_URL = "\(BASE_URL)getBranchByArea/"

let ORDER_DETAILS_URL = "\(BASE_URL)listOrderbyID/"
let GET_SIZE_URL = "\(BASE_URL)sizeDisplay/"
let GET_base_URL = "\(BASE_URL)basesDisplay"
let GET_TOPINGS_URL = "\(BASE_URL)toppingsDisplay"
let UPLOAD_IMG = "\(BASE_URL)uploadImage"
let DESIGN_CHARGE = "\(BASE_URL)showDesignCharge"
let VIEW_NOTIFICATION_URL = "\(BASE_URL)GeneralNotificationList"
let VIEW_PROMOTIONAL_NOTIFICATION_URL = "\(BASE_URL)PromotionalNotificationList"
let NOTIFICATION_STATUS_URL = "\(BASE_URL)NotificationStatusUpdate"
let POPULAR_DISHES_URL = "\(BASE_URL)popularDishes"
let CUSTOMIZATION_URL = "\(BASE_URL)listAllProductOptions/"
let LAST_API_CALL = "\(BASE_URL)getPaymentStatus/"
let NEW_OTP_phone = "\(BASE_URL)getNewOtp"
let NUMBER_CHANGE_URl = "\(BASE_URL)mobileNumberChange"

let Change_to_new_num_url = "\(BASE_URL)ChangeToNewNumber"
//let GOLD_PRICE_URL = "\(BASE_URL)viewGoldPrice"

let ACCOUNT_EDIT_MAIN_URL =  "\(BASE_URL)ProfileUpdate/"

let CHANGE_NUMBER_OTP_url = "\(BASE_URL)OtpForMobileChange"

let change_pass_url = "\(BASE_URL)changePassword"

let testimonials_Url = "\(BASE_URL)testimonials"

let timing_url = "\(BASE_URL)timing"

let NEW_Time_Url = "\(BASE_URL)settings"

let fc_toke = "\(BASE_URL)updateDeviceToken"

let device_info_url = "\(BASE_URL)addDeviceInfo"

let check_update_url = "\(BASE_URL)getLatestAppVersion"

// Headers
let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]
var BEARER_2HEADER = [
    "Authorization":"Bearer \(authentication_services.instance.token)",
    "Content-Type": "application/json; charset=utf-8"
]

var BEARER_HEADER = [
//    "Authorization":"Bearer \(authentication_services.instance.token)",
    "Content-Type": "application/json; charset=utf-8"
]

var BEARER_HEADER_IMG = [
    "Authorization":"Bearer \(authentication_services.instance.token)"
    
]
//var BEARER_HEADER = [
//    "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJpc3MiOiJodHRwOi8vd3d3LmNndGluZGlhLmluL2xhYnJpb2NoZS1hcGkvYXBpL3B1YmxpYy9hcGkvdXNlcmxvZ2luIiwiaWF0IjoxNTM2NzQ4MDY3LCJleHAiOjE1MzczNTI4NjcsIm5iZiI6MTUzNjc0ODA2NywianRpIjoiZVVuRm9UREhyVjQzUXNjRiJ9.Pk5qfhPKJNJou_1qR9WQc8uMIma71xoHVXjeVBpbEBM",
//    "Content-Type": "application/json; charset=utf-8"
//]






//keys




let CART_KEY = "cart"
let USER_ID_KEY = "userid"
let MOBILE_KEY = "mobile"
let TOKEN_KEY = "token"
let ISLOGGEDIN_KEY = "isloggedin"
let CART_MAIN_KEY = "cartmain"

let DELIVERY_TYPE = "type"



