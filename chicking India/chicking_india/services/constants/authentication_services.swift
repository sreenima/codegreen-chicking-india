
import Foundation
import Alamofire


class authentication_services {
    static let instance = authentication_services()
    
    let defaults = UserDefaults.standard
    
    
    var isloggedin : Bool{
        
        get{
            
            return defaults.value(forKey: ISLOGGEDIN_KEY) as! Bool
            
        }
        set{
            
            defaults.set(newValue, forKey: ISLOGGEDIN_KEY)
            
        }
        
        
    }
    
    var Delivery_type : String{
        
        get{
            
            return defaults.value(forKey: DELIVERY_TYPE) as! String
            
        }
        set{
            
            defaults.set(newValue, forKey: DELIVERY_TYPE)
            
        }
        
        
    }
    
    

    var token : String{
        
        get{
            
            return defaults.value(forKey: TOKEN_KEY) as! String
            
        }
        set{
            
            defaults.set(newValue, forKey: TOKEN_KEY)
            
        }
        
        
    }
    var userid : String{
        
        get{
            
            return defaults.value(forKey: USER_ID_KEY) as! String
            
        }
        set{
            
            defaults.set(newValue, forKey: USER_ID_KEY)
            
        }
    }
        var mobile : String{
        
        get{
        
        return defaults.value(forKey: MOBILE_KEY) as! String
        
        }
        set{
        
        defaults.set(newValue, forKey: MOBILE_KEY)
        
        }
        
        
        
        
        
    }
    var cart_session : NSArray{
        
        get{
            
            return defaults.value(forKey: CART_MAIN_KEY) as! NSArray
            
        }
        set{
            
            defaults.set(newValue, forKey: CART_MAIN_KEY)
            
        }
        
        
    }
   // var cart = NSMutableArray()

    
    
    
    func register_user(name:String,password:String,mobile:String,country:String,email:String,completion:@escaping CompletionHandler )  {
        let body: [String: Any] = [
            
            "name" : name,
            "mobileno" : mobile,
            "password" : password,
            "country_code_id" : country,
            "email" : email
            
        ]
        
       print(body)
        Alamofire.request(REGISTER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.register_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                print(REGISTER_URL)
               print(response.error.debugDescription)
                
                
                completion(false)
            }
            
        }
        
        
    }
    
    
    func verify_otp_register(otp:String,userid:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        let body: [String: Any] = [
            
            "user_id" : userid,
            "otp" : otp,
            "token": [
                "device": "ios",
                "value": "654C4DB3-3F68-4969-8ED2-80EA16B46EB0"//userdata_services.instance.firebase_token
            ]
            
        ]
        
        print("\(body)")
        Alamofire.request(VERIFY_OTP_REGISTER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.verify_otp_register_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
 
        
    }
    
    

    
    func login(mobile:String,password:String,value:String,completion:@escaping CompletionHandler ) {
       
        
        let body: [String: Any] = [
            
            "mobileno" : mobile,
            "password" : password,
            "token": [
             "device": "ios",
             "value": value//userdata_services.instance.firebase_token
             ]
            
        ]
        
        print("body is:\(body)")
        Alamofire.request(LOGIN_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.login_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
     
        
        
    }
    
    
 
    func forgot_pwd(mobile:String,coutry_id:String,completion:@escaping CompletionHandler ) {
        
    
        
        let body: [String: Any] = [
            
           
            "mobileno" :mobile,
            
            "country_code_id" : coutry_id
            
        ]
        print(body)
        
        Alamofire.request(FORGOT_PWD_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.forgot_pwd_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }

    
    
    
    
    
    
    
    
    
    
    func forgot_reset_pwd(otp:String,password:String,userid:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        let body: [String: Any] = [
            
"user_id" : userid,
"otp" : otp,
"password" : password,
"token": [
"device": "ios",
"value": "654C4DB3-3F68-4969-8ED2-80EA16B46EB0"//userdata_services.instance.firebase_token

            
        ]]
        print(body)
        
        Alamofire.request(FORGOT_RESET_PWD_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.forgot_reset_pwd_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
    
    }
    
    
    
    
    
    
  
    
    
    
    
    
    
    //for food banners
    
    
    func banner_images(FoodORcake:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        

        
        Alamofire.request("\(BANNERS_URL)cake", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                print("\(BANNERS_URL)cake")
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.banner_images = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
  
    
    
    
    
    
    
    
    func food_categories(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(FOOD_CATEGORIES_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.food_categories_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func listfood_items(id:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        print("\(FOOD_ITEMS_URL)\(id)")
        
        Alamofire.request("\(FOOD_ITEMS_URL)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.list_fooditems_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    //cake banner
    
    
    func banner_image_cake(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(BANNERS_URL)cake", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.banner_cake_images = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    func cake_categories(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(CAKE_CATEGORIES_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.cake_categories_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func listcake_items(id:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        print("\(CAKE_ITEMS_URL)\(id)")
        
        Alamofire.request("\(CAKE_ITEMS_URL)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.list_cakeitems_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    func user_details(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
       
        
        Alamofire.request("\(USER_DETAILS_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.user_details_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    func user_profile_update(name:String,email:String,dob:String,loyality:String,id:String,completion:@escaping CompletionHandler ) {
        
        let body: [String: Any] = [
            
            "name": name,
           "email":email,
           "dob" : dob,
           "loyality_card": loyality]
        
        
        
        
        
        
        Alamofire.request("\(USER_PROFILE_UPDATE)\(id)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.user_profile_update = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    func cake_by_id(id:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        //DISPLAY_CAKE_BY_ID_URL
        
//        print(token)
        
        Alamofire.request("\(CUSTOMIZATION_URL)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                
               // self.customization(id: id, completion: {_ in })
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.cake_by_id = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    func show_delivery_charges(completion:@escaping CompletionHandler ) {
        
   
        
        Alamofire.request("\(DELIVERY_CHARGE)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.show_delivery_charges = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
     func show_PackingCharge(completion:@escaping CompletionHandler ){
         
         Alamofire.request("\(DISPAY_PackingCharge)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
             
             if response.result.error == nil {
                 guard let response_data = response.data else {return}
                 
                 let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                 print("packing_charge = \(json as Any)")
                 userdata_services.instance.show_packing_charges = json as! [String : Any]
                 completion(true)
                 
             }
             else{
                 
                 completion(false)
             }
             
         }
         
     }
    
    
    
    
    func show_first_address(completion:@escaping CompletionHandler ) {
        
        
        
        Alamofire.request("\(VIEW_FIRST_ADDRESS)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.show_first_address_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    func list_all_address(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(LIST_ADDRESS)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.list_all_address_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    func delete_address(id:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(DELETE_ADDRESS)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.delete_address_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    func add_address(address:String,flat_name:String,landmark:String,other_details:String,tick_type:String,area_id:String,building:String,longi:String,lati:String,completion:@escaping CompletionHandler ) {
        
        
        
        let body = [
            
            "flat_name" : flat_name,
            "street": address,
            "building": building,
            "landmark": landmark,
            "other_details": other_details,
            "type": tick_type,
            "is_default":"yes",
            "area_id" : area_id,
            "longitude": longi,
            "latitude": lati
            
        ]
        
        print(body)
        
        
        Alamofire.request("\(ADD_ADDRESS)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            
            
            
            
            
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.add_address_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    func Edit_addresS(id:String,completion:@escaping CompletionHandler ) {
        
        
        
        let body = [
            
            
            "is_default":"yes"
            
            
        ]
        
        
        
        
        Alamofire.request("\("\(BASE_URL)changeDefaultAddress/")\(id)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            
            
            
            
            
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.add_address_data1 = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
               print(response)
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    func order_processing(body:Dictionary<String, Any>,completion:@escaping CompletionHandler ) {
        
    
        
        print(body)
        
        Alamofire.request("\(ORDER_PROCESSING)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.order_processing = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                print(response)
                print(response.result.error)
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    func my_orders(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(VIEW_ORDERS_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.my_orders_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    func past_orders(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(PAST_ORDER_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.past_order_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    func ongoing_orders(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(ONGOING_ORDER_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.ongoing_order_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
   
    
    
    func favorite_orders(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(FAVORITE_ORDER_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.favorite_order_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    func countrycodes(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(COUNTRYCODE_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.countrycode_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    func deliverypartners(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(DELIVERY_PARTNERS_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.delivery_partners_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    func edit_address(id:String,address:String,flat_name:String,landmark:String,other_details:String,tick_type:String,area_id:String,completion:@escaping CompletionHandler ) {
        
        
        
    let body = [
    
    "flat_name" : flat_name,
    "address": address,
    "landmark": landmark,
    "other_details": other_details,
    "type": tick_type,
    "is_default": "yes",
    "area_id": area_id
    
    
    ]
        print(body)
        print("\(EDIT_ADDRESS_URL)\(id)")
        Alamofire.request("\(EDIT_ADDRESS_URL)\(id)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.edit_address_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func logout(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(LOGOUT_URL)", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.logout_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    func alreadyreg_user_otp(userid:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        let body: [String: Any] = [
            
"user_id" : userid
            
            
            
        ]
        print(body)
        
        Alamofire.request(OTP_REGISTERED_USER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.registered_user_otp_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    func promocode(promocode:String,amount:String,completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        let body: [String: Any] = [
            
             "code":"\(promocode)",
            "amount":"\(amount)"
            
        ]
        
        print("\(body)")
        Alamofire.request(PROMOCODE_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            print(response)
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.promocode_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
    }
    
    
    func cities_List(completion:@escaping CompletionHandler ) {
           
           
        //ShowCities
           Alamofire.request(CITIES_LIST, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
               
               if response.result.error == nil {
                   guard let response_data = response.data else {return}
                   
                   let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                   print(json as Any)
                   
                   print("response = \(json as! [String : Any])")
                   userdata_services.instance.show_ciities_data = json as! [String : Any]
                   completion(true)
                   
               }
               else{
                   
                   
                   completion(false)
               }
               
           }
       }
    
    
    
    
    
    
    
    func show_cities(completion:@escaping CompletionHandler ) {
        
        
     //   ActiveDeliveryAreaList

        Alamofire.request(SHOW_CITIES_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                
                print("response = \(json as! [String : Any])")
                userdata_services.instance.show_ciities_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                completion(false)
            }
            
        }
    }
    
    
    
    func branches_list(cityid:String,completion:@escaping CompletionHandler ) {
        
        print("cityid = \(cityid)")
       // ShowBranches

        Alamofire.request("\(BRANCHES_LIST)\(cityid)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.show_branch_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
    }
    
    
    
    
    
   
    func show_branches(cityid:String,completion:@escaping CompletionHandler ) {
        
        print("cityid = \(cityid)")
       // getBranchByArea

        Alamofire.request("\(SHOW_BRANCHES_URL)\(cityid)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.show_branch_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
    }
    
    
    
    func order_details(orderid:String,completion:@escaping CompletionHandler ) {
        
        
        Alamofire.request("\(ORDER_DETAILS_URL)\(orderid)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [
            "Authorization":"Bearer \(authentication_services.instance.token)",
            "Content-Type": "application/json; charset=utf-8"
            ]).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.order_detail_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    func get_size(shape:String,completion:@escaping CompletionHandler ) {
        
        
        Alamofire.request("\(GET_SIZE_URL)\(shape)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.get_size_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
    }

    func get_base(shape:String,size:String,completion:@escaping CompletionHandler ) {
        
        let body: [String: Any] = [
            "size" :size,
            "shape" : shape
            
        ]
        print(body)
        
        Alamofire.request(GET_base_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.get_base_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                completion(false)
            }
            
        }
    
    }
    func get_topings(shape:String,size:String,completion:@escaping CompletionHandler ) {
        
        let body: [String: Any] = [
            "size" :size,
            "shape" : shape
        ]
        print(body)
        
        Alamofire.request(GET_TOPINGS_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.get_topings_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                completion(false)
            }
            
        }
        
    }
    

    func upload_cake_img(img_url:NSURL,completion:@escaping CompletionHandler ) {

        // User "authentication":
            let parameters = ["user_id":authentication_services.instance.userid]
        
             // Image to upload:
             let imageToUploadURL = img_url as URL
             // Server address (replace this with the address of your own server):
             let url = "\(UPLOAD_IMG)"
        Alamofire.upload(multipartFormData:{ multipartFormData in
                                        // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            multipartFormData.append(imageToUploadURL, withName: "image_name")
            
           //  multipartFormData.append(data, withName: "avatar", fileName: fileName!,mimeType: "image/jpeg")
           
            for (key, val) in parameters {
                multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                print(key)
                print(val)
            }
        },
    usingThreshold:UInt64.init(),
    to:UPLOAD_IMG,
    method:.post,
    headers:BEARER_HEADER_IMG,
    encodingCompletion: { encodingResult in
    switch encodingResult {
    case .success(let upload, _, _):
    upload.responseJSON { response in
        
        
        guard let response_data = response.data else {return}
        
        
        
        if response.result.error == nil {
        
        
        
        let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
        print(json as Any)
        userdata_services.instance.img_upload_data = json as! [String : Any]
        
        completion(true)
            debugPrint(response)}
        
        else{
        
            completion(false)
            debugPrint(response)
        
        
        }
        
    }
    case .failure(let encodingError):
    print(encodingError)
        completion(false)
    }
    })
        
        
        
        
        
        
//
//             // Use Alamofire to upload the imag
//             Alamofire.upload(
//                     multipartFormData: { multipartFormData in
//                            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
//                        multipartFormData.append(imageToUploadURL, withName: "image_name")
//                             for (key, val) in parameters {
//                                     multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
//                                 }
//                     },
//                     to: url,
//                     encodingCompletion: { encodingResult in
//                         switch encodingResult {
//                         case .success(let upload, _, _):
//                             upload.responseJSON { response in
//                                 if let jsonResponse = response.result.value as? [String: Any] {
//                                    print(jsonResponse)
//                                }
//                             }
//                         case .failure(let encodingError):
//                             print(encodingError)
//                         }
//                 }
//                 )
//
//
//
//
//
//         }
    
        
        
        
        
    }
    

    
    func upload_customized_img(img_data:Data,completion:@escaping CompletionHandler ) {
        
        // User "authentication":
        let parameters = ["user_id":authentication_services.instance.userid]
        
        // Image to upload:
        //let imageToUploadURL = img_url as URL
        // Server address (replace this with the address of your own server):
        let url = "\(UPLOAD_IMG)"
        Alamofire.upload(multipartFormData:{ multipartFormData in
            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            multipartFormData.append(img_data, withName: "image_name")
            
            //  multipartFormData.append(data, withName: "avatar", fileName: fileName!,mimeType: "image/jpeg")
            
            for (key, val) in parameters {
                multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                print(key)
                print(val)
            }
        },
                         usingThreshold:UInt64.init(),
                         to:UPLOAD_IMG,
                         method:.post,
                         headers:BEARER_HEADER_IMG,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    
                                    
                                    guard let response_data = response.data else {return}
                                    
                                    
                                    
                                    if response.result.error == nil {
                                        
                                        
                                        
                                        let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                                        print(json as Any)
                                        userdata_services.instance.img_upload_data = json as! [String : Any]
                                        
                                        
                                        
                                        
                                        completion(true)
                                        debugPrint(response)}
                                        
                                    else{
                                        
                                        completion(false)
                                        debugPrint(response)
                                        
                                        
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                completion(false)
                            }
        })
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func get_design_charge(completion:@escaping CompletionHandler ) {
        
        
        

        
        
        Alamofire.request(DESIGN_CHARGE, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.desing_charge_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
    }
    
    
    func view_notifications(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        Alamofire.request(VIEW_NOTIFICATION_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.notification_daata = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    func view_promotional_notifications(completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        Alamofire.request(VIEW_PROMOTIONAL_NOTIFICATION_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.promotional_notification_daata = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    func notification_status(status:String, completion:@escaping CompletionHandler ) {
        
        
        let body = ["status":status]
        
        
        
        Alamofire.request(NOTIFICATION_STATUS_URL, method: .get, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.notification_status_daata = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
        
        
        
    }
    
    
    
    func popular_dishes(completion:@escaping CompletionHandler ) {
        
        
        
        Alamofire.request("\(POPULAR_DISHES_URL)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.popular_dishes = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    func customization(id:String,completion:@escaping CompletionHandler ) {
       
        Alamofire.request("\(CUSTOMIZATION_URL)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.customization_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
//    func get_god_price_new(id:String,completion:@escaping CompletionHandler ) {
//        
//        
//        
//        let body = [
//            
//            
//            "is_default":"yes"
//            
//            
//        ]
//        
//        
//        
//        
//        Alamofire.request(GOLD_PRICE_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
//            
//            
//            
//            
//            
//            
//            
//            if response.result.error == nil {
//                guard let response_data = response.data else {return}
//                
//                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
//                print(json as Any)
//                userdata_services.instance.add_address_data1 = json as! [String : Any]
//                completion(true)
//                
//            }
//            else{
//                
//                
//                print(response)
//                
//                
//                completion(false)
//            }
//            
//        }
//        
//    }
    
    
    func payment_confirmation(id: String, completion:@escaping CompletionHandler ) {
        
        
        
        
        
        
        
        
        Alamofire.request("\(LAST_API_CALL)\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.payment_way = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
    }
    
   
    
    
    
    func new_otp_call(mobille:String, country:String,completion:@escaping CompletionHandler ) {
        
        let body:[String:Any] = [
            
            
            "mobile_no":mobille,
            "country_code_id":country

            
            
        ]
        
        Alamofire.request(CHANGE_NUMBER_OTP_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
               
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.new_OTP_DATE = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    




    func number_change(otp:String,completion:@escaping CompletionHandler ) {
    
    
    let body:[String:Any] =
    [
        "otp":otp

    ]

    
    
    
    
    
    Alamofire.request(Change_to_new_num_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
        
        if response.result.error == nil {
            
            guard let response_data = response.data else {return}
            
            let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
            print(json as Any)
            userdata_services.instance.mobile_change_data = json as! [String : Any]
            completion(true)
            
        }
        else{
            
            
            
            print(response)
            
            completion(false)
        }
        
    }
    
}



    
    func account_edit_main(name:String,email:String,id1:String,completion:@escaping CompletionHandler ) {
        
        
        let body:[String:Any] =
            [
                "name":name,
                "email":email
        ]
        
        
        
        
        
        
        Alamofire.request("\(ACCOUNT_EDIT_MAIN_URL)\(id1)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                
                print(authentication_services.instance.token)
                print(body)
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.ACCOUNT_EDIT_MAIN_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
   //change_pass_url
    
    func change_new_pass(cc_pass:String,new_pass:String,con_pass:String,completion:@escaping CompletionHandler ) {
        
        
        let body:[String:Any] =
           [
                "current_password": cc_pass,
                "new_password": new_pass,
                "confirm_password": con_pass

          ]
        
         Alamofire.request("\(change_pass_url)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.change_new_pass_DATA = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
//    testimonials_Url
    
    
    
    func testimonial(completion:@escaping CompletionHandler ) {
        
        
       
        Alamofire.request("\(testimonials_Url)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.testimonials_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    func timing(completion:@escaping CompletionHandler ) {
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        Alamofire.request("\(NEW_Time_Url)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.timing_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }


    
    
    
 func fcm_toke_n( device:String,value:String,completion:@escaping CompletionHandler ) {
    
    let body:[String:Any] = [
        "device": device,    // ios
        "value": value
    ]
    print(body)
      print(HEADER)
        print(fc_toke)
    
        Alamofire.request("\(fc_toke)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            print(response)
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.fcm_tk = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
 //device_info_url
    
    
    func device_information( model:String,model_number:String,os_version:String,app_version:String,completion:@escaping CompletionHandler ) {
        
        let body:[String:Any] = [
           
            "type":"ios",
            "modelName":model,
            "modelNumber":model_number,
            "osVersion":os_version,
            "appVersion":app_version

        ]
        print(body)
        print(HEADER)
        print(fc_toke)
        
        Alamofire.request("\(device_info_url)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_2HEADER).responseJSON{(response) in
            
            print(response)
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.device_info_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    func check_for_update(completion:@escaping CompletionHandler ) {
        
        let body:[String:Any] = [
            
            "app": "ios"
            
            
        ]
        
        Alamofire.request("\(check_update_url)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            print(response)
            if response.result.error == nil {
                
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.check_update_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                print(response)
                
                completion(false)
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    

}
