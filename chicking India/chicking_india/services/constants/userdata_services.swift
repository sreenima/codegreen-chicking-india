
import Foundation
import UIKit
class  userdata_services {
    static let instance = userdata_services()
    
    var fav_check = String()
    var offer_check = String()
    var rating_data = Dictionary<String,Any>()
    var register_data = Dictionary<String,Any>()
    var verify_otp_register_data = Dictionary<String,Any>()
    var login_data = Dictionary<String,Any>()
    var forgot_pwd_data = Dictionary<String,Any>()
    var forgot_reset_pwd_data = Dictionary<String,Any>()
    var banner_images = Dictionary<String,Any>()
    var food_categories_data = Dictionary<String,Any>()
    var list_fooditems_data = Dictionary<String,Any>()
    var banner_cake_images = Dictionary<String,Any>()
    var cake_categories_data = Dictionary<String,Any>()
    var list_cakeitems_data = Dictionary<String,Any>()
    var user_details_data = Dictionary<String,Any>()
    var user_profile_update = Dictionary<String,Any>()
    var cake_by_id = Dictionary<String,Any>()
    var show_delivery_charges = Dictionary<String,Any>()
    var show_packing_charges = Dictionary<String,Any>()

    var show_first_address_data = Dictionary<String,Any>()
    var list_all_address_data = Dictionary<String,Any>()
    var delete_address_data = Dictionary<String,Any>()
    var add_address_data = Dictionary<String,Any>()
    var add_address_data1 = Dictionary<String,Any>()
    var order_processing = Dictionary<String,Any>()
    var my_orders_data = Dictionary<String,Any>()
    var selected_address = Dictionary<String,Any>()
    var countrycode_data = Dictionary<String,Any>()
    var delivery_partners_data = Dictionary<String,Any>()
    var delivery_partners = NSArray()
    var alert = Bool()
    var check_fav = String()
    var ckeorfood = String()
    var edit_address_data = Dictionary<String,Any>()
    var logout_data = Dictionary<String,Any>()
    var total = Double()
    var registered_user_otp_data = Dictionary<String,Any>()
    var promocode_data = Dictionary<String,Any>()
    var show_ciities_data  =  Dictionary<String,Any>()
    var show_branch_data = Dictionary<String,Any>()
    var order_detail_data = Dictionary<String,Any>()
    var get_size_data = Dictionary<String,Any>()
    var get_base_data = Dictionary<String,Any>()
    var get_topings_data = Dictionary<String,Any>()
    var img_upload_data = Dictionary<String,Any>()
    var desing_charge_data = Dictionary<String,Any>()
    var notification_daata = Dictionary<String,Any>()
    var promotional_notification_daata = Dictionary<String,Any>()
    var notification_status_daata = Dictionary<String,Any>()
    var popular_dishes = Dictionary<String,Any>()
    var customization_data = Dictionary<String,Any>()
    var skip_to_login = false
    var area_id = NSInteger()
    var ongoing_order_data = Dictionary<String,Any>()
    var past_order_data = Dictionary<String,Any>()
    var favorite_order_data = Dictionary<String,Any>()
    var no_address = String()
    var make_address = String()
    var direct_new_address = String()
    var data1 = Dictionary<String,Any>() //for map
    var the_way = String()
    var payment_way = Dictionary<String,Any>()
    var cart = NSMutableArray()
    var new_OTP_DATE = Dictionary<String,Any>()
    var side_btn_needed = String()
    var mobile_change_data = Dictionary<String,Any>()
    var USER_NAME = String()
    var USER_MOBILE = String()
    
    var ACCOUNT_EDIT_MAIN_data = Dictionary<String,Any>()
    var change_new_pass_DATA = Dictionary<String,Any>()
    var testimonials_data = Dictionary<String,Any>()
    
    var timing_data = Dictionary<String,Any>()
    
    var fcm_tk = Dictionary<String,Any>()
    
    var FCM_Clicked_TYPE = "any"
    
    var oder_detail_id = String()
    
    var address_from_position = String()
    
    var device_info_data = Dictionary<String,Any>()
    
    var check_update_data = Dictionary<String,Any>()
    
    var from_cart_store = String()
    //forcart
    var quantity = "1"
    
    var attributes_count = NSArray()
    
    var product:[String:Any] = [
        
                               "product_id" : "",
                               "quantity" :  "",
                               "title":"",
                               "description":"",
                               "image":"",
                               "price":"",
                               "order_item_addon":[],
                               "order_item_list": []

                                ]
    var order_item_addon:[String:Any] = [
        "addon_id" : "",
        "quantity":""
    ]
    
    var order_item_list:[String:Any] = [
    
        "item_list_id" : "",
        "quantity" :  "",
        "order_item_list_attributes" :[]
    
    ]
    
    var  order_item_list_attributes:[String:Any] = [
    "item_list_attribute_id" : "",
    "quantity" :  "",
    "order_item_list_attribute_values" : []
      
    ]
    
    var  order_item_list_attribute_values:[String:Any] = [
        "item_list_attribute_value_id" : "",
        "quantity" :  ""
    
    ]
 
    var order_details : Dictionary = [
    
        "payment_option": "",
        "source":"ios",
        "transaction_type":"",
        "outlet_code":"",
        "pick_up_time":"",
        "user_address_id": "",
        "promo_code_id": "",
        "promo_code_value": "",
        "total_price" : "",
        "is_favorite" : "",
        "order_items": NSArray()
        
        ] as [String : Any]
    var firebase_token = String()
    var ready_img = UIImage()
    var custom_img = UIImage()
    var size = String()
    func logout() {
         authentication_services.instance.cart_session = NSArray()
        // firebase_token = String()
         register_data = Dictionary<String,Any>()
         verify_otp_register_data = Dictionary<String,Any>()
         login_data = Dictionary<String,Any>()
         forgot_pwd_data = Dictionary<String,Any>()
         forgot_reset_pwd_data = Dictionary<String,Any>()
         banner_images = Dictionary<String,Any>()
         food_categories_data = Dictionary<String,Any>()
         list_fooditems_data = Dictionary<String,Any>()
         banner_cake_images = Dictionary<String,Any>()
         cake_categories_data = Dictionary<String,Any>()
         list_cakeitems_data = Dictionary<String,Any>()
         user_details_data = Dictionary<String,Any>()
         user_profile_update = Dictionary<String,Any>()
         cake_by_id = Dictionary<String,Any>()
         show_delivery_charges = Dictionary<String,Any>()
         show_first_address_data = Dictionary<String,Any>()
         list_all_address_data = Dictionary<String,Any>()
         delete_address_data = Dictionary<String,Any>()
         add_address_data = Dictionary<String,Any>()
         order_processing = Dictionary<String,Any>()
         my_orders_data = Dictionary<String,Any>()
        selected_address = Dictionary<String,Any>()
         countrycode_data = Dictionary<String,Any>()
        
        
        
        
        authentication_services.instance.logout(completion: {_ in })
        
        authentication_services.instance.isloggedin = false
        authentication_services.instance.token = ""
        authentication_services.instance.userid = ""
        authentication_services.instance.mobile = ""
        //authentication_services.instance.cart = NSMutableArray()
        
         total = Double()
        
//         order_details : Dictionary = [
//            
//            "payment_option": "",
//            "user_address_id": "",
//            "total_price" : "",
//            
//            "order_items": NSArray()
//            ] as [String : Any]
        
        
        
    }
    
    
}
