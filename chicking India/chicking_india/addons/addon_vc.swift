//
//  addon_vc.swift
//  chicking
//
//  Created by CGT on 16/11/18.
//  Copyright © 2018 Ios CGT. All rights reserved.
//

import UIKit

class addon_vc: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return all_data.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! addon_cell
        
        let dat = all_data[indexPath.row] as! Dictionary<String,Any>
        
        cell.count_lbl.text = "\(counts[indexPath.row])"
        cell.price_lbl.text = "\(Double("\(counts[indexPath.row])")! *  Double("\(dat["price"]!)")!) INR"
        cell.add_btn.tag = indexPath.row
        cell.minus_btn.tag = indexPath.row
        
        
        var details = dat["addon_details"] as! Dictionary<String,Any>
        
        
        
        cell.title_lbl.text = "\(details["title"]!)"
        
       
        if cell.count_lbl.text == "0"{
            
            cell.price_lbl.text = "\(dat["price"]!) INR"}
        
        
        
        return cell
        
    }
    
    @IBAction func decriment(_ sender: UIButton) {
        
        var c = Int("\(counts[sender.tag])")!
        if c > 0{
            
            c = c - 1
            counts.replaceObject(at: sender.tag, with: "\(c)")
            
            
            
        }
        print(counts)
        self.tab.reloadData()
        
        
    }
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    
    @IBAction func increment(_ sender: UIButton) {
        
        
        
        
        var c = Int("\(counts[sender.tag])")!
        
        
        c = c + 1
        counts.replaceObject(at: sender.tag, with: "\(c)")
        
        
        
        
        self.tab.reloadData()
        
        
        
    }
    
    @IBAction func continue_action(_ sender: UIButton) {
        
        
        
        let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
        
        var img_name = ( "\(dat["image"]!)").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        userdata_services.instance.product = [
            
            "product_id" : "\(dat["id"]!)",
            "quantity" :  "\(userdata_services.instance.quantity)",
            "title":"\(dat["title"]!)",
            "description":"\(dat["description"]!)",
            "image":"\(img_name)",
            "price":"\(self.total_price)",
            "order_item_addon":[],
            "order_item_list": []
            
        ]
      
        let order_item_addon = NSMutableArray()
        print(all_data)
        print(all_data.count)
        for i in 0...(all_data.count - 1){
            
            if    "\(counts[i])" != "0"{
                
                
                var data_at_i = all_data[i] as! [String:Any]
                
                if "\(counts[i])" != "0"{
                    self.total_price = total_price + (Double("\(data_at_i["price"]!)")! * Double("\(counts[i])")!)
                    order_item_addon[order_item_addon.count] = [ "addon_id" : "\(data_at_i["id"]!)",
                        "quantity":"\(counts[i])"] as [String:Any]
                    
                }
                
                
            }
            
        }
        userdata_services.instance.product["order_item_addon"] = order_item_addon
 
        let order_item_list = NSMutableArray()
        let item_list = dat["item_list"] as! NSArray
        
        
        for i1 in 0...(item_list.count - 1){
            if userdata_services.instance.attributes_count.count-1 >= i1{
                
                let item = item_list[i1] as! Dictionary<String,Any>
                
                
                
                var c1 = userdata_services.instance.attributes_count[i1] as! NSArray
                
                
                let item_attributes = item["item_attributes"] as! NSArray
                if item_attributes.count > 0{
                    let order_item_list_attributes = NSMutableArray()
                    for i3 in 0...(item_attributes.count-1){
                        var c2 = c1[i3] as! NSArray
                        var attributes = item_attributes[i3] as! Dictionary<String,Any>
                        
                        
                        
                        let attribute_values = attributes["attribute_values"] as! NSArray
                        
                   
                        
                        let order_item_list_attribute_values = NSMutableArray()
                        if attribute_values.count > 0{
                            
                            for i2 in 0...(attribute_values.count-1){
                                var c3 = c2[i2] as! String
                                var  attribute_value_particular = attribute_values[i2] as! Dictionary<String,Any>
                                let attribute_counts = userdata_services.instance.attributes_count[i1] as! NSArray
                                if "\(c3)" != "0"{
                                    
                                    self.total_price = self.total_price + (Double("\(attribute_value_particular["price"]!)")! * Double("\(c3)")!)
                                    
                                    order_item_list_attribute_values[order_item_list_attribute_values.count] = [
                                        
                                        "item_list_attribute_value_id" : "\(attribute_value_particular["id"]!)",
                                        "quantity" :  "\(c3)"
                                        
                                    ]
                                    
                                }
                                
                            }
                            if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                                
                                order_item_list_attributes[order_item_list_attributes.count] = ["item_list_attribute_id" : "\(attributes["id"]!)",
                                    "quantity" : "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)"  ,
                                    
                                    "order_item_list_attribute_values" : order_item_list_attribute_values
                                    
                                    
                                ]
                            }
                   
                        }
                    }
                    
                    if "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)" != "0"{
                        order_item_list[order_item_list.count] = [
                            
                            "item_list_id" : "\(item["id"]!)",
                            "quantity" :  "\(Int("\(item["qty"]!)")! * Int(userdata_services.instance.quantity)!)",
                            "order_item_list_attributes" :order_item_list_attributes //[]
                            
                            ] as Dictionary<String,Any>
                    }
                    
                    
                }
                
            }
       
            
        }
        
        userdata_services.instance.product["order_item_list"] = order_item_list
        
        userdata_services.instance.product["price"] = "\(self.total_price)"

        
        
        print("my product list : \(userdata_services.instance.product)")
        
        userdata_services.instance.cart[userdata_services.instance.cart.count] = userdata_services.instance.product
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "carto") as! cart_ViewController
        
        
        self.present(newViewController, animated: false, completion: nil)
        
        
        
    }
    @IBOutlet var tab: UITableView!
    var all_data = NSArray()
    var counts = NSMutableArray()
    var total_price = 0.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindowLevelAlert + 1
            let controller = UIAlertController(title: "\nNo Internet Detected !", message: "\n\n\n\n\nMake sure your device is connected to the internet.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            var height:NSLayoutConstraint = NSLayoutConstraint(item: controller.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 230)
            controller.view.addConstraint(height)
            controller.view.tintColor = UIColor.red
            
            var imageView = UIImageView(frame: CGRect(x: 55, y: 20, width: 170, height: 170))
            imageView.image = #imageLiteral(resourceName: "no internet")
            
            controller.view.addSubview(imageView)
            
            controller.addAction(ok)
            
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(controller, animated: true, completion:nil)
            
            
        }
        
        
        tab.rowHeight = UITableViewAutomaticDimension
        let dat  = userdata_services.instance.cake_by_id["data"] as! Dictionary<String,Any>
        
        self.total_price = total_price + (Double("\(dat["price"]!)")! * Double(userdata_services.instance.quantity)!)
        
        
        if let addon = dat["addons"] as? NSArray{
            
            all_data = addon
           
            for i in 0...all_data.count-1{
                
                counts[i] = "0"
                
                
            }
        
            tab.reloadData()
        }
        else{
            
            
        }
    }
 
}


//table view cell


class addon_cell: UITableViewCell {
    
    
    @IBOutlet var title_lbl:UILabel!
    @IBOutlet var price_lbl:UILabel!
    @IBOutlet var add_btn:UIButton!
    @IBOutlet var minus_btn:UIButton!
    @IBOutlet var count_lbl:UILabel!
 
}


