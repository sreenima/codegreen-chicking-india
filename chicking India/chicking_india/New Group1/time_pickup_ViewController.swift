//
//  time_pickup_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 23/09/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class time_pickup_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet var txt_height: NSLayoutConstraint!
    
    @IBOutlet var tbl_height: NSLayoutConstraint!
    @IBOutlet var bg_view: UIView!
    
    @IBOutlet var BIG_view: UIView!
    
    @IBOutlet var tbl: UITableView!
    @IBOutlet var time_pick_txt: UITextField!
    
    @IBOutlet var continue_clicked: UIButton!
    @IBOutlet var view_height: NSLayoutConstraint!
    var condition_time = TimeInterval()
    var check_date = TimeInterval()
    var condition_time2 = TimeInterval()
    
  
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var txt_hight: NSLayoutConstraint!
    var Opening_Time_Stamp = TimeInterval()
    var Opening_Time = TimeInterval()
    var Closing_Time_Stamp = TimeInterval()
    var Closing_Time = TimeInterval()
    var time_array = NSMutableArray()
    var modi_time_array = NSMutableArray()
    var printable_array = NSMutableArray()
    var type_of_delivery = String()
    var incresed_height = CGFloat()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        BIG_view.layer.cornerRadius = 15.0
        BIG_view.layer.shadowColor = UIColor.black.cgColor
        BIG_view.layer.shadowOpacity = 0.2
        BIG_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        BIG_view.layer.shadowRadius = 3
        
         continue_clicked.layer.cornerRadius = 8
         time_pick_txt.layer.borderColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
         time_pick_txt.layer.borderWidth = 0.5
         time_pick_txt.layer.cornerRadius = 8
         txt_hight.constant = 45
        
        bg_view.layer.cornerRadius = 8
        
        bg_view.layer.shadowColor = UIColor.black.cgColor
        bg_view.layer.shadowOpacity = 0.2
        bg_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        bg_view.layer.shadowRadius = 3
        
        let dateFormatter = DateFormatter()
        let amFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        amFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        amFormatter.dateFormat = "a"
        
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        let dateInFor4 = dateFormatter.date(from: dateInFormat)!
        
        
        self.check_date = dateInFor4.timeIntervalSince1970
        
        let amInFormat = amFormatter.string(from: NSDate() as Date)
        
        
        print(check_date)
        print( dateInFormat)
        print(amInFormat)
        
     self.spinner.startAnimating()
        
        authentication_services.instance.timing(){ (success) in
            self.spinner.stopAnimating()
            if success {
                
                if userdata_services.instance.timing_data["error"] == nil{
                    
                    if userdata_services.instance.timing_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.timing_data["data"] as! Dictionary<String,Any>
                        
                        //   self.all_orders = dat
                        print(userdata_services.instance.timing_data)
                        print(dat)
                        
                        
                        
                        self.condition_time = TimeInterval()
                        self.condition_time2 = TimeInterval()
                        
                        let t1 = dat["storeOpen"] as! String
                        let t2 = dat["storeClose"] as! String                 
                        let da = DateFormatter()
                        
                        da.timeZone = TimeZone.current
                        da.locale = Locale(identifier: "en-US")
                        
                        da.dateFormat = "hh:mm a"
                        
                        
                        if let dateInFormatt = da.date(from: t1){
                            let pure_date1 = da.string(from:dateInFormatt as Date)
                            
                            let dateInFor = da.date(from: pure_date1)!
                            
                            self.condition_time = dateInFor.timeIntervalSince1970
                            //////////////////////////////////////////////////////////////
                            let da2 = DateFormatter()
                            
                            da2.timeZone = TimeZone.current
                            da2.locale = Locale(identifier: "en-US")
                            
                            da2.dateFormat = "hh:mm a"
                            
                            let dateInFormattt = da2.date(from: t2)!
                            let pure_date2 = da2.string(from:dateInFormattt as Date)
                            
                            let dateInForr = da2.date(from: pure_date2)!
                            
                            self.condition_time2 = dateInForr.timeIntervalSince1970
                            
                            let texx = "Next Available At \(t1 )"
                            
                            print(dateInFormat)
                        
                            
                            //*************************************//
                            
                            
                // OPENING TIME INTERVAL
                            
                            print(self.condition_time)
                            
                            let minutes: TimeInterval = 30 * 60
                            let OpeningPlusOne = self.condition_time + minutes
                            
                            print(OpeningPlusOne)
                            
                            self.Opening_Time_Stamp = OpeningPlusOne
                            
                                let date = Date(timeIntervalSince1970: OpeningPlusOne)
                                let dateFormatter = DateFormatter()
                                dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                                dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                                dateFormatter.timeZone = .current
                                dateFormatter.dateFormat = "hh:mm a"
                                let One_hr_after_opening = dateFormatter.string(from: date)
                            
                         
                            
                            print(One_hr_after_opening)
                         
                            
                   
                            //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                            
                            
                //CURRENT TIME INTERVAL
                            
                            
                            let minutes2: TimeInterval = 30 * 60
                            let dateAfterHour = self.check_date + minutes2
                            
                            print(dateAfterHour)
                            
                            
                            
                            
                            //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                            
                //CLOSING TIME INTERVAL
                            
                            print(self.condition_time2)
                            
                            let mminutes: TimeInterval = 30 * 60
                            let closingPlusOne = self.condition_time2 //+ mminutes
                            
                            print(closingPlusOne)
                            
                            self.Closing_Time_Stamp = closingPlusOne
                            
                            let date2 = Date(timeIntervalSince1970: closingPlusOne)
              
                            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                            dateFormatter.timeZone = .current
                            dateFormatter.dateFormat = "hh:mm a"
                            let One_hr_after_Closing = dateFormatter.string(from: date)
                            
                            
                            
                            print(One_hr_after_Closing)
                            
                            
                            
                            
                            
                            //*************************************//
                            
                           var incre = OpeningPlusOne
                                var i = 0
                                while( incre <= closingPlusOne){
                                    
                                    self.time_array[i] = incre
                                
                                    i = i + 1
                                   incre = incre + minutes
                                }
                             
                            
                            //************************************//
                            
                            let a:Int? = Int(dateInFormat)
                            if Int(self.condition_time) <= Int(self.check_date) && Int(self.check_date) <= Int(self.condition_time2){
                                
                                
                               
                                var incremnt = self.check_date
                                
                                var o = 0
                                for i in 0...self.time_array.count-1{
                                    
                                    if (self.time_array[i] as! TimeInterval >= self.check_date){
                                        
                                        self.modi_time_array[o] = self.time_array[i] as! TimeInterval
                                        
                                        o = o + 1
                                    }
                                    
                                }
                                var p = 0
                                for i in 0...self.modi_time_array.count-1 {
                                    
                                    
                                    let time1 = Date(timeIntervalSince1970: incremnt)
                                    let time2 = Date(timeIntervalSince1970: (self.modi_time_array[i] as! TimeInterval))
                                    let difference = Calendar.current.dateComponents([.minute], from: time1, to: time2)
                                    let duration = difference.minute
                                    
                                    if duration! >= 30{
                                        
                                        self.printable_array[p] = self.modi_time_array[i] as! TimeInterval
                                        
                                        p = p+1
                                    }
                                    
                                }
                                   
                                self.tbl.reloadData()
                                
                            }else{
                                
                                
                                
                            }
                            
                            
                        }else{
                            
                            
                            self.viewDidLoad()
                            
                        }
                        
                        
                    }
                    else{
                     self.spinner.stopAnimating()
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.timing_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
                
            }
                
                
            else{
                
            self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
            
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return printable_array.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! time_cell
        
        let date2 = Date(timeIntervalSince1970: printable_array[indexPath.row] as! TimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "hh:mm a"
        let One_hr_after_Closing = dateFormatter.string(from: date2)
        
        print(One_hr_after_Closing)
        
        cell.time_lbl.text = One_hr_after_Closing
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let date2 = Date(timeIntervalSince1970: printable_array[indexPath.row] as! TimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "hh:mm a"
        let One_hr_after_Closing = dateFormatter.string(from: date2)
        
        print(One_hr_after_Closing)
        
       
        
        self.time_pick_txt.text =  One_hr_after_Closing
        
        
        if  self.tbl_height.constant == 0{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                
                if 400 >  CGFloat((32 * self.printable_array.count)){
                    self.tbl_height.constant = CGFloat((32 * self.printable_array.count))
                    self.view_height.constant = CGFloat((32 * self.printable_array.count))
                    
                }else{
                    
                    self.tbl_height.constant = 400
                    self.view_height.constant = 400
                }
               
                self.view.layoutIfNeeded()
            })
            
        }else{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tbl_height.constant = 0
                self.view_height.constant = 0
                
                
                self.view.layoutIfNeeded()
            })
            
            
        }
        
        
        if  self.txt_height.constant == 200{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.txt_height.constant = 40
                self.view.layoutIfNeeded()
            })
            
        }else{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.txt_height.constant = 200
                
                
                self.view.layoutIfNeeded()
            })
            
            
        }
        
        
    }
    
    
    
    
    
    
    @IBAction func time_pick_click(_ sender: Any) {
        
        
        if  self.tbl_height.constant == 0{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                
                if 400 >  CGFloat((32 * self.printable_array.count)){
                    self.tbl_height.constant = CGFloat((32 * self.printable_array.count))
                    self.view_height.constant = CGFloat((32 * self.printable_array.count))
                    
                }else{
                    
                    self.tbl_height.constant = 400
                    self.view_height.constant = 400
                }
                
               
                print("HEIGHT:- \(self.tbl_height.constant)")
                self.view.layoutIfNeeded()
            })
            
        }else{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tbl_height.constant = 0
                self.view_height.constant = 0
                
              
                self.view.layoutIfNeeded()
            })

            
        }
        
        
        if  self.txt_height.constant == 200{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.txt_height.constant = 40
                self.view.layoutIfNeeded()
            })
            
        }else{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.txt_height.constant = 200
                self.view.layoutIfNeeded()
            })
            
            
        }
        
        
    }
    
    @IBAction func back_clicked(_ sender: Any) {
        
    dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func continue_clicked(_ sender: Any) {
        
        if self.time_pick_txt.text != ""{
      
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order") as! order_customizeViewController
        
     
        newViewController.pickup_time = self.time_pick_txt.text!
        newViewController.type_of_delivery = "\(self.type_of_delivery)"
        
    userdata_services.instance.order_details["pick_up_time"] =  self.time_pick_txt.text
        
        self.present(newViewController, animated: false, completion: nil)
        }else{
            
            let alert2 = UIAlertController(title: "", message: "Choose a pickup time to continue. ", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


class time_cell : UITableViewCell{
    
    
    @IBOutlet var time_lbl: UILabel!
    
}
