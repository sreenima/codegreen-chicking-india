//
//  pick_up_stores_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/09/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class pick_up_stores_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var selected_city = "0"
    @IBOutlet var city_vw: UIView!
    @IBOutlet var city_vw_height: NSLayoutConstraint!
    
   
  
    @IBOutlet var city_view: UIView!
    @IBOutlet var city_lbl: UILabel!
    @IBOutlet var drop_down_btn: UIButton!
    @IBOutlet var tab_city: UITableView!
    @IBOutlet var main_tab: UITableView!
    @IBOutlet var tab_height: NSLayoutConstraint!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    var cities = NSArray()
    var branches = NSArray()
    var titleArray = [String]()
    var Company_Id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.city_vw.isHidden = true
         self.city_vw.layer.cornerRadius = 15.0
         self.city_vw.layer.shadowColor = UIColor.black.cgColor
         self.city_vw.layer.shadowOpacity = 0.2
         self.city_vw.layer.shadowOffset = CGSize(width: -1, height: 1)
         self.city_vw.layer.shadowRadius = 3
        city_lbl.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        city_lbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        city_view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        city_view.layer.borderWidth = 0.5
        city_view.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        city_view.layer.cornerRadius = 8
        city_view.clipsToBounds = true
        
      //  ShowCities
         self.spinner.startAnimating()
        authentication_services.instance.cities_List(){ (success) in
            if success {
               self.spinner.stopAnimating()
                if userdata_services.instance.show_ciities_data["error"] == nil{
                    
                    if userdata_services.instance.show_ciities_data["status"]  as! String == "ok"{
                        
                        let dat  = userdata_services.instance.show_ciities_data["data"] as! NSArray
                        
                        print("show_ciities_data = \(dat)")
                        self.cities = dat
                     
                        self.tab_city.reloadData()
                        
                    }
                    else{
                        self.spinner.stopAnimating()
                        
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_ciities_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
                self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        }
        
       self.spinner.startAnimating()
// to be reverted
       /* authentication_services.instance.show_branches(cityid: "0"){ (success) in
            if success {
                self.spinner.stopAnimating()
                if userdata_services.instance.show_branch_data["error"] == nil{
                    
                    if userdata_services.instance.show_branch_data["status"]  as! String == "ok"{
                        userdata_services.instance.data1 = userdata_services.instance.show_branch_data
                        
                        let dat  = userdata_services.instance.show_branch_data["data"] as! NSArray
                             print(dat)
                             self.branches = dat
                       
                        self.main_tab.reloadData()
               
                    }
                    else{
                     self.spinner.stopAnimating()
                     
                        let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_branch_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    userdata_services.instance.logout()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")
                    
                    //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String
                    
                    self.present(newViewController, animated: false, completion: nil)
                    
                    
                }
                
            }
            else{
             self.spinner.stopAnimating()
                let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
                
            }
            
        } */
        

        // Do any additional setup after loading the view.
    }
    
    
  
    
        func selected_branches() {

            self.titleArray.removeAll()

            self.spinner.startAnimating()
            authentication_services.instance.branches_list(cityid: selected_city){ (success) in
            if success {
            self.spinner.stopAnimating()
            if userdata_services.instance.show_branch_data["error"] == nil{

            if userdata_services.instance.show_branch_data["status"]  as! String == "ok"{
            userdata_services.instance.data1 = userdata_services.instance.show_branch_data

            let dat  = userdata_services.instance.show_branch_data["data"] as! NSArray
            print(dat)
            self.branches = dat
            print("branch_List\(self.branches)")
             
            self.main_tab.reloadData()



//            if self.branches != nil{
//
//                        let titleStr = self.branches["title"]as? String
//                        print("titleStr = \(titleStr)")
//                        self.titleArray.append(titleStr!)
//                let CompanyId = self.branches["company_id"] as? Int
//                print("Company_Id = \(CompanyId)")
//
//                UserDefaults.standard.set(CompanyId, forKey: "companyID")
//
//
//                }
//                print("titleArray = \(self.titleArray),titleArray.count = \(self.titleArray.count)")


            self.main_tab.reloadData()

            }
            else{
            self.spinner.stopAnimating()

            let alert2 = UIAlertController(title: "", message: "\(userdata_services.instance.show_branch_data["message"]  as! String)", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)
            }
            }


            else{
            userdata_services.instance.logout()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "log_in")

            //newViewController.userid = userdata_services.instance.register_data["user_id"] as! String

            self.present(newViewController, animated: false, completion: nil)


            }

            }
            else{
            self.spinner.stopAnimating()
            let alert2 = UIAlertController(title: "", message: "Network Problem.", preferredStyle: UIAlertControllerStyle.alert)
            alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert2, animated: true, completion: nil)

            }

            }



            }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             if tableView == tab_city {
                 return cities.count
             }else{
                return branches.count
             }
         }
      
      
      
      
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          

              if tableView != main_tab{
              self.city_lbl.textColor = .white
              let dat = cities[indexPath.row] as! Dictionary<String,Any>
              print("cities list = \(dat)")
              self.city_lbl.text = "\(dat["title"]!)"
              self.city_lbl.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)

              //        let lat = "\(dat["latitude"]!)"
              //        let lon = "\(dat["longitude"]!)"

              self.city_vw.isHidden = true
              UIView.animate(withDuration: 0.2, animations: { () -> Void in
              self.tab_height.constant = 0
              self.city_vw_height.constant = 0
              self.view.layoutIfNeeded()
              })

             self.selected_city = "\(dat["id"]!)"
             print("bid = \(selected_city)")
                
             self.selected_branches()





              
          }else{
              
//              print(branches)

            let cc = branches[indexPath.row] as! Dictionary<String,Any>
            print("cc = \(cc)")
            let code = "\(cc["outlet_code"]!)"
            print("code = \(code)")
            userdata_services.instance.order_details["outlet_code"] = code
            let CompanyId = cc["company_id"] as? String
            print("Company_Id = \(CompanyId)")
            
            UserDefaults.standard.set(CompanyId, forKey: "companyID")
            
                
            

            //        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order") as! order_customizeViewController
            //
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "time_pick") as! time_pickup_ViewController
            let indexPath = tableView.indexPathForSelectedRow
            let currentCell = tableView.cellForRow(at: indexPath!) as! pick_up_cell

            newViewController.type_of_delivery = "\(currentCell.lbl.text!)"

            self.present(newViewController, animated: false, completion: nil)
            }
}
      
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == main_tab {
        return 15
        }else{

        return 0

        }
       
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tab_city{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! showonmap_cell
        
        let dat = cities[indexPath.row] as! Dictionary<String,Any>
        cell.lbl.text = "\(dat["title"]!)"
        
        return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! pick_up_cell
            
            cell.vw.layer.cornerRadius = 8
            cell.vw.layer.borderWidth = 1
            cell.vw.layer.borderColor = UIColor.clear.cgColor
            cell.vw.layer.masksToBounds = true
            cell.vw.layer.shadowColor = UIColor.black.cgColor
            cell.vw.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.vw.layer.shadowRadius = 3.0
            cell.vw.layer.shadowOpacity = 0.2
            cell.vw.layer.masksToBounds = false
            cell.vw.layer.backgroundColor = UIColor.white.cgColor
            
            let dat = branches[indexPath.row] as! Dictionary<String,Any>
            
//
//            d2[0] = d["latitude"]
//            d2[1] = d["longitude"]
//            d2[2] = d["title"]
//            d2[3] = d["address"]
//            d2[4] = d["mbile"]
            
            print("branch = \(self.branches)")
            print("branchCount = \(branches.count)")
            
            
            let st = "\(dat["address"]!)".htmlToString
            print("st = \(st)")
            
            cell.lbl.text = "\(dat["title"]!), \(st)"
            print("\(dat["title"]), \(st)")
            
            return cell
            
        }
    }
    
    
    @IBAction func dropdown(_ sender: UIButton) {
        print("dropdown")
        if  self.tab_height.constant == 0{
            
             self.city_vw.isHidden = false
        
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 189
                self.city_vw_height.constant = 189
                self.view.layoutIfNeeded()
            })
            
        }else{
        
            self.city_vw.isHidden = true
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.tab_height.constant = 0
                 self.city_vw_height.constant = 0
                self.view.layoutIfNeeded()
                
            })
            
            
            
            
        }
        
        
        
    }
    @IBAction func back_cliekd(_ sender: Any) {
        
        
        self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


class pick_up_cell: UITableViewCell{
    
    
    @IBOutlet var lbl: UILabel!
    @IBOutlet var vw: UIView!
    
}
