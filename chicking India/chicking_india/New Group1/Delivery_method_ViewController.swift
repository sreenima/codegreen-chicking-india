//
//  Delivery_method_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 21/09/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit

class Delivery_method_ViewController: UIViewController {
    
    @IBOutlet var pick_from_store: UIButton!
    
    @IBOutlet var home_delivery: UIButton!
    @IBOutlet var continue_btn: UIButton!
    
    @IBOutlet var back_button: UIButton!
    @IBOutlet var bg_view: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userdata_services.instance.from_cart_store == "yes"{
           
            back_button.isHidden = false
        }
        bg_view.layer.cornerRadius = 15.0
        bg_view.layer.shadowColor = UIColor.black.cgColor
        bg_view.layer.shadowOpacity = 0.2
        bg_view.layer.shadowOffset = CGSize(width: -1, height: 1)
        bg_view.layer.shadowRadius = 3

         let userdefaults = UserDefaults.standard
        
        continue_btn.layer.cornerRadius = 8
        
        if let savedValue = userdefaults.value(forKey: DELIVERY_TYPE){
            print(savedValue)
            if authentication_services.instance.Delivery_type == "home" {
                
                home_delivery.setImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
                
                pick_from_store.setImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
                
                authentication_services.instance.Delivery_type = "home"
                
                print(authentication_services.instance.Delivery_type)
                
            }else{
                
                pick_from_store.setImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
                home_delivery.setImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
                authentication_services.instance.Delivery_type = "store"
                
                print(authentication_services.instance.Delivery_type)
            }
            
        }else{
            
            pick_from_store.setImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
            home_delivery.setImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
            authentication_services.instance.Delivery_type = "store"
            
            print(authentication_services.instance.Delivery_type)
            
            
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn_clicked(_ sender: Any) {
        
    self.dismiss(animated: false, completion: nil)
        
        
    }
    
    @IBAction func pick_up_from_store_clicked(_ sender: Any) {
        
        pick_from_store.setImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
         home_delivery.setImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
         authentication_services.instance.Delivery_type = "store"
        
        print(authentication_services.instance.Delivery_type)
        
         userdata_services.instance.order_details["transaction_type"] = "takeaway"
        
      
    }
    
    @IBAction func home_delivery_clicked(_ sender: Any) {
        
        
        home_delivery.setImage(#imageLiteral(resourceName: "Group 449"), for: .normal)
        
         pick_from_store.setImage(#imageLiteral(resourceName: "Ellipse 10"), for: .normal)
        
        authentication_services.instance.Delivery_type = "home"
        
          print(authentication_services.instance.Delivery_type)
        
        userdata_services.instance.order_details["transaction_type"] = "delivery"
    }
    
    
    
    @IBAction func continue_clicked(_ sender: Any) {
        
        
        if authentication_services.instance.Delivery_type == "store"{
        
        print(authentication_services.instance.Delivery_type)
        
        userdata_services.instance.order_details["transaction_type"] = "takeaway"
        }
        if userdata_services.instance.from_cart_store == "yes"{
            
            if authentication_services.instance.Delivery_type == "store"{
            
            let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard1.instantiateViewController(withIdentifier: "pick_up_store") 
            
            self.present(newViewController, animated: false, completion: nil)
            }else{
                
                
                let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard1.instantiateViewController(withIdentifier: "order") as! order_customizeViewController
               
                self.present(newViewController, animated: false, completion: nil)
                
            }
        }else{
        
        
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "log_in") as! log_in_ViewController
        
        self.present(newViewController, animated: false, completion: nil)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
