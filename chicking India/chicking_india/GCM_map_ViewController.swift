//
//  GCM_map_ViewController.swift
//  chicking
//
//  Created by Ios CGT on 08/10/19.
//  Copyright © 2019 Ios CGT. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker

class GCM_map_ViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet var bg_view: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet var bg_button_search: UIButton!
    
    var latestDirection: Int = 0
    var translation: CGPoint!
    var startPosition: CGPoint! //Start position for the gesture transition
    var originalHeight: CGFloat = 0 // Initial Height for the UIView
    var difference: CGFloat!
    
    var translation1: CGPoint!
    var startPosition1: CGPoint! //Start position for the gesture transition
    var originalHeight1: CGFloat = 0 // Initial Height for the UIView
    var difference1: CGFloat!
    
    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    
    
    var placesClient: GMSPlacesClient!
    var likeHoodList: GMSPlaceLikelihoodList?
    private let locationManager = CLLocationManager()
     var tap = UITapGestureRecognizer()
    var Add_array = NSMutableArray()
    
    var lbl_address: AnyObject?
    
    var addr = NSString()
    var addr_pos = NSString()
    var no_ad = NSString()
    var from_edit = String()
    var check = String()
    
    @IBOutlet var tbl_height: NSLayoutConstraint!
    @IBOutlet var table_view: UITableView!
    
    @IBOutlet var tbl_height_g: NSLayoutConstraint!
    
    
    
    @IBAction func viewDidDragged(_ sender: UIPanGestureRecognizer) {
        
        
        if sender.state == .began {
            startPosition = gestureRecognizer.location(in: bg_view) // the postion at which PanGestue Started
             startPosition1 = gestureRecognizer.location(in: table_view)
        }
        
        if sender.state == .began || sender.state == .changed {
            let velocity = sender.velocity(in: bg_view)
            var currentDirection: Int = 0
            
            if velocity.y > 0 {
                print("panning down")
                currentDirection = 2
                
            } else {
                print("panning up")
                 currentDirection = 1
            }
           
            if bg_view.frame.height >= 72.0 && bg_view.frame.height <= (self.view.frame.height / 1.5){
            
            translation = sender.translation(in: self.view)
             translation1 = sender.translation(in: self.view)
            sender.setTranslation(CGPoint(x: 0.0, y: 0.0), in: self.view)
            let endPosition = sender.location(in: bg_view) // the posiion at which PanGesture Ended
             let endPosition1 = sender.location(in: table_view)
            difference = endPosition.y - startPosition.y
            difference1 = endPosition1.y - startPosition.y
            var newFrame = bg_view.frame
             var newFrame1 = table_view.frame
            newFrame.origin.x = bg_view.frame.origin.x
            newFrame.origin.y = bg_view.frame.origin.y + difference
            newFrame1.origin.x = table_view.frame.origin.x
            newFrame1.origin.y = table_view.frame.origin.y + difference1//Gesture Moving Upward will produce a negative value for difference
            newFrame.size.width = bg_view.frame.size.width
            newFrame.size.height = bg_view.frame.size.height - difference
            newFrame1.size.width = table_view.frame.size.width
            newFrame1.size.height = table_view.frame.size.height - difference1//Gesture Moving Upward will produce a negative value for difference
             bg_view.frame = newFrame
            print("THE NEW HEIGHT OF VIEW : \(bg_view.frame.height)")
             table_view.frame = newFrame1
            tbl_height_g.constant = bg_view.frame.height - 68
            }else{
                
                if currentDirection == 1 && bg_view.frame.height <= (self.view.frame.height / 1.5){
                    
                    translation = sender.translation(in: self.view)
                    translation1 = sender.translation(in: self.view)
                    sender.setTranslation(CGPoint(x: 0.0, y: 0.0), in: self.view)
                    let endPosition = sender.location(in: bg_view) // the posiion at which PanGesture Ended
                    let endPosition1 = sender.location(in: table_view)
                    difference = endPosition.y - startPosition.y
                    difference1 = endPosition1.y - startPosition.y
                    var newFrame = bg_view.frame
                    var newFrame1 = table_view.frame
                    newFrame.origin.x = bg_view.frame.origin.x
                    newFrame.origin.y = bg_view.frame.origin.y + difference
                    newFrame1.origin.x = table_view.frame.origin.x
                    newFrame1.origin.y = table_view.frame.origin.y + difference1//Gesture Moving Upward will produce a negative value for difference
                    newFrame.size.width = bg_view.frame.size.width
                    newFrame.size.height = bg_view.frame.size.height - difference
                    newFrame1.size.width = table_view.frame.size.width
                    newFrame1.size.height = table_view.frame.size.height - difference1//Gesture Moving Upward will produce a negative value for difference
                    bg_view.frame = newFrame
                    print("THE NEW HEIGHT OF VIEW : \(bg_view.frame.height)")
                    table_view.frame = newFrame1
                    tbl_height_g.constant = bg_view.frame.height - 68
                    
                    
                }else if currentDirection == 2 && bg_view.frame.height >= 72.0{
                    
                    translation = sender.translation(in: self.view)
                    translation1 = sender.translation(in: self.view)
                    sender.setTranslation(CGPoint(x: 0.0, y: 0.0), in: self.view)
                    let endPosition = sender.location(in: bg_view) // the posiion at which PanGesture Ended
                    let endPosition1 = sender.location(in: table_view)
                    difference = endPosition.y - startPosition.y
                    difference1 = endPosition1.y - startPosition.y
                    var newFrame = bg_view.frame
                    var newFrame1 = table_view.frame
                    newFrame.origin.x = bg_view.frame.origin.x
                    newFrame.origin.y = bg_view.frame.origin.y + difference
                    newFrame1.origin.x = table_view.frame.origin.x
                    newFrame1.origin.y = table_view.frame.origin.y + difference1//Gesture Moving Upward will produce a negative value for difference
                    newFrame.size.width = bg_view.frame.size.width
                    newFrame.size.height = bg_view.frame.size.height - difference
                    newFrame1.size.width = table_view.frame.size.width
                    newFrame1.size.height = table_view.frame.size.height - difference1//Gesture Moving Upward will produce a negative value for difference
                    bg_view.frame = newFrame
                    print("THE NEW HEIGHT OF VIEW : \(bg_view.frame.height)")
                    table_view.frame = newFrame1
                    tbl_height_g.constant = bg_view.frame.height - 68
                    
                    
                }
                
                print("THE NEW HEIGHT OF VIEW : \(bg_view.frame.height)")
                
            }
        }
        
        if sender.state == .ended || sender.state == .cancelled {
            //Do Something
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.layer.borderWidth = 0.5
        table_view.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0, blue: 0, alpha: 1)
        
         originalHeight = bg_view.frame.height
         originalHeight1 = table_view.frame.height
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        
       tap.delegate = self
        
        addressLabel.addGestureRecognizer(tap)
        
      mapView.delegate = self
        
        
        locationManager.delegate = (self as! CLLocationManagerDelegate)
        locationManager.requestWhenInUseAuthorization()
        
        placesClient = GMSPlacesClient.shared()
        self.nearbyPlaces()

        self.table_view.reloadData()
        
        bg_view.layer.cornerRadius = 16.0
        bg_view.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        bg_view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        bg_view.layer.shadowRadius = 3.0
        bg_view.layer.shadowOpacity = 0.2
        bg_view.layer.masksToBounds = false
        
        bg_button_search.layer.cornerRadius = 8
    }
    
    
    @objc func dismissKeyboard() {
       
        
    }
  
    
    
    @IBAction func label_address_click(_ sender: Any) {
        
        
        
        print(self.lbl_address?.lines as Any)
        print(self.lbl_address?.place)
        print(self.lbl_address?.placeID)
        print("Place name \(self.lbl_address?.lines)")
        print("Place address \(String(describing: self.lbl_address?.formattedAddress))")
        print("Place attributions \(String(describing: self.lbl_address?.attributions))")
        
        var lat = self.lbl_address?.coordinate.latitude
        var long = self.lbl_address?.coordinate.longitude
        var position = self.lbl_address?.coordinate
        
        if self.lbl_address?.formattedAddress == nil {
            addr_pos = NSString(string: "\(self.lbl_address?.lines as Any)")
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            let dadr = ("\(self.lbl_address?.lines), \(self.lbl_address?.formattedAddress!)") as NSString
            addr = dadr
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        if from_edit == "yes" {
            newViewController.checker = "NO"
            
            newViewController.check = "no"
            
        }
        else{
            newViewController.checker = "yes"
            if userdata_services.instance.no_address == "no"{
                newViewController.check = "yes"
                newViewController.id_check = self.check
            }
        }
        
        
        if no_ad == "yes" {
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(position!) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                } else {
                    if let places = response?.results() {
                        if let place = places.first {
                            
                            
                            if let lines = place.lines {
                                print("GEOCODE: Formatted Address: \(lines)")
                                var plc = "\(lines[0])"
                                newViewController.no_add = self.no_ad
                                
                                newViewController.test = plc as NSString
                                newViewController.addr_coordinates = place.coordinate
                                // self.spinner.stopAnimating()
                                self.present(newViewController, animated: false, completion: nil)
                            }
                            
                        } else {
                            
                            newViewController.test = self.addr_pos
                            print("GEOCODE: nil first in places")
                            // self.spinner.stopAnimating()
                            self.present(newViewController, animated: false, completion: nil)
                        }
                    } else {
                        
                        newViewController.test = self.addr_pos
                        print("GEOCODE: nil in places")
                        // self.spinner.stopAnimating()
                        self.present(newViewController, animated: false, completion: nil)
                    }
                }
            }
            
            
            
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            newViewController.addr_coordinates = self.lbl_address!.coordinate
            // self.spinner.stopAnimating()
            self.present(newViewController, animated: false, completion: nil)
            
        }
        
        
    }
    
    
    
    
    
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            print(address)
            self.lbl_address = address
            // 3
            self.addressLabel.text = lines.joined(separator: "\n")
            
            // 1
            let labelHeight = self.addressLabel.intrinsicContentSize.height
            self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                bottom: labelHeight, right: 0)

            
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
        
        
    }
   
    
    
    func nearbyPlaces() {
        placesClient?.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                self.likeHoodList = placeLikelihoodList
                self.table_view.reloadData()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let likeHoodList = likeHoodList {
            return likeHoodList.likelihoods.count
        }else{
        return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as!  tbl_cell_map
    
        var place = likeHoodList!.likelihoods[indexPath.row].place //this is a GMSPlace object
      Add_array[indexPath.row] = place //https://developers.google.com/places/ios-api/reference/interface_g_m_s_place
        print(place.formattedAddress!)
            print(place)
        
        cell.new_lbl_map.text = "\(place.name!),\(place.formattedAddress!)"
        
     //
        
        
        return cell
    
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var place = likeHoodList!.likelihoods[indexPath.row].place
        
        print(place)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        
        var lat = place.coordinate.latitude
        var long = place.coordinate.longitude
        var position = place.coordinate
        
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            let dadr = ("\(place.name!), \(place.formattedAddress!)") as NSString
            addr = dadr
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        if from_edit == "yes" {
            newViewController.checker = "NO"
            
            newViewController.check = "no"
            
        }
        else{
            newViewController.checker = "yes"
            if userdata_services.instance.no_address == "no"{
                newViewController.check = "yes"
                newViewController.id_check = self.check
            }
        }
        
        
        if no_ad == "yes" {
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(position) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                } else {
                    if let places = response?.results() {
                        if let place = places.first {
                            
                            
                            if let lines = place.lines {
                                print("GEOCODE: Formatted Address: \(lines)")
                                var plc = "\(lines[0])"
                                newViewController.no_add = self.no_ad
                                
                                newViewController.test = plc as NSString
                                newViewController.addr_coordinates = place.coordinate
                              // self.spinner.stopAnimating()
                                self.present(newViewController, animated: false, completion: nil)
                            }
                            
                        } else {
                            
                            newViewController.test = self.addr_pos
                            print("GEOCODE: nil first in places")
                           // self.spinner.stopAnimating()
                            self.present(newViewController, animated: false, completion: nil)
                        }
                    } else {
                        
                        newViewController.test = self.addr_pos
                        print("GEOCODE: nil in places")
                       // self.spinner.stopAnimating()
                        self.present(newViewController, animated: false, completion: nil)
                    }
                }
            }
            
            
            
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            newViewController.addr_coordinates = place.coordinate
           // self.spinner.stopAnimating()
            self.present(newViewController, animated: false, completion: nil)
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
         
        
        view.layoutIfNeeded()
        table_view.layoutIfNeeded()
        let indexpath = IndexPath(item: 0, section: 0)
        UIView.animate(withDuration: 1.5, animations: {
            self.tbl_height.constant = 180
            self.view.layoutIfNeeded()
            self.tbl_height_g.constant = 400
            self.table_view.scrollToRow(at: indexpath, at: UITableViewScrollPosition.top, animated: true)
            self.table_view.layoutIfNeeded()
        })
    }
    }
   
    @IBAction func back_btn_clicked(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
   
    @IBAction func onLaunchClicked(sender: UIButton) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = (self as! GMSAutocompleteViewControllerDelegate)
        present(acController, animated: true, completion: nil)
    }
    

}


extension GCM_map_ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        print(place.placeID)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        
         dismiss(animated: true, completion: nil)
        
        var lat = place.coordinate.latitude
        var long = place.coordinate.longitude
        var position = place.coordinate
        
        if place.formattedAddress == nil {
            addr_pos = place.name as! NSString
            no_ad = "yes"
        }
        else{
            no_ad = "no"
            let dadr = ("\(place.name!), \(place.formattedAddress!)") as NSString
            addr = dadr
            
        }
        let storyBoard1: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard1.instantiateViewController(withIdentifier: "edit") as! edit_address_ViewController
        if from_edit == "yes" {
            newViewController.checker = "NO"
            
            newViewController.check = "no"
            
        }
        else{
            newViewController.checker = "yes"
            if userdata_services.instance.no_address == "no"{
                newViewController.check = "yes"
                newViewController.id_check = self.check
            }
        }
        
        
        if no_ad == "yes" {
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(position) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                } else {
                    if let places = response?.results() {
                        if let place = places.first {
                            
                            
                            if let lines = place.lines {
                                print("GEOCODE: Formatted Address: \(lines)")
                                var plc = "\(lines[0])"
                                newViewController.no_add = self.no_ad
                                
                                newViewController.test = plc as NSString
                                newViewController.addr_coordinates = place.coordinate
                                // self.spinner.stopAnimating()
                                self.present(newViewController, animated: false, completion: nil)
                            }
                            
                        } else {
                            
                            newViewController.test = self.addr_pos
                            print("GEOCODE: nil first in places")
                            // self.spinner.stopAnimating()
                            self.present(newViewController, animated: false, completion: nil)
                        }
                    } else {
                        
                        newViewController.test = self.addr_pos
                        print("GEOCODE: nil in places")
                        // self.spinner.stopAnimating()
                        self.present(newViewController, animated: false, completion: nil)
                    }
                }
            }
            
            
            
        }
        else{
            newViewController.no_add = no_ad
            newViewController.test = addr
            newViewController.addr_coordinates = place.coordinate
            // self.spinner.stopAnimating()
            self.present(newViewController, animated: false, completion: nil)
            
        }
       
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
}


// MARK: - CLLocationManagerDelegate
//1
extension GCM_map_ViewController: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        // 7
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 20, bearing: 0, viewingAngle: 0)
        
        // 8
        locationManager.stopUpdatingLocation()
    }
    
    
}



extension GCM_map_ViewController: GMSMapViewDelegate {
    
    
//    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
//        addressLabel.lock()
//    }

    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        reverseGeocodeCoordinate(position.target)
        
        
        
    }

}


class tbl_cell_map: UITableViewCell{
    
    @IBOutlet var new_lbl_map: UILabel!
    
    
    
}
